<?php
 // created: 2014-05-31 12:39:38
$layout_defs["prov1_Aziende"]["subpanel_setup"]['prov1_aziende_prov1_dipendenti'] = array (
  'order' => 100,
  'module' => 'prov1_Dipendenti',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROV1_AZIENDE_PROV1_DIPENDENTI_FROM_PROV1_DIPENDENTI_TITLE',
  'get_subpanel_data' => 'prov1_aziende_prov1_dipendenti',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
