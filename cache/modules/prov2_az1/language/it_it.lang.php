<?php
// created: 2014-06-06 12:30:09
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Cancellato',
  'LBL_NAME' => 'Ragione Sociale',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_LIST_FORM_TITLE' => 'azi1 Lista',
  'LBL_MODULE_NAME' => 'azi1',
  'LBL_MODULE_TITLE' => 'azi1',
  'LBL_HOMEPAGE_TITLE' => 'Mio azi1',
  'LNK_NEW_RECORD' => 'Crea azi1',
  'LNK_LIST' => 'Visualizza azi1',
  'LNK_IMPORT_PROV2_AZIENDE' => 'Importa azi1',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca azi1',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_PROV2_AZIENDE_SUBPANEL_TITLE' => 'azi1',
  'LBL_NEW_FORM_TITLE' => 'Nuovo azi1',
  'LBL_RAGIONE_SOCIALE' => 'Ragione Sociale',
  'LNK_IMPORT_PROV2_AZ1' => 'Importa azi1',
  'LBL_PROV2_AZ1_SUBPANEL_TITLE' => 'azi1',
);