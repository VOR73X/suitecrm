<?php
// created: 2014-06-06 12:30:04
$mod_strings = array (
  'LBL_LIST_NAME' => 'Nome',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Cancellato',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_LIST_FORM_TITLE' => 'F_Potenziale Cliente Lista',
  'LBL_MODULE_NAME' => 'F_Potenziale Cliente',
  'LBL_MODULE_TITLE' => 'F_Potenziale Cliente',
  'LBL_HOMEPAGE_TITLE' => 'Mio F_Potenziale Cliente',
  'LNK_NEW_RECORD' => 'Crea F_Potenziale Cliente',
  'LNK_LIST' => 'Visualizza F_Potenziale Cliente',
  'LNK_IMPORT_PROV2_POTENZIALE_CLIENTE' => 'Importa F_Potenziale Cliente',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca F_Potenziale Cliente',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_PROV2_POTENZIALE_CLIENTE_SUBPANEL_TITLE' => 'F_Potenziale Cliente',
  'LBL_NEW_FORM_TITLE' => 'Nuovo F_Potenziale Cliente',
  'LNK_IMPORT_PROV2_F_POTENZIALE_CLIENTE' => 'Importa F_Potenziale Cliente',
  'LBL_PROV2_F_POTENZIALE_CLIENTE_SUBPANEL_TITLE' => 'F_Potenziale Cliente',
  'LBL_COGNOME' => 'Cognome',
  'LBL_NUMERO_PATENTE' => 'Numero di Patente',
  'LBL_CODICE_FISCALE' => 'Codice Fiscale',
  'LBL_VIA' => 'Via',
  'LBL_CITTA' => 'Citta',
  'LBL_PROVINCIA' => 'Provincia',
  'LBL_STATO' => 'Stato',
  'LBL_DATA_NASCITA' => 'Data di Nascita',
  'LBL_EDITVIEW_PANEL1' => 'Dati Anagrafici',
  'LBL_TELEFONO' => 'Telefono',
  'LBL_QUICKCREATE_PANEL1' => 'Nuovo Pannello 1',
);