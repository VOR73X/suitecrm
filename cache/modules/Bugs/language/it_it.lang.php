<?php
// created: 2014-06-06 12:30:03
$mod_strings = array (
  'LBL_LIST_NAME' => 'Nome',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Creato il:',
  'LBL_DATE_MODIFIED' => 'Modificato il:',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DESCRIPTION' => 'Descrizione:',
  'LBL_DELETED' => 'Cancellato',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_SUBJECT' => 'Oggetto:',
  'LBL_WORK_LOG' => 'Registro Operazioni:',
  'LBL_NUMBER' => 'Numero:',
  'LBL_STATUS' => 'Stato:',
  'LBL_PRIORITY' => 'Priorità:',
  'LBL_RESOLUTION' => 'Soluzione:',
  'LBL_LAST_MODIFIED' => 'Ultima Modifica',
  'LBL_CREATED_BY' => 'Creato da:',
  'LBL_DATE_CREATED' => 'Data creazione:',
  'LBL_MODIFIED_BY' => 'Ultima modifica fatta da:',
  'LBL_ASSIGNED_USER' => 'Assegnato a:',
  'LBL_SYSTEM_ID' => 'ID Sistema',
  'LBL_TEAM_NAME' => 'Nome Gruppo:',
  'LBL_TYPE' => 'Tipo:',
  'LBL_FOUND_IN_RELEASE_NAME' => 'Trovato nella Release',
  'LBL_PORTAL_VIEWABLE' => 'Portale visibile',
  'LBL_EXPORT_ASSIGNED_USER_NAME' => 'Nome Utente Assegnato',
  'LBL_EXPORT_ASSIGNED_USER_ID' => 'ID Utente Assegnato',
  'LBL_EXPORT_FIXED_IN_RELEASE_NAMR' => 'Risolto nella Release',
  'LBL_EXPORT_MODIFIED_USER_ID' => 'Modificato da ID',
  'LBL_EXPORT_CREATED_BY' => 'Creato da ID',
  'LBL_MODULE_TITLE' => 'Bug Tracker: Home',
  'LBL_BUG' => 'Bug:',
  'LBL_LIST_RELEASE' => 'Release',
  'LBL_RELEASE' => 'Release:',
  'LBL_DEFAULT_SUBPANEL_TITLE' => 'Bug Tracker',
  'LBL_MODULE_NAME' => 'Bug',
  'LBL_MODULE_ID' => 'Bug',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca Bug',
  'LBL_LIST_FORM_TITLE' => 'Elenco Bug',
  'LBL_NEW_FORM_TITLE' => 'Nuovo Bug',
  'LBL_CONTACT_BUG_TITLE' => 'Contatto-Bug:',
  'LBL_BUG_NUMBER' => 'Numero Bug:',
  'LBL_CONTACT_NAME' => 'Nome Contatto:',
  'LBL_BUG_SUBJECT' => 'Oggetto Bug:',
  'LBL_CONTACT_ROLE' => 'Ruolo:',
  'LBL_LIST_NUMBER' => 'Numero.',
  'LBL_LIST_SUBJECT' => 'Oggetto',
  'LBL_LIST_STATUS' => 'Stato',
  'LBL_LIST_PRIORITY' => 'Priorità',
  'LBL_LIST_RESOLUTION' => 'Soluzione',
  'LBL_LIST_LAST_MODIFIED' => 'Ultima modifica',
  'LBL_INVITEE' => 'Contatti',
  'LBL_LIST_TYPE' => 'Tipo',
  'LNK_NEW_BUG' => 'Nuovo Bug',
  'LNK_BUG_LIST' => 'Bug',
  'NTC_REMOVE_INVITEE' => 'Sei sicuro di voler rimuovere questo contatto dal Bug ?',
  'NTC_REMOVE_ACCOUNT_CONFIRMATION' => 'Sei sicuro di voler rimuovere questo Bug dall´azienda ?',
  'ERR_DELETE_RECORD' => 'Per eliminare il bug deve essere specificato il numero del record.',
  'LBL_LIST_MY_BUGS' => 'I miei Bug',
  'LNK_IMPORT_BUGS' => 'Importa Bug',
  'LBL_FOUND_IN_RELEASE' => 'Trovato nella Release:',
  'LBL_FIXED_IN_RELEASE' => 'Risolto nella Release:',
  'LBL_LIST_FIXED_IN_RELEASE' => 'Risolto nella Release',
  'LBL_SOURCE' => 'Fonte:',
  'LBL_PRODUCT_CATEGORY' => 'Categoria:',
  'LBL_DATE_LAST_MODIFIED' => 'Data Modifica:',
  'LBL_LIST_EMAIL_ADDRESS' => 'Indirizzo Email',
  'LBL_LIST_CONTACT_NAME' => 'Nome contatto',
  'LBL_LIST_ACCOUNT_NAME' => 'Azienda',
  'LBL_LIST_PHONE' => 'Telefono',
  'NTC_DELETE_CONFIRMATION' => 'Sei sicuro di voler rimuovere questo contatto da questo Bug ?',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contatti',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Aziende',
  'LBL_CASES_SUBPANEL_TITLE' => 'Reclami',
  'LBL_PROJECTS_SUBPANEL_TITLE' => 'Progetti',
  'LBL_DOCUMENTS_SUBPANEL_TITLE' => 'Documenti',
  'LBL_LIST_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LNK_BUG_REPORTS' => 'Report dei Bug',
  'LBL_SHOW_IN_PORTAL' => 'Mostralo nel Portale',
  'LBL_BUG_INFORMATION' => 'Informazioni Bug',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Gruppi di Sicurezza',
);