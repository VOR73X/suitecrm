<?php
// created: 2014-06-06 12:29:21
$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Aziende',
  'LBL_ASSIGNED_TO' => 'Assegnato a',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contatti',
  'LBL_COPY_PREFIX' => 'Copia di',
  'LBL_CREATED' => 'Creato da:',
  'LBL_CREATED_BY' => 'Creato da:',
  'LBL_DATE_CREATED' => 'Data Creazione',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_LAST_MODIFIED' => 'Data Modifica',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DOMAIN' => 'Nessuna email di Dominio:',
  'LBL_DOMAIN_NAME' => 'Nome Dominio',
  'LBL_ENTRIES' => 'Voci Totali',
  'LBL_LEADS_SUBPANEL_TITLE' => 'Lead',
  'LBL_LIST_DESCRIPTION' => 'Descrizione',
  'LBL_LIST_END_DATE' => 'Data Fine',
  'LBL_LIST_ENTRIES' => 'Obiettivi nella Lista',
  'LBL_LIST_FORM_TITLE' => 'Liste Obiettivi',
  'LBL_LIST_PROSPECTLIST_NAME' => 'Nome',
  'LBL_LIST_PROSPECT_LIST_NAME' => 'Lista Obiettivi',
  'LBL_LIST_TYPE' => 'Tipo',
  'LBL_LIST_TYPE_LIST_NAME' => 'Tipo',
  'LBL_LIST_TYPE_NO' => 'Tipo',
  'LBL_MARKETING_ID' => 'Marketing',
  'LBL_MARKETING_MESSAGE' => 'Messaggio di Email Marketing',
  'LBL_MARKETING_NAME' => 'Nome Marketing',
  'LBL_MODIFIED' => 'Modificato da:',
  'LBL_MODIFIED_BY' => 'Modificato da:',
  'LBL_MODULE_ID' => 'Liste Obiettivi',
  'LBL_MODULE_NAME' => 'Liste Obiettivi',
  'LBL_MODULE_TITLE' => 'Liste Obiettivi: Home',
  'LBL_MORE_DETAIL' => 'Più dettagli',
  'LBL_NAME' => 'Nome',
  'LBL_NEW_FORM_TITLE' => 'Nuova Lista Obiettivi',
  'LBL_PROSPECTS_SUBPANEL_TITLE' => 'Obiettivi',
  'LBL_PROSPECT_LISTS_SUBPANEL_TITLE' => 'Liste Obiettivi',
  'LBL_PROSPECT_LIST_NAME' => 'Lista Obiettivo:',
  'LBL_SEARCH_FORM_TITLE' => 'Cerca Liste Obiettivi',
  'LBL_TEAM' => 'Gruppo:',
  'LBL_TYPE' => 'Tipo',
  'LBL_USERS_SUBPANEL_TITLE' => 'Utenti',
  'LNK_CAMPAIGN_LIST' => 'Campagne',
  'LNK_NEW_CAMPAIGN' => 'Crea Campagna',
  'LNK_NEW_PROSPECT' => 'Nuovo Obiettivo',
  'LNK_NEW_PROSPECT_LIST' => 'Nuova Lista Obiettivi',
  'LNK_PROSPECT_LIST' => 'Obiettivi',
  'LNK_PROSPECT_LIST_LIST' => 'Lista Obiettivi',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Gruppi di Sicurezza',
);