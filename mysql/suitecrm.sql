-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Giu 06, 2014 alle 12:59
-- Versione del server: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `suitecrm`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `account_type` varchar(50) DEFAULT NULL,
  `industry` varchar(50) DEFAULT NULL,
  `annual_revenue` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `rating` varchar(100) DEFAULT NULL,
  `phone_office` varchar(100) DEFAULT NULL,
  `phone_alternate` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `ownership` varchar(100) DEFAULT NULL,
  `employees` varchar(10) DEFAULT NULL,
  `ticker_symbol` varchar(10) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `sic_code` varchar(10) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_accnt_id_del` (`id`,`deleted`),
  KEY `idx_accnt_name_del` (`name`,`deleted`),
  KEY `idx_accnt_assigned_del` (`deleted`,`assigned_user_id`),
  KEY `idx_accnt_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts_audit`
--

CREATE TABLE IF NOT EXISTS `accounts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_accounts_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts_bugs`
--

CREATE TABLE IF NOT EXISTS `accounts_bugs` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_bug_acc` (`account_id`),
  KEY `idx_acc_bug_bug` (`bug_id`),
  KEY `idx_account_bug` (`account_id`,`bug_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts_cases`
--

CREATE TABLE IF NOT EXISTS `accounts_cases` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_case_acc` (`account_id`),
  KEY `idx_acc_acc_case` (`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts_contacts`
--

CREATE TABLE IF NOT EXISTS `accounts_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_contact` (`account_id`,`contact_id`),
  KEY `idx_contid_del_accid` (`contact_id`,`deleted`,`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts_cstm`
--

CREATE TABLE IF NOT EXISTS `accounts_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `accounts_opportunities`
--

CREATE TABLE IF NOT EXISTS `accounts_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_opportunity` (`account_id`,`opportunity_id`),
  KEY `idx_oppid_del_accid` (`opportunity_id`,`deleted`,`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `acl_actions`
--

CREATE TABLE IF NOT EXISTS `acl_actions` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `acltype` varchar(100) DEFAULT NULL,
  `aclaccess` int(3) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclaction_id_del` (`id`,`deleted`),
  KEY `idx_category_name` (`category`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `acl_actions`
--

INSERT INTO `acl_actions` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `name`, `category`, `acltype`, `aclaccess`, `deleted`) VALUES
('22bac10e-124f-783a-5688-5388e3128ce1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Leads', 'module', 89, 0),
('23b4c695-2804-6399-f4e9-5388e35d6e1f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Leads', 'module', 90, 0),
('24704335-3b03-53a9-0e10-5388e3a7c529', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Leads', 'module', 90, 0),
('256a4f40-10a0-48fe-b27c-5388e36b7ff6', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Leads', 'module', 90, 0),
('2625cc2f-76ad-237a-ab6e-5388e33759d9', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Leads', 'module', 90, 0),
('26e15e99-74b9-990d-0b4e-5388e3d7c6de', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Leads', 'module', 90, 0),
('279cd16c-1b2f-3ba0-9582-5388e36cc71f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Leads', 'module', 90, 0),
('2896d572-4261-252e-bb76-5388e3b8538a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Leads', 'module', 90, 0),
('44e94365-5327-e987-bf1b-5388e32ecb27', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Cases', 'module', 89, 0),
('45e34fd0-c43a-15f6-ecaa-5388e302fe9e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Cases', 'module', 90, 0),
('469ec493-1c13-ea92-81d4-5388e37d7b43', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Cases', 'module', 90, 0),
('475a48ad-5936-4106-4d46-5388e3d8edc9', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Cases', 'module', 90, 0),
('4815c1ca-a683-1661-80ca-5388e3f58f37', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Cases', 'module', 90, 0),
('48d15451-c7b8-8102-8944-5388e37d96ef', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Cases', 'module', 90, 0),
('49cb536c-6b83-94d3-1fe7-5388e3b63502', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Cases', 'module', 90, 0),
('4a86d945-9a9a-5a72-70f5-5388e3692873', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Cases', 'module', 90, 0),
('617a2084-7266-5c20-e79a-5388e3a15c70', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Bugs', 'module', 89, 0),
('62743b5b-1c33-6661-a23f-5388e3f6fcfa', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Bugs', 'module', 90, 0),
('632fbde6-7066-2e47-03b6-5388e3338dab', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Bugs', 'module', 90, 0),
('6429bf41-08fb-e5bc-c129-5388e32c1860', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Bugs', 'module', 90, 0),
('64e53665-4092-6d26-d22d-5388e3dad1fb', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Bugs', 'module', 90, 0),
('65a0b408-2e79-1299-8bdb-5388e3708a7c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Bugs', 'module', 90, 0),
('665c3665-e8df-52cb-bc12-5388e3a6fd67', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Bugs', 'module', 90, 0),
('6717c4db-177a-3d4d-355e-5388e39a02c8', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Bugs', 'module', 90, 0),
('83e725dc-b5ae-1d9c-9518-5388e36e6798', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Users', 'module', 89, 0),
('84a2be79-1d5d-3b72-323f-5388e3296248', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Users', 'module', 90, 0),
('855e3716-98c8-6a7c-a2e3-5388e34276dd', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Users', 'module', 90, 0),
('8619b946-91c7-60a3-ae09-5388e3ad2d93', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Users', 'module', 90, 0),
('8713b1db-4c4d-603e-d8dc-5388e3a6cd52', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Users', 'module', 90, 0),
('880dbfda-7084-21a8-d39e-5388e353de25', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Users', 'module', 90, 0),
('88c948c9-a136-d7a4-3731-5388e36f8573', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Users', 'module', 90, 0),
('8984c819-e76e-cfbc-498d-5388e3bd8237', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Users', 'module', 90, 0),
('9e45964c-f5af-c7ad-2154-5388e31f6e9c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Project', 'module', 89, 0),
('9f011d8d-df55-a318-657d-5388e3c2374e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Project', 'module', 90, 0),
('9fbc972c-f40d-7d0a-94ec-5388e3d0f93a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Project', 'module', 90, 0),
('a0b691c8-dbcd-38ea-5f6f-5388e3d1ef99', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Project', 'module', 90, 0),
('a172149d-759a-da34-8540-5388e3573f25', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Project', 'module', 90, 0),
('a22d951d-22ac-7e37-a907-5388e3e9194d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Project', 'module', 90, 0),
('a2e927b2-dcdc-30b3-1096-5388e3d93fe9', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Project', 'module', 90, 0),
('a3a4a0aa-64bc-3c24-a238-5388e357fce0', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Project', 'module', 90, 0),
('b2c7d06f-a298-28c6-af7c-5388e372e100', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'ProjectTask', 'module', 89, 0),
('b38362e6-336c-4fa0-bd32-5388e308edec', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'ProjectTask', 'module', 90, 0),
('b47d66d1-35c0-bbad-1a4b-5388e3114c29', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'ProjectTask', 'module', 90, 0),
('b538e925-d2da-f423-37b5-5388e3a94b36', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'ProjectTask', 'module', 90, 0),
('b5f46422-ab97-8a07-ae31-5388e314498f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'ProjectTask', 'module', 90, 0),
('b6afe8ac-795d-9709-a118-5388e35b20e0', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'ProjectTask', 'module', 90, 0),
('b76b6bc7-8ac8-e1ed-132c-5388e300ed78', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'ProjectTask', 'module', 90, 0),
('b86574a4-a863-3860-9a42-5388e389395b', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'ProjectTask', 'module', 90, 0),
('d728e8ce-e1be-a51c-cc43-5388e3e16756', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Campaigns', 'module', 89, 0),
('d822ed73-3ef9-dbea-68ae-5388e32df995', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Campaigns', 'module', 90, 0),
('d8de6f40-33ef-a704-a2f8-5388e3f270c5', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Campaigns', 'module', 90, 0),
('d999ed20-ba39-56fc-bea1-5388e385464e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Campaigns', 'module', 90, 0),
('da557e36-9431-9c0c-f763-5388e3486ceb', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Campaigns', 'module', 90, 0),
('db10fb1c-252a-1e9c-08b5-5388e3a5388e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Campaigns', 'module', 90, 0),
('dc0afe71-e002-df0e-c072-5388e3d90ac7', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Campaigns', 'module', 90, 0),
('dcc67b01-78bf-d9a5-9044-5388e31e5349', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Campaigns', 'module', 90, 0),
('1938b756-dcd6-d104-77d3-5388e3f6fdb1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'ProspectLists', 'module', 89, 0),
('19f4316e-99e7-5104-c309-5388e3fb4402', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'ProspectLists', 'module', 90, 0),
('1aee3588-8325-60b0-7a5a-5388e3d36913', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'ProspectLists', 'module', 90, 0),
('1ba9bb17-11d1-3bca-f7d4-5388e334292d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'ProspectLists', 'module', 90, 0),
('1c65387d-879e-8fc8-7c5a-5388e3438fb5', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'ProspectLists', 'module', 90, 0),
('1d20ca0a-a395-feef-1f31-5388e31eaed2', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'ProspectLists', 'module', 90, 0),
('1ddc416a-c976-2fb7-ff3e-5388e390cb02', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'ProspectLists', 'module', 90, 0),
('1e97cad3-0693-8755-1b9a-5388e30274e6', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'ProspectLists', 'module', 90, 0),
('2dbafa78-19c5-67b7-adad-5388e3e5be26', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Prospects', 'module', 89, 0),
('2eb50717-19b7-9148-b419-5388e362779f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Prospects', 'module', 90, 0),
('2f708db9-21bc-c6c8-eace-5388e3894ddd', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Prospects', 'module', 90, 0),
('302c095a-9f07-1638-159e-5388e3eb4cab', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Prospects', 'module', 90, 0),
('30e787d9-3047-47ff-e287-5388e34fb05b', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Prospects', 'module', 90, 0),
('31a308e5-0481-0ab3-f810-5388e32ebfd0', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Prospects', 'module', 90, 0),
('329d1b4e-a32f-7703-d16e-5388e35d7e01', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Prospects', 'module', 90, 0),
('335896c8-cb06-37a1-4ab1-5388e3b7bc6e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Prospects', 'module', 90, 0),
('44ecdce2-4696-ffba-5d97-5388e37f26ed', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'EmailMarketing', 'module', 89, 0),
('45a85891-7f2b-b601-6472-5388e36d4dc9', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'EmailMarketing', 'module', 90, 0),
('46a258b1-364c-113f-c47e-5388e31a456f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'EmailMarketing', 'module', 90, 0),
('475dd6aa-77e5-319a-1a15-5388e3949467', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'EmailMarketing', 'module', 90, 0),
('48196717-deb9-51c0-78f4-5388e3cf5f75', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'EmailMarketing', 'module', 90, 0),
('48d4ecc6-2ced-1ba9-9e08-5388e3ec74fe', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'EmailMarketing', 'module', 90, 0),
('49ceeab2-7a6c-db15-4c7c-5388e3632281', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'EmailMarketing', 'module', 90, 0),
('4a8a67ea-9c6d-8c59-c176-5388e3fdc20c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'EmailMarketing', 'module', 90, 0),
('a1f2b8da-66e6-7faa-9866-5388e3142972', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Contacts', 'module', 89, 0),
('a2ae3d42-30da-6610-e5b0-5388e32d1b47', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Contacts', 'module', 90, 0),
('a3a834e3-af17-34f1-0a6b-5388e33ef904', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Contacts', 'module', 90, 0),
('a463b6be-ca45-6e2a-629d-5388e369fba0', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Contacts', 'module', 90, 0),
('a51f31a2-dfae-c48b-24e4-5388e3b2047c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Contacts', 'module', 90, 0),
('a5dac7db-8f3e-c742-3f62-5388e33b7865', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Contacts', 'module', 90, 0),
('a69649c3-f292-2939-71a5-5388e39d5a61', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Contacts', 'module', 90, 0),
('a751c0c9-bfcc-c672-7a48-5388e3ef322d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Contacts', 'module', 90, 0),
('c4dcb927-c60d-b588-ebf7-5388e303015c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Accounts', 'module', 89, 0),
('c59837aa-52a2-54d3-47a3-5388e30ad50f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Accounts', 'module', 90, 0),
('c6923621-77dc-3308-0e70-5388e383ed4a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Accounts', 'module', 90, 0),
('c74db2b0-bb7a-35cc-6fd6-5388e377529b', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Accounts', 'module', 90, 0),
('c847c4d1-cac7-258c-68eb-5388e3b0d64b', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Accounts', 'module', 90, 0),
('c941cdf4-0256-a9df-9552-5388e3bcbfed', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Accounts', 'module', 90, 0),
('c9fd4bcf-5355-49ca-6bd5-5388e3a5eeea', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Accounts', 'module', 90, 0),
('caf74ea9-d181-c73f-8a92-5388e3394996', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Accounts', 'module', 90, 0),
('e5d2b3c3-47f1-1a77-0d7a-5388e3ed5189', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Opportunities', 'module', 89, 0),
('e68e3e04-625e-1e31-7f85-5388e39036bd', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Opportunities', 'module', 90, 0),
('e749b556-2123-69ad-b713-5388e3addafe', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Opportunities', 'module', 90, 0),
('e843b240-247e-f8fe-80a9-5388e35c0a64', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Opportunities', 'module', 90, 0),
('e8ff3ae0-b0c7-abf1-7123-5388e3ada9ac', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Opportunities', 'module', 90, 0),
('e9bab8b3-0eb5-db8c-1c56-5388e384fe6d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Opportunities', 'module', 90, 0),
('ea7646b9-63a0-684a-1ac0-5388e3d021dc', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Opportunities', 'module', 90, 0),
('eb31c770-8856-6829-374e-5388e3bbdcf7', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Opportunities', 'module', 90, 0),
('d8410adf-2386-a6bb-37f8-5388e31d8fe4', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'EmailTemplates', 'module', 89, 0),
('e7e10bff-a5a6-630d-8bf5-5388e3573082', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'EmailTemplates', 'module', 90, 0),
('f3990481-68cc-d0cb-d866-5388e3a99242', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'EmailTemplates', 'module', 90, 0),
('ff520a6c-fcc8-9b46-0b3a-5388e3815323', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'EmailTemplates', 'module', 90, 0),
('10b0a4df-9734-f495-7667-5388e3e843aa', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'EmailTemplates', 'module', 90, 0),
('11aaadcf-430a-8817-894c-5388e3f73ccf', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'EmailTemplates', 'module', 90, 0),
('12662d3c-af16-bf22-a3e2-5388e3f895dc', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'EmailTemplates', 'module', 90, 0),
('1321af24-549c-d2c8-a4b7-5388e3bc1971', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'EmailTemplates', 'module', 90, 0),
('210c6195-54a1-50fd-7630-5388e301b389', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Notes', 'module', 89, 0),
('21c7eedf-59d6-17d6-bbff-5388e3a6bbe1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Notes', 'module', 90, 0),
('2283678e-f753-5c31-eefa-5388e34cf495', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Notes', 'module', 90, 0),
('233ee4fb-caee-e852-c911-5388e3bbfc91', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Notes', 'module', 90, 0),
('2438e3b4-9b08-4e81-8c8d-5388e3628e69', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Notes', 'module', 90, 0),
('24f4789e-01c5-e1be-baa0-5388e31f76b8', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Notes', 'module', 90, 0),
('25ee75f6-b1b0-1257-ef72-5388e38ecb8c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Notes', 'module', 90, 0),
('26a9f544-5861-97bc-a263-5388e35c1f11', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Notes', 'module', 90, 0),
('3ba94b25-e2ac-ce84-b282-5388e32b631d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Calls', 'module', 89, 0),
('3c64c291-1aa9-4a8f-3e80-5388e34ca7cd', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Calls', 'module', 90, 0),
('3d203229-29dd-1a63-118f-5388e3531183', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Calls', 'module', 90, 0),
('3e1a4299-b81f-7516-7842-5388e39a861e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Calls', 'module', 90, 0),
('3ed5d6a4-ab69-998f-ff74-5388e3b02e18', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Calls', 'module', 90, 0),
('3f915580-d85d-d22b-358e-5388e3ff7e2f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Calls', 'module', 90, 0),
('404cdcd3-f744-83db-2581-5388e3ece82b', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Calls', 'module', 90, 0),
('41085470-8204-0009-b81b-5388e38a27a0', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Calls', 'module', 90, 0),
('5413a884-7122-bd10-aeae-5388e3c88a6e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Emails', 'module', 89, 0),
('550daf1d-fd20-7710-f9e7-5388e36da34e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Emails', 'module', 90, 0),
('55c9285d-30b1-8c3f-54b8-5388e304eaed', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Emails', 'module', 90, 0),
('5684a292-ba8e-ba37-b4d3-5388e3c97359', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Emails', 'module', 90, 0),
('577ea5c5-307e-90b2-4ea6-5388e3df6f5c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Emails', 'module', 90, 0),
('583a39cc-464d-63a2-b886-5388e3f2b599', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Emails', 'module', 90, 0),
('58f5b957-35c8-a554-5779-5388e305b5aa', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Emails', 'module', 90, 0),
('59b13c64-7a65-5142-ecd4-5388e3e0112b', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Emails', 'module', 90, 0),
('6e33867d-ed6f-e2f4-9371-5388e323a250', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Meetings', 'module', 89, 0),
('6f2d852b-23b2-39ef-29c7-5388e37ff2ea', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Meetings', 'module', 90, 0),
('6fe902ab-3fbc-866f-a16d-5388e37b8509', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Meetings', 'module', 90, 0),
('70a48314-106e-8682-85e8-5388e31bfef1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Meetings', 'module', 90, 0),
('716004d4-b88a-5854-56d9-5388e327112a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Meetings', 'module', 90, 0),
('721b9228-54dd-1128-ae19-5388e300cf65', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Meetings', 'module', 90, 0),
('72d71c3a-e0e5-ab4a-328f-5388e32982b8', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Meetings', 'module', 90, 0),
('73929575-9fe9-322f-2f90-5388e3ebe948', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Meetings', 'module', 90, 0),
('8526d04f-6ac4-8cdb-69c4-5388e3d6048a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Tasks', 'module', 89, 0),
('8620d07d-d4bb-7e28-741a-5388e3f1e7ce', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Tasks', 'module', 90, 0),
('86dc572d-f1ad-58ea-044e-5388e351db79', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Tasks', 'module', 90, 0),
('8797ecce-5075-42c2-c0cf-5388e3398816', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Tasks', 'module', 90, 0),
('88536979-24da-cf05-dfba-5388e340390d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Tasks', 'module', 90, 0),
('894d678e-5423-9282-7243-5388e3c4c7ac', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Tasks', 'module', 90, 0),
('8a08eaaa-1758-2781-bdbd-5388e328d024', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Tasks', 'module', 90, 0),
('8ac46c5c-4882-5017-7273-5388e3a7e976', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Tasks', 'module', 90, 0),
('1f1be6c9-c849-f8fc-5f7d-5388e3de803d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Documents', 'module', 89, 0),
('1fd779b2-6b2d-ea64-bcd2-5388e3ec84c8', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Documents', 'module', 90, 0),
('2092fc0d-0af7-fd1d-e017-5388e3a8ee6c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Documents', 'module', 90, 0),
('218cf84f-9c06-31dc-48f5-5388e3d32787', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Documents', 'module', 90, 0),
('2286f269-3747-9a7b-a608-5388e341a4d8', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Documents', 'module', 90, 0),
('23427861-4a72-65ea-f110-5388e328e81e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Documents', 'module', 90, 0),
('23fe0380-883e-55f2-3aa6-5388e311fbf5', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Documents', 'module', 90, 0),
('24b9819a-9fb9-7b23-41c8-5388e3bdf700', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Documents', 'module', 90, 0),
('79ef49aa-4f69-6ef2-eadc-5388e3e6d767', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'EAPM', 'module', 89, 0),
('7b27c754-0113-2f65-90c8-5388e39b4b6c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'EAPM', 'module', 90, 0),
('7be345dd-146b-493c-e457-5388e312bb07', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'EAPM', 'module', 90, 0),
('7cdd4b2c-a843-47c4-1a2c-5388e3b3e711', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'EAPM', 'module', 90, 0),
('7d98dd47-3e36-b9f2-8bf1-5388e389a260', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'EAPM', 'module', 90, 0),
('7e5451cb-98e4-472e-3c69-5388e3036764', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'EAPM', 'module', 90, 0),
('7f4e52cd-cffe-42ba-7600-5388e3b38f05', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'EAPM', 'module', 90, 0),
('8009d06d-d196-ba4b-a903-5388e3877e29', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'EAPM', 'module', 90, 0),
('b0a009c3-f65f-5589-af14-5388e3669bdb', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'Calls_Reschedule', 'module', 89, 0),
('b15b9278-c54a-0699-7342-5388e3862bec', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'Calls_Reschedule', 'module', 90, 0),
('b21711d7-800c-0fcc-9647-5388e3b45269', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'Calls_Reschedule', 'module', 90, 0),
('b2d29d10-9725-257c-6297-5388e3e22dcb', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'Calls_Reschedule', 'module', 90, 0),
('b38e1a7c-776f-dfb8-3c06-5388e3864a19', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'Calls_Reschedule', 'module', 90, 0),
('b4881708-d568-3350-9ee6-5388e35910d1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'Calls_Reschedule', 'module', 90, 0),
('b543af2f-2c28-c096-ef9a-5388e3fa1bb2', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'Calls_Reschedule', 'module', 90, 0),
('b5ff256a-224d-bdc9-1b7b-5388e38e2c66', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'Calls_Reschedule', 'module', 90, 0),
('cbb9f62a-c4b6-1044-fba7-5388e34c69da', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'SecurityGroups', 'module', 89, 0),
('ccb3f6df-edbd-7053-a4f0-5388e3faa723', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'SecurityGroups', 'module', 90, 0),
('cdadfe02-b931-af01-2682-5388e324df74', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'SecurityGroups', 'module', 90, 0),
('ce6975b3-3599-0d30-7023-5388e3543dae', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'SecurityGroups', 'module', 90, 0),
('cf6384be-00cd-28ab-48fd-5388e39cfe1d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'SecurityGroups', 'module', 90, 0),
('d01f0b28-84d9-22e2-7adb-5388e30fb3b3', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'SecurityGroups', 'module', 90, 0),
('d0da8b0e-381e-700e-6a62-5388e382431f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'SecurityGroups', 'module', 90, 0),
('d1d48528-30ad-bec5-5fc3-5388e3f15511', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'SecurityGroups', 'module', 90, 0),
('e9066802-d10f-6901-d3c7-5388e35f47bf', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'jjwg_Maps', 'module', 89, 0),
('e9c1e118-63a0-3d70-aa32-5388e380ea30', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'jjwg_Maps', 'module', 90, 0),
('eabbeb29-3461-ca01-056c-5388e36348fe', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'jjwg_Maps', 'module', 90, 0),
('eb776ba2-a893-b16c-0482-5388e3f23d7a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'jjwg_Maps', 'module', 90, 0),
('ec32e1c9-f1d3-85eb-8668-5388e317c817', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'jjwg_Maps', 'module', 90, 0),
('ed2cfd25-1fc1-b362-0f5f-5388e34f9b40', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'jjwg_Maps', 'module', 90, 0),
('ede87e8b-c6fc-5759-ba2d-5388e38585c3', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'jjwg_Maps', 'module', 90, 0),
('eea3f27d-4525-96ac-8f1f-5388e361b200', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'jjwg_Maps', 'module', 90, 0),
('12abd104-5896-f8fc-0809-5388e336b17a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'jjwg_Markers', 'module', 89, 0),
('13a5de51-1b99-306a-7ea2-5388e38ccf88', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'jjwg_Markers', 'module', 90, 0),
('14615b3e-7e28-db6d-a8bc-5388e332b944', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'jjwg_Markers', 'module', 90, 0),
('151cd55a-d812-075c-a222-5388e391a7da', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'jjwg_Markers', 'module', 90, 0),
('15d8507a-87f3-69c3-4392-5388e3f2a04d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'jjwg_Markers', 'module', 90, 0),
('16d26955-031d-d195-c9b4-5388e39ac57d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'jjwg_Markers', 'module', 90, 0),
('178def0a-772a-526d-a01c-5388e31bd4e1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'jjwg_Markers', 'module', 90, 0),
('184964c0-570a-7520-ec28-5388e39c68b9', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'jjwg_Markers', 'module', 90, 0),
('2866ae76-03f7-7640-ea65-5388e3bc082a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'jjwg_Areas', 'module', 89, 0),
('2922287c-a4e7-acdf-838f-5388e313694e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'jjwg_Areas', 'module', 90, 0),
('29dda2a4-a983-042d-d3f6-5388e3d12f1d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'jjwg_Areas', 'module', 90, 0),
('2ad7a7e9-a2d4-225d-e6c0-5388e371660e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'jjwg_Areas', 'module', 90, 0),
('2b93206f-c300-2c38-ee17-5388e3d08765', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'jjwg_Areas', 'module', 90, 0),
('2c4eb557-24ef-5134-4ef2-5388e33498f1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'jjwg_Areas', 'module', 90, 0),
('2d0a3ab9-bc81-2954-34e7-5388e373abe1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'jjwg_Areas', 'module', 90, 0),
('2e04383e-db39-4550-9c2e-5388e3c346cf', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'jjwg_Areas', 'module', 90, 0),
('3b71e16e-69a6-7e05-7f64-5388e353b1fc', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'jjwg_Address_Cache', 'module', 89, 0),
('3c6bef33-dc43-3327-65df-5388e3411f9d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'jjwg_Address_Cache', 'module', 90, 0),
('3d277938-42a6-85a6-f232-5388e35b6807', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'jjwg_Address_Cache', 'module', 90, 0),
('3de2f06a-525b-594b-3260-5388e313fabd', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'jjwg_Address_Cache', 'module', 90, 0),
('3edcf5a1-59fd-ad99-abb1-5388e347eb16', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'jjwg_Address_Cache', 'module', 90, 0),
('3f987b66-d7ce-8b5b-5579-5388e3dca486', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'jjwg_Address_Cache', 'module', 90, 0),
('4053f86a-c197-5f8f-961f-5388e3279e2c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'jjwg_Address_Cache', 'module', 90, 0),
('410f8d8e-5d50-4f16-faa9-5388e360735b', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'jjwg_Address_Cache', 'module', 90, 0),
('52a3ceeb-518a-f10d-268c-5388e3a4fa45', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOP_Case_Events', 'module', 89, 0),
('539dc952-d280-1967-1fb1-5388e3747825', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOP_Case_Events', 'module', 90, 0),
('54594b34-5e62-129f-6d86-5388e30f3737', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOP_Case_Events', 'module', 90, 0),
('55534257-2a6a-84b2-8ed3-5388e3822a33', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOP_Case_Events', 'module', 90, 0),
('560ec519-2a6a-485a-1a0e-5388e38ec204', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOP_Case_Events', 'module', 90, 0),
('56ca5243-67a2-a4f2-9aec-5388e399f0fb', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOP_Case_Events', 'module', 90, 0),
('5785d8c5-5624-d6f2-3afc-5388e38bcc51', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOP_Case_Events', 'module', 90, 0),
('587fdbab-6fbd-5ec3-0f9b-5388e36543e4', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOP_Case_Events', 'module', 90, 0),
('6c082f12-6424-9f46-ea58-5388e354ae1f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOP_Case_Updates', 'module', 89, 0),
('6d022cdf-96c8-e61a-f6fb-5388e3eba4fe', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOP_Case_Updates', 'module', 90, 0),
('6dbda9a1-61bf-2198-9cf7-5388e34502ca', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOP_Case_Updates', 'module', 90, 0),
('6e792f09-f1d7-768a-269d-5388e31b09e8', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOP_Case_Updates', 'module', 90, 0),
('6f732a68-d065-d92a-aafc-5388e3075135', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOP_Case_Updates', 'module', 90, 0),
('706d37f3-4fab-c50d-90bf-5388e350114a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOP_Case_Updates', 'module', 90, 0),
('7128b2b7-4b8e-2b90-c7cc-5388e3b651c5', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOP_Case_Updates', 'module', 90, 0),
('71e4307b-6d7a-96f4-3311-5388e3005d10', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOP_Case_Updates', 'module', 90, 0),
('885a8214-4292-cd50-0ef9-5388e3a9e6da', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'FP_events', 'module', 89, 0),
('89548227-38d6-aaae-0e77-5388e301e92f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'FP_events', 'module', 90, 0),
('8a8d1f89-e0df-01b3-fec1-5388e3567292', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'FP_events', 'module', 90, 0),
('8b87181f-8aa5-00f0-7e25-5388e3ded2d9', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'FP_events', 'module', 90, 0),
('8d3c95cb-2a19-2ec2-6106-5388e311bdfa', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'FP_events', 'module', 90, 0),
('8f30a337-a4a3-6eab-1cef-5388e3ee9160', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'FP_events', 'module', 90, 0),
('8fec2118-d181-e0a6-93a4-5388e3f89290', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'FP_events', 'module', 90, 0),
('9124a657-9803-780a-e905-5388e3525144', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'FP_events', 'module', 90, 0),
('a79b0b97-2cf4-695e-b440-5388e34bac6c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'FP_Event_Locations', 'module', 89, 0),
('a85685f7-f5c4-568e-95eb-5388e317bc29', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'FP_Event_Locations', 'module', 90, 0),
('a9120eaf-a428-6b5b-ce36-5388e39c2d0c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'FP_Event_Locations', 'module', 90, 0),
('aa0c0063-2ee0-bf8b-7402-5388e355b10a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'FP_Event_Locations', 'module', 90, 0),
('aac78035-25cb-e88a-cc02-5388e33a4b23', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'FP_Event_Locations', 'module', 90, 0),
('ab8306dc-7bf0-e495-27aa-5388e3614b0e', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'FP_Event_Locations', 'module', 90, 0),
('ac3e9e75-7f68-6961-9713-5388e347ce60', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'FP_Event_Locations', 'module', 90, 0),
('ad38945b-21c6-2557-bb25-5388e35dd0e3', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'FP_Event_Locations', 'module', 90, 0),
('c331ed5e-0ebe-8f1f-f372-5388e3e8c39c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOR_Reports', 'module', 89, 0),
('c42be44d-1d30-4805-e881-5388e375c252', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOR_Reports', 'module', 90, 0),
('c4e768d5-295e-1deb-a6ef-5388e3d8a961', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOR_Reports', 'module', 90, 0),
('c5a2f841-8b74-196e-b6a1-5388e3fe01f5', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOR_Reports', 'module', 90, 0),
('c65e71da-5c7b-935a-979e-5388e3f8dfc0', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOR_Reports', 'module', 90, 0),
('c7587f91-c7cf-5630-53d5-5388e334001f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOR_Reports', 'module', 90, 0),
('c813f38c-f8c5-4a25-1260-5388e3d6a70b', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOR_Reports', 'module', 90, 0),
('c8cf799f-0552-1047-20bc-5388e3b63c41', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOR_Reports', 'module', 90, 0),
('1a028a60-2918-2e8e-6ea9-5388e327c49f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOW_WorkFlow', 'module', 89, 0),
('1abe0da2-c0a6-5a35-64a4-5388e3dcbeab', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOW_WorkFlow', 'module', 90, 0),
('1b798d10-209a-1ff4-3aa6-5388e3887754', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOW_WorkFlow', 'module', 90, 0),
('1c738a9b-d4f7-1bf6-5948-5388e3687f98', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOW_WorkFlow', 'module', 90, 0),
('1d2f0e2a-e03a-77ce-3731-5388e3917f1d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOW_WorkFlow', 'module', 90, 0),
('1dea86fc-ceca-2fe0-ea97-5388e3d1ea88', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOW_WorkFlow', 'module', 90, 0),
('1ea61fa4-473f-066b-6c6e-5388e388312b', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOW_WorkFlow', 'module', 90, 0),
('1f619004-0ff4-e00c-f12c-5388e3372747', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOW_WorkFlow', 'module', 90, 0),
('3460ec46-8f1d-8509-7055-5388e3c842b2', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOW_Processed', 'module', 89, 0),
('351c6e5d-db5e-63ec-7442-5388e393c497', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOW_Processed', 'module', 90, 0),
('35d7ef38-de0d-ca48-0144-5388e3ba6b3d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOW_Processed', 'module', 90, 0),
('36d1e6c6-7f4b-8151-d65c-5388e3091bd4', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOW_Processed', 'module', 90, 0),
('378d64af-9999-1090-0181-5388e3eaf8ae', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOW_Processed', 'module', 90, 0),
('3848f79a-5d0e-30db-6a34-5388e3f56dc8', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOW_Processed', 'module', 90, 0),
('39047290-f281-9e01-720e-5388e3292fb0', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOW_Processed', 'module', 90, 0),
('39fe7c21-f669-b240-c745-5388e3d820e5', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOW_Processed', 'module', 90, 0),
('53a15fa3-b4c2-15a1-e152-5388e304bbeb', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOS_Contracts', 'module', 89, 0),
('549b5f42-8a82-8f5d-c35e-5388e3d64b23', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOS_Contracts', 'module', 90, 0),
('5556dc0c-1943-5a5e-3f67-5388e331c5c6', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOS_Contracts', 'module', 90, 0),
('56126f74-3d30-d855-4f9d-5388e36c2603', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOS_Contracts', 'module', 90, 0),
('570c6927-32b4-44e0-3cfe-5388e3fd6f0a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOS_Contracts', 'module', 90, 0),
('58066377-1531-57c2-af74-5388e355add7', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOS_Contracts', 'module', 90, 0),
('58c1e3cb-9c64-2851-2fc3-5388e3b5dd02', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOS_Contracts', 'module', 90, 0),
('597d627d-3d4d-2ece-0ce0-5388e3f65227', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOS_Contracts', 'module', 90, 0),
('7070cf64-065c-3978-6ce0-5388e3b251bc', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOS_Invoices', 'module', 89, 0),
('716ac58a-24f6-04bd-050c-5388e33f6671', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOS_Invoices', 'module', 90, 0),
('7226416b-e842-a65b-9a9e-5388e337b2c2', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOS_Invoices', 'module', 90, 0),
('72e1ce3b-8d42-0753-0f83-5388e385b72c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOS_Invoices', 'module', 90, 0),
('739d538b-9eb2-b147-f282-5388e365370a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOS_Invoices', 'module', 90, 0),
('74975cf8-3a8a-8f75-b17b-5388e3e0d93a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOS_Invoices', 'module', 90, 0),
('7552d726-47a6-ebde-a2c5-5388e350bb13', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOS_Invoices', 'module', 90, 0),
('760e5181-ea66-5585-13cc-5388e3515674', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOS_Invoices', 'module', 90, 0),
('9260ca1f-878a-e1e2-224e-5388e38f9a77', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOS_PDF_Templates', 'module', 89, 0),
('931c41c0-9a83-03ab-c5d2-5388e3b4870f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOS_PDF_Templates', 'module', 90, 0),
('93d7ca8a-f4a6-a852-4ae2-5388e35baec6', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOS_PDF_Templates', 'module', 90, 0),
('94d1c816-8cc3-c68d-ecc2-5388e3eeb2f2', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOS_PDF_Templates', 'module', 90, 0),
('958d52ab-e080-7272-beb9-5388e3078a78', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOS_PDF_Templates', 'module', 90, 0),
('9648d060-cb44-2952-2e45-5388e30c46e1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOS_PDF_Templates', 'module', 90, 0),
('9742da8e-b8ca-c314-b3bd-5388e3124883', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOS_PDF_Templates', 'module', 90, 0),
('97fe536d-8ffa-386c-e11b-5388e39212f5', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOS_PDF_Templates', 'module', 90, 0),
('ae3625cb-ca31-9888-ec3f-5388e30690e8', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOS_Product_Categories', 'module', 89, 0),
('aef1af52-d672-96c4-0f5e-5388e3f49840', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOS_Product_Categories', 'module', 90, 0),
('afebb880-6b33-1680-5e8d-5388e342b226', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOS_Product_Categories', 'module', 90, 0),
('b0a73fda-bbb3-4227-4c45-5388e3922e1a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOS_Product_Categories', 'module', 90, 0),
('b162bbcb-910f-4f33-f3bf-5388e3bb1028', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOS_Product_Categories', 'module', 90, 0),
('b21e3140-1917-4a62-48c0-5388e37cf175', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOS_Product_Categories', 'module', 90, 0),
('b2d9b55f-cc99-d68a-7251-5388e343067c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOS_Product_Categories', 'module', 90, 0),
('b3d3c6c4-7be0-e078-7f97-5388e31bab46', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOS_Product_Categories', 'module', 90, 0),
('cbff9af6-350b-b51c-45e0-5388e3cd7c80', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOS_Products', 'module', 89, 0),
('ccbb1ffe-2458-3c86-0985-5388e3b78391', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOS_Products', 'module', 90, 0),
('cdb525af-f5d6-ab0f-b038-5388e376e145', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOS_Products', 'module', 90, 0),
('ce70a52e-447f-5478-d7c4-5388e3be0824', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOS_Products', 'module', 90, 0),
('cf2c2cae-6cd9-7e5b-c63a-5388e345034a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOS_Products', 'module', 90, 0),
('cfe7a72d-84ea-c5c6-0db1-5388e3a33685', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOS_Products', 'module', 90, 0),
('d0a32608-039d-4d14-aba4-5388e3cf9676', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOS_Products', 'module', 90, 0),
('d15eb97c-e0a6-e9e6-c194-5388e370a98f', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOS_Products', 'module', 90, 0),
('2197b8aa-4318-3c41-2b6b-5388e3a56728', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'access', 'AOS_Quotes', 'module', 89, 0),
('2291bb88-5bfa-3d45-5420-5388e317aca0', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'view', 'AOS_Quotes', 'module', 90, 0),
('234d3153-afd4-5ff4-f600-5388e392f5bd', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'list', 'AOS_Quotes', 'module', 90, 0),
('2408b78c-0d78-3720-5323-5388e361bee4', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'edit', 'AOS_Quotes', 'module', 90, 0),
('24c43d1f-7267-8002-aab9-5388e39cddba', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'delete', 'AOS_Quotes', 'module', 90, 0),
('25be439a-1ab3-a5ab-1237-5388e31cbd3c', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'import', 'AOS_Quotes', 'module', 90, 0),
('2679c135-4cb7-b6d6-533b-5388e382f4d1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'export', 'AOS_Quotes', 'module', 90, 0),
('273542a8-d9f2-cfe0-b3ba-5388e34d8746', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '', 'massupdate', 'AOS_Quotes', 'module', 90, 0),
('201c19ea-88ff-3957-3bea-5388ecbb5b60', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'access', 'prov1_Aziende', 'module', 89, 1),
('230a2bc6-29db-c1f3-da56-5388ecfa19e4', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'view', 'prov1_Aziende', 'module', 90, 1),
('24fe2835-b875-e5e7-f502-5388ec0b02dc', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'list', 'prov1_Aziende', 'module', 90, 1),
('26f23936-08aa-8676-0116-5388ecfef22a', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'edit', 'prov1_Aziende', 'module', 90, 1),
('28e63094-3b5f-33fc-93d4-5388ecd86b55', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'delete', 'prov1_Aziende', 'module', 90, 1),
('2ada43d2-20ce-33de-dfc5-5388eca7f227', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'import', 'prov1_Aziende', 'module', 90, 1),
('2c8fcc03-8e1a-f6fd-7550-5388ecdece6c', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'export', 'prov1_Aziende', 'module', 90, 1),
('2e83d8cc-5040-629e-1eaa-5388ec22c671', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'massupdate', 'prov1_Aziende', 'module', 90, 1),
('472ca6ee-6f02-d5f4-8054-5388ec98bab5', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'access', 'prov1_Dipendenti', 'module', 89, 1),
('4920bf14-a7ce-ab7c-feb5-5388ec6947cb', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'view', 'prov1_Dipendenti', 'module', 90, 1),
('4b533780-45e7-be0f-08bc-5388ec034954', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'list', 'prov1_Dipendenti', 'module', 90, 1),
('4d08c03c-ccb3-a11e-1107-5388ec733e41', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'edit', 'prov1_Dipendenti', 'module', 90, 1),
('4f3b42e6-a58a-7cc4-ffd6-5388ec34b55b', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'delete', 'prov1_Dipendenti', 'module', 90, 1),
('50f0d8a5-1ba9-9e7c-8c90-5388ec43a2a4', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'import', 'prov1_Dipendenti', 'module', 90, 1),
('52e4d51f-50d7-d9fe-32de-5388ec504bdb', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'export', 'prov1_Dipendenti', 'module', 90, 1),
('54d8ec33-cda4-0114-6ba2-5388ecd73e6c', '2014-05-30 20:40:26', '2014-05-31 09:02:10', '1', '1', 'massupdate', 'prov1_Dipendenti', 'module', 90, 1),
('630dd0d6-ba43-2df1-91bb-538995ec5eaf', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'access', 'prov2_Aziende_A', 'module', 89, 1),
('70f89120-16bd-a31a-5f92-53899568ce0e', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'view', 'prov2_Aziende_A', 'module', 90, 1),
('7f9ecacc-d4fe-b961-1a03-53899552106c', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'list', 'prov2_Aziende_A', 'module', 90, 1),
('8192d391-41e0-1426-83d9-538995fc2ae7', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'edit', 'prov2_Aziende_A', 'module', 90, 1),
('8386d21a-7864-eb15-3634-53899509700b', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'delete', 'prov2_Aziende_A', 'module', 90, 1),
('857ad1c9-27a3-b4be-6000-5389953624e6', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'import', 'prov2_Aziende_A', 'module', 90, 1),
('876eeab1-23b4-64f1-9ca7-53899517b998', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'export', 'prov2_Aziende_A', 'module', 90, 1),
('8962e449-1b70-c3a6-8c93-538995ca5e9d', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'massupdate', 'prov2_Aziende_A', 'module', 90, 1),
('15abc1d6-343b-3e24-fa55-5389953c9c84', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'access', 'prov2_Dipendenti_A', 'module', 89, 1),
('179fdd50-0d11-4da2-fa38-538995f0a1cc', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'view', 'prov2_Dipendenti_A', 'module', 90, 1),
('1993d11e-e181-9630-4a77-5389955d3502', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'list', 'prov2_Dipendenti_A', 'module', 90, 1),
('1b87ef91-7da4-001c-ecdf-538995b8c2a1', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'edit', 'prov2_Dipendenti_A', 'module', 90, 1),
('1d7bef6b-9fd5-5491-7e96-538995e3c580', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'delete', 'prov2_Dipendenti_A', 'module', 90, 1),
('1f6feac8-ebf4-28ac-88ce-538995f9398f', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'import', 'prov2_Dipendenti_A', 'module', 90, 1),
('2163f6d8-3c98-d983-ca9d-538995baba81', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'export', 'prov2_Dipendenti_A', 'module', 90, 1),
('235804f4-ae19-977a-1466-5389954edaf6', '2014-05-31 08:41:39', '2014-05-31 08:48:13', '1', '1', 'massupdate', 'prov2_Dipendenti_A', 'module', 90, 1),
('21b3efbc-3e82-4578-42f8-538997bd664e', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'access', 'prov2_Aziende_A', 'module', 89, 1),
('24637246-c0f8-32ce-733f-538997d92dc2', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'view', 'prov2_Aziende_A', 'module', 90, 1),
('265774ac-9800-2389-95ab-538997fe05e0', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'list', 'prov2_Aziende_A', 'module', 90, 1),
('288a079c-4c45-cdfd-1f19-5389970a8c4b', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'edit', 'prov2_Aziende_A', 'module', 90, 1),
('2a7e0363-0577-b374-c327-538997ea3408', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'delete', 'prov2_Aziende_A', 'module', 90, 1),
('2c72147f-f733-bd9a-c95b-53899756dbda', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'import', 'prov2_Aziende_A', 'module', 90, 1),
('2e279512-af2a-d6fb-4230-538997f6f5de', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'export', 'prov2_Aziende_A', 'module', 90, 1),
('305a225b-26cd-16cd-0ab4-53899757157c', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'massupdate', 'prov2_Aziende_A', 'module', 90, 1),
('449df0f1-6e2a-71e0-1d32-538997a1fae3', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'access', 'prov2_Dipendenti_A', 'module', 89, 1),
('4691f7ae-a5d5-67a1-7f23-53899711afea', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'view', 'prov2_Dipendenti_A', 'module', 90, 1),
('4885f17d-c4b7-f50f-3af4-5389979f5535', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'list', 'prov2_Dipendenti_A', 'module', 90, 1);
INSERT INTO `acl_actions` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `name`, `category`, `acltype`, `aclaccess`, `deleted`) VALUES
('4a7a0232-616d-d704-b159-538997f22538', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'edit', 'prov2_Dipendenti_A', 'module', 90, 1),
('4cac8efc-8dee-dbc6-82ba-538997636acf', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'delete', 'prov2_Dipendenti_A', 'module', 90, 1),
('4e621ada-0c71-f877-a9de-53899753e2a3', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'import', 'prov2_Dipendenti_A', 'module', 90, 1),
('50949d53-0184-9120-933d-538997a55a0a', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'export', 'prov2_Dipendenti_A', 'module', 90, 1),
('524a21a3-1d85-e6e4-167b-53899717d9ee', '2014-05-31 08:48:29', '2014-05-31 08:50:37', '1', '1', 'massupdate', 'prov2_Dipendenti_A', 'module', 90, 1),
('d4b632bc-2e58-ebde-5c20-538997bd2a25', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'access', 'prov2_Aziende_A', 'module', 89, 1),
('d7a44772-a1e4-15d6-9051-5389973e2e1c', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'view', 'prov2_Aziende_A', 'module', 90, 1),
('d9985374-fd69-39de-c3dc-538997f94466', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'list', 'prov2_Aziende_A', 'module', 90, 1),
('db8c5407-f45f-d8bd-e9ed-5389971a49a8', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'edit', 'prov2_Aziende_A', 'module', 90, 1),
('dd80555f-8445-bc61-bf42-538997a5f5d7', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'delete', 'prov2_Aziende_A', 'module', 90, 1),
('df746332-66d8-5b81-bd23-5389970fb5c4', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'import', 'prov2_Aziende_A', 'module', 90, 1),
('e1a6ea3d-d9d2-b1ae-c8a5-53899791daa2', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'export', 'prov2_Aziende_A', 'module', 90, 1),
('e39af705-4c1b-57d9-623e-53899770bc14', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'massupdate', 'prov2_Aziende_A', 'module', 90, 1),
('725c00ae-41bb-53ae-8fa7-538997108004', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'access', 'prov2_Dipendenti_A', 'module', 89, 1),
('9585028c-e3c4-5388-a867-538997d646ac', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'view', 'prov2_Dipendenti_A', 'module', 90, 1),
('b8ad0643-b94e-7d13-281a-53899789f0a3', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'list', 'prov2_Dipendenti_A', 'module', 90, 1),
('d7ee0b6c-1b01-8550-4c74-538997dc0bbe', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'edit', 'prov2_Dipendenti_A', 'module', 90, 1),
('f72e0001-082c-a45b-9f66-5389974e7006', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'delete', 'prov2_Dipendenti_A', 'module', 90, 1),
('1166fe33-271d-ff0f-cd0a-53899744498a', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'import', 'prov2_Dipendenti_A', 'module', 90, 1),
('135afe2c-e8ee-4128-8db0-538997038f0f', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'export', 'prov2_Dipendenti_A', 'module', 90, 1),
('154f0a5b-3e59-bb6c-41cd-538997cd1d75', '2014-05-31 08:50:53', '2014-05-31 08:53:13', '1', '1', 'massupdate', 'prov2_Dipendenti_A', 'module', 90, 1),
('d11d4d00-83e4-79a1-b598-538998562bbe', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'access', 'prov2_Aziende_A', 'module', 89, 1),
('d449d5d1-fe42-a8fb-9d1b-538998e2e026', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'view', 'prov2_Aziende_A', 'module', 90, 1),
('d67c6a5d-2b4c-a6d7-a599-538998a703de', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'list', 'prov2_Aziende_A', 'module', 90, 1),
('d8706257-2d5e-cf88-87dd-538998f5de82', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'edit', 'prov2_Aziende_A', 'module', 90, 1),
('daa2fd5b-83e0-695b-145d-538998f06517', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'delete', 'prov2_Aziende_A', 'module', 90, 1),
('dcd5796f-ee8e-3d35-d03b-538998408231', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'import', 'prov2_Aziende_A', 'module', 90, 1),
('dec98626-aff9-759d-7cb5-538998749158', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'export', 'prov2_Aziende_A', 'module', 90, 1),
('e0fc04bd-1f8a-ba56-3d0d-538998655582', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'massupdate', 'prov2_Aziende_A', 'module', 90, 1),
('6f7e0d75-2944-5f67-03ae-5389981a259c', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'access', 'prov2_Dipendenti_A', 'module', 89, 1),
('92a70c47-aae4-61d4-0e01-5389986c2557', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'view', 'prov2_Dipendenti_A', 'module', 90, 1),
('b1e70e51-2d94-e624-569a-53899871ac59', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'list', 'prov2_Dipendenti_A', 'module', 90, 1),
('d5100a50-b582-9dd1-dc1a-53899839163a', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'edit', 'prov2_Dipendenti_A', 'module', 90, 1),
('f4500555-7c1c-051f-3912-538998d9e80c', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'delete', 'prov2_Dipendenti_A', 'module', 90, 1),
('11391692-98ea-4c81-404c-53899801e0ff', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'import', 'prov2_Dipendenti_A', 'module', 90, 1),
('136b9338-c288-4c2c-e136-5389984ade68', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'export', 'prov2_Dipendenti_A', 'module', 90, 1),
('155fadf2-e0ba-06f1-53a7-53899830df47', '2014-05-31 08:53:35', '2014-05-31 08:55:32', '1', '1', 'massupdate', 'prov2_Dipendenti_A', 'module', 90, 1),
('1337d3e6-4a39-940c-9d45-53899a531e93', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'access', 'prov1_Aziende', 'module', 89, 1),
('1625ef18-c396-ddf0-c2d5-53899a120a02', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'view', 'prov1_Aziende', 'module', 90, 1),
('1819ed72-8891-9f38-8dc5-53899a9e770b', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'list', 'prov1_Aziende', 'module', 90, 1),
('19cf7863-fd7b-19a5-9c1b-53899ac974d1', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'edit', 'prov1_Aziende', 'module', 90, 1),
('1bc37442-8258-c555-42a4-53899aef2ace', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'delete', 'prov1_Aziende', 'module', 90, 1),
('1db786fb-7da5-5b6d-f0d8-53899ad792ef', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'import', 'prov1_Aziende', 'module', 90, 1),
('1f6d0df1-80fb-0f82-3f70-53899ac13c90', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'export', 'prov1_Aziende', 'module', 90, 1),
('2161133e-c724-3ed5-7a1b-53899a8ba52d', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'massupdate', 'prov1_Aziende', 'module', 90, 1),
('4063032b-1b28-a81d-80d4-53899a74bce0', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'access', 'prov1_Dipendenti', 'module', 89, 1),
('42958ca3-f74a-9583-d3a0-53899a41cb6e', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'view', 'prov1_Dipendenti', 'module', 90, 1),
('448993c9-206e-5937-b740-53899acbd57f', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'list', 'prov1_Dipendenti', 'module', 90, 1),
('467d9747-4e57-ead8-5699-53899a4fc3d3', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'edit', 'prov1_Dipendenti', 'module', 90, 1),
('4871aa59-9d7f-852f-ddda-53899a66835d', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'delete', 'prov1_Dipendenti', 'module', 90, 1),
('4a65ac4c-7310-142b-eb83-53899a32833d', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'import', 'prov1_Dipendenti', 'module', 90, 1),
('4c983d08-5c06-d3ae-b73f-53899a0b4cde', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'export', 'prov1_Dipendenti', 'module', 90, 1),
('4e8c3086-8519-23a9-c708-53899a01f93d', '2014-05-31 09:02:40', '2014-05-31 10:23:41', '1', '1', 'massupdate', 'prov1_Dipendenti', 'module', 90, 1),
('68066d58-fb68-ff82-fb66-5389ad82fa49', '2014-05-31 10:24:05', '2014-06-01 08:25:20', '1', '1', 'access', 'prov1_Aziende', 'module', 89, 1),
('6af47610-0e84-93bb-95af-5389ad641f60', '2014-05-31 10:24:05', '2014-06-01 08:25:20', '1', '1', 'view', 'prov1_Aziende', 'module', 90, 1),
('6ce874e0-af64-7dd6-7f8b-5389ad1b80a8', '2014-05-31 10:24:05', '2014-06-01 08:25:20', '1', '1', 'list', 'prov1_Aziende', 'module', 90, 1),
('6e9e04aa-740c-9736-0b51-5389ad4985c0', '2014-05-31 10:24:05', '2014-06-01 08:25:20', '1', '1', 'edit', 'prov1_Aziende', 'module', 90, 1),
('70920a01-f6e5-0c70-61b2-5389ad8c323f', '2014-05-31 10:24:05', '2014-06-01 08:25:20', '1', '1', 'delete', 'prov1_Aziende', 'module', 90, 1),
('7286106a-6ed1-a079-439b-5389ad8624ba', '2014-05-31 10:24:05', '2014-06-01 08:25:20', '1', '1', 'import', 'prov1_Aziende', 'module', 90, 1),
('747a11d3-ed54-9957-99ee-5389ada967e6', '2014-05-31 10:24:05', '2014-06-01 08:25:20', '1', '1', 'export', 'prov1_Aziende', 'module', 90, 1),
('766e17dd-ddcc-ecfb-f9ab-5389ad4714eb', '2014-05-31 10:24:05', '2014-06-01 08:25:20', '1', '1', 'massupdate', 'prov1_Aziende', 'module', 90, 1),
('cce2000c-3d25-c3e6-bce4-538ae3b8fa52', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'access', 'comp1_A_Aziende', 'module', 89, 1),
('19bed7ba-d006-8c71-08a9-538ae347704b', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'view', 'comp1_A_Aziende', 'module', 90, 1),
('1bb2ef4c-f6fe-1649-de6b-538ae38a9fc5', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'list', 'comp1_A_Aziende', 'module', 90, 1),
('1da6e694-6211-9db0-e397-538ae35d5286', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'edit', 'comp1_A_Aziende', 'module', 90, 1),
('1f9aef0c-0581-3f1f-4f02-538ae39ff9ec', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'delete', 'comp1_A_Aziende', 'module', 90, 1),
('218ef2a8-5c89-bbc6-5861-538ae3745588', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'import', 'comp1_A_Aziende', 'module', 90, 1),
('2382fbb4-3d54-2255-526c-538ae3d9a3bf', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'export', 'comp1_A_Aziende', 'module', 90, 1),
('25770870-727c-a915-0877-538ae3482959', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'massupdate', 'comp1_A_Aziende', 'module', 90, 1),
('4d81906a-8e52-32d3-60fc-538ae376cd13', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'access', 'comp1_A_Dipendenti', 'module', 89, 1),
('4fb4297c-befe-6354-162f-538ae377a9a6', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'view', 'comp1_A_Dipendenti', 'module', 90, 1),
('51a822ca-6e62-533f-0e5a-538ae36ef78b', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'list', 'comp1_A_Dipendenti', 'module', 90, 1),
('539c3f23-c4f2-cf3b-0caa-538ae3b4caf6', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'edit', 'comp1_A_Dipendenti', 'module', 90, 1),
('559030ca-9ad5-a760-8ba9-538ae31b7817', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'delete', 'comp1_A_Dipendenti', 'module', 90, 1),
('57844902-08fe-ae87-da5a-538ae35fbb0a', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'import', 'comp1_A_Dipendenti', 'module', 90, 1),
('59784d2c-4dd6-248c-27b4-538ae3ba9f53', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'export', 'comp1_A_Dipendenti', 'module', 90, 1),
('5b6c5ae8-ba25-74ec-a95b-538ae3a7708f', '2014-06-01 08:24:27', '2014-06-01 08:24:42', '1', '1', 'massupdate', 'comp1_A_Dipendenti', 'module', 90, 1),
('2df00714-0443-d93b-d532-538ae37de8c5', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'access', 'vrt1_Azienda', 'module', 89, 1),
('31c006a4-ebd9-3064-1e60-538ae31763d5', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'view', 'vrt1_Azienda', 'module', 90, 1),
('54e80260-9c55-b76d-eaf1-538ae341fb66', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'list', 'vrt1_Azienda', 'module', 90, 1),
('742908d3-a99e-af62-726e-538ae34b8b99', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'edit', 'vrt1_Azienda', 'module', 90, 1),
('93690733-bf20-243e-4ff8-538ae3a17f9c', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'delete', 'vrt1_Azienda', 'module', 90, 1),
('aec201f8-4162-e29d-edbf-538ae310f418', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'import', 'vrt1_Azienda', 'module', 90, 1),
('ce020f52-0d85-a58f-ded0-538ae37b108d', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'export', 'vrt1_Azienda', 'module', 90, 1),
('f12b0b2e-36ee-9295-2f39-538ae3e22dfe', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'massupdate', 'vrt1_Azienda', 'module', 90, 1),
('25c789ed-5fe9-81a5-f20e-538ae30e3afe', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'access', 'vrt1_Impiegato', 'module', 89, 1),
('27fa0b97-3051-0601-368a-538ae3d87479', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'view', 'vrt1_Impiegato', 'module', 90, 1),
('29ee1584-d894-3d15-8310-538ae38d173f', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'list', 'vrt1_Impiegato', 'module', 90, 1),
('2be21c42-c800-6223-5eb4-538ae3e36e5d', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'edit', 'vrt1_Impiegato', 'module', 90, 1),
('2dd625ec-756d-395a-3b4c-538ae34d2ecd', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'delete', 'vrt1_Impiegato', 'module', 90, 1),
('2fca28e2-cefd-62d1-139a-538ae3765fde', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'import', 'vrt1_Impiegato', 'module', 90, 1),
('31be358a-fffb-1269-6096-538ae3c91895', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'export', 'vrt1_Impiegato', 'module', 90, 1),
('33b23599-5092-18ee-681e-538ae3009606', '2014-06-01 08:25:07', '2014-06-01 08:25:20', '1', '1', 'massupdate', 'vrt1_Impiegato', 'module', 90, 1),
('33dc2ee9-eb19-c823-7cba-538ae621ce1b', '2014-06-01 08:38:09', '2014-06-04 21:27:07', '1', '1', 'access', 'prov2_Aziende', 'module', 89, 1),
('368bab66-bdf8-4ee5-8675-538ae6c43c89', '2014-06-01 08:38:09', '2014-06-04 21:27:07', '1', '1', 'view', 'prov2_Aziende', 'module', 90, 1),
('387fb843-2498-e7d8-08c1-538ae6078dd8', '2014-06-01 08:38:09', '2014-06-04 21:27:07', '1', '1', 'list', 'prov2_Aziende', 'module', 90, 1),
('3a73bb63-6ccf-6fe1-df14-538ae6dade19', '2014-06-01 08:38:09', '2014-06-04 21:27:07', '1', '1', 'edit', 'prov2_Aziende', 'module', 90, 1),
('3ca6411f-324e-ada9-9e02-538ae6d0b87c', '2014-06-01 08:38:09', '2014-06-04 21:27:07', '1', '1', 'delete', 'prov2_Aziende', 'module', 90, 1),
('3e9a3921-dac3-3b10-efee-538ae635b652', '2014-06-01 08:38:09', '2014-06-04 21:27:07', '1', '1', 'import', 'prov2_Aziende', 'module', 90, 1),
('404fd9ec-d31e-b5bb-afef-538ae68591d1', '2014-06-01 08:38:09', '2014-06-04 21:27:07', '1', '1', 'export', 'prov2_Aziende', 'module', 90, 1),
('4243d765-cb82-9fb0-e125-538ae6569ba0', '2014-06-01 08:38:09', '2014-06-04 21:27:07', '1', '1', 'massupdate', 'prov2_Aziende', 'module', 90, 1),
('6c3330fb-d570-cf33-d1e9-538ae96700ff', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'access', 'prov2_az1', 'module', 89, 1),
('6f2138b5-4ae0-9497-8e3d-538ae9867c6e', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'view', 'prov2_az1', 'module', 90, 1),
('71154f08-21a8-6259-e222-538ae97089ad', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'list', 'prov2_az1', 'module', 90, 1),
('73094523-33aa-bd55-1171-538ae9d16f9d', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'edit', 'prov2_az1', 'module', 90, 1),
('74bedeec-1828-1a1f-d972-538ae9b3c7d4', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'delete', 'prov2_az1', 'module', 90, 1),
('76f1554b-b681-c14c-fbb4-538ae9e0e640', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'import', 'prov2_az1', 'module', 90, 1),
('78a6d81c-0f3e-9fd4-fae7-538ae90b560c', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'export', 'prov2_az1', 'module', 90, 1),
('7a9ae56f-76f6-8ade-0552-538ae96d445b', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'massupdate', 'prov2_az1', 'module', 90, 1),
('90d2b1e2-0aa0-971c-d211-538ae99443b2', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'access', 'prov2_dip1', 'module', 89, 1),
('92c6c771-8687-a7a0-3816-538ae9da19a0', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'view', 'prov2_dip1', 'module', 90, 1),
('94f942fa-418b-c5c9-c2ae-538ae97a017a', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'list', 'prov2_dip1', 'module', 90, 1),
('96ed58f6-7c06-61d3-953e-538ae95f1f21', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'edit', 'prov2_dip1', 'module', 90, 1),
('98e15285-f2a5-8f53-97f0-538ae9a18e5b', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'delete', 'prov2_dip1', 'module', 90, 1),
('9ad55b38-8557-a1d2-1862-538ae96e113e', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'import', 'prov2_dip1', 'module', 90, 1),
('9cc9652a-4c15-6786-606c-538ae940b1d6', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'export', 'prov2_dip1', 'module', 90, 1),
('9ebd6bca-cc00-5847-fa07-538ae9dd6e51', '2014-06-01 08:50:54', '2014-06-04 21:27:07', '1', '1', 'massupdate', 'prov2_dip1', 'module', 90, 1),
('46d572e6-f321-9dbb-49ba-538f91d76594', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'access', 'prov1_C_Azienda', 'module', 89, 1),
('4d2e8747-3e27-2565-cf6f-538f9105daba', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'view', 'prov1_C_Azienda', 'module', 90, 1),
('4f229083-974a-1bd6-027a-538f915a3164', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'list', 'prov1_C_Azienda', 'module', 90, 1),
('511691e9-a218-d301-2b13-538f9117fcc9', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'edit', 'prov1_C_Azienda', 'module', 90, 1),
('530a9a63-6c1f-1178-b11e-538f91522f78', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'delete', 'prov1_C_Azienda', 'module', 90, 1),
('54feaa09-7cd2-8342-ef99-538f919a3223', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'import', 'prov1_C_Azienda', 'module', 90, 1),
('56f2a43c-8f3c-eac8-df5a-538f91810445', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'export', 'prov1_C_Azienda', 'module', 90, 1),
('58e6b6e9-9601-7717-0575-538f9122316d', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'massupdate', 'prov1_C_Azienda', 'module', 90, 1),
('718f9cf6-444e-2598-3c64-538f91ffa9f1', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'access', 'prov1_C_Dipendenti', 'module', 89, 1),
('73c21008-ad5a-f5d0-7035-538f9189a043', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'view', 'prov1_C_Dipendenti', 'module', 90, 1),
('75b62156-5c2a-594c-4bf2-538f91404e85', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'list', 'prov1_C_Dipendenti', 'module', 90, 1),
('77aa2b48-6c84-6a27-cfd7-538f913c8ea0', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'edit', 'prov1_C_Dipendenti', 'module', 90, 1),
('79dcbc38-65a3-9a13-0ff1-538f9154cfca', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'delete', 'prov1_C_Dipendenti', 'module', 90, 1),
('7bd0b96e-a311-0151-ce8e-538f912b057f', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'import', 'prov1_C_Dipendenti', 'module', 90, 1),
('7dc4baf5-f1a2-1d37-1357-538f910f8477', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'export', 'prov1_C_Dipendenti', 'module', 90, 1),
('7fb8b03b-c863-e810-fc5d-538f912f5c09', '2014-06-04 21:36:27', '2014-06-04 22:26:38', '1', '1', 'massupdate', 'prov1_C_Dipendenti', 'module', 90, 1),
('331a03e7-4655-988b-79d1-538f9d9ab97a', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'access', 'prov1_C_Azienda', 'module', 89, 1),
('61fa011a-10b9-2ced-ed87-538f9d2f4b00', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'view', 'prov1_C_Azienda', 'module', 90, 1),
('813b0684-5ddd-a1dc-4e18-538f9d30b828', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'list', 'prov1_C_Azienda', 'module', 90, 1),
('a07b0346-e709-6815-06f5-538f9d253e32', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'edit', 'prov1_C_Azienda', 'module', 90, 1),
('bfbc00ba-a4ab-e280-25a4-538f9d02db1c', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'delete', 'prov1_C_Azienda', 'module', 90, 1),
('defc0b3a-e369-1863-2a19-538f9d949998', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'import', 'prov1_C_Azienda', 'module', 90, 1),
('10225872-3053-9cea-a3ec-538f9d8d4341', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'export', 'prov1_C_Azienda', 'module', 90, 1),
('12165fef-7e3a-9eab-9853-538f9d628707', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'massupdate', 'prov1_C_Azienda', 'module', 90, 1),
('2cb339b2-32a5-daac-41fe-538f9da926d0', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'access', 'prov1_C_Dipendenti', 'module', 89, 1),
('2ee5c050-de56-60aa-979e-538f9df06516', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'view', 'prov1_C_Dipendenti', 'module', 90, 1),
('30d9c9af-7618-7994-d894-538f9db145d7', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'list', 'prov1_C_Dipendenti', 'module', 90, 1),
('330c518e-b0b2-b06f-f83f-538f9da892d1', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'edit', 'prov1_C_Dipendenti', 'module', 90, 1),
('35005809-36e5-2241-273d-538f9df89d3a', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'delete', 'prov1_C_Dipendenti', 'module', 90, 1),
('36f46f2e-a003-e825-80bc-538f9d459429', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'import', 'prov1_C_Dipendenti', 'module', 90, 1),
('38e8614c-d5df-ff96-dc69-538f9db2f75a', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'export', 'prov1_C_Dipendenti', 'module', 90, 1),
('3adc7e11-dd3c-f921-45b8-538f9d9ee780', '2014-06-04 22:26:50', '2014-06-04 22:46:04', '1', '1', 'massupdate', 'prov1_C_Dipendenti', 'module', 90, 1),
('7e61c892-cdf4-ff67-daf4-538fa18d9e51', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'access', 'prov1_C_Azienda', 'module', 89, 0),
('814fde8f-2fb6-5ca1-14df-538fa166bcb3', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'view', 'prov1_C_Azienda', 'module', 90, 0),
('8343d1d6-2835-5ed7-aeb8-538fa18284fa', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'list', 'prov1_C_Azienda', 'module', 90, 0),
('857668b6-5bed-f1c8-93cf-538fa10690a3', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'edit', 'prov1_C_Azienda', 'module', 90, 0),
('876a63fa-bfa4-f4f6-8504-538fa17d1085', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'delete', 'prov1_C_Azienda', 'module', 90, 0),
('899cec4c-5e19-ed9e-1292-538fa193d812', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'import', 'prov1_C_Azienda', 'module', 90, 0),
('8b90f16c-f660-97d5-a539-538fa1c79c33', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'export', 'prov1_C_Azienda', 'module', 90, 0),
('8d84fd71-0365-e03c-8573-538fa1388a3e', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'massupdate', 'prov1_C_Azienda', 'module', 90, 0),
('a95a6994-6b8c-1be7-6fcb-538fa1f921b0', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'access', 'prov1_C_Dipendenti', 'module', 89, 0),
('abcb683f-68ad-42ee-c203-538fa17ae1c5', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'view', 'prov1_C_Dipendenti', 'module', 90, 0),
('adbf7f60-2a01-8a9d-269f-538fa1f8cd84', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'list', 'prov1_C_Dipendenti', 'module', 90, 0),
('afb37ccc-7af5-8bc0-593b-538fa12bde54', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'edit', 'prov1_C_Dipendenti', 'module', 90, 0),
('b1a78041-86b5-39c2-7ba3-538fa1b4d26c', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'delete', 'prov1_C_Dipendenti', 'module', 90, 0),
('b3da0c5f-d9fc-bc5d-4be5-538fa11f5a89', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'import', 'prov1_C_Dipendenti', 'module', 90, 0),
('b5ce1280-cc3f-b7cd-c989-538fa11aa18e', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'export', 'prov1_C_Dipendenti', 'module', 90, 0),
('b7c215e4-ea6f-2dac-9f13-538fa1ff0d83', '2014-06-04 22:46:32', '2014-06-04 22:46:32', '1', '1', 'massupdate', 'prov1_C_Dipendenti', 'module', 90, 0),
('b4d926e8-405b-c2b0-8b97-539013c1458e', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'access', 'prov2_F_Cliente', 'module', 89, 0),
('b7c733a6-fef1-a409-4c9d-539013f7c6f3', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'view', 'prov2_F_Cliente', 'module', 90, 0),
('b9f9b6f2-1576-44c9-6993-5390139be7db', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'list', 'prov2_F_Cliente', 'module', 90, 0),
('bbedc624-8e46-a074-a889-5390135bef4f', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'edit', 'prov2_F_Cliente', 'module', 90, 0),
('bde1cd58-816a-6bc4-93c5-5390133dd6a8', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'delete', 'prov2_F_Cliente', 'module', 90, 0),
('bfd5dd8a-7143-7f11-b089-539013e7b21c', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'import', 'prov2_F_Cliente', 'module', 90, 0),
('c2085fb4-347b-06b7-8bdc-53901311156c', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'export', 'prov2_F_Cliente', 'module', 90, 0),
('c3fc69f7-6f06-b869-03e4-5390130c7801', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'massupdate', 'prov2_F_Cliente', 'module', 90, 0),
('dab13877-5fa0-c519-e7a0-53901390097d', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'access', 'prov2_F_Contratti', 'module', 89, 0),
('dce3c76f-effa-47f6-2bde-539013217aa5', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'view', 'prov2_F_Contratti', 'module', 90, 0),
('ded7c456-f5ba-cde7-97c4-5390133b4d79', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'list', 'prov2_F_Contratti', 'module', 90, 0),
('e10a5cd0-94a5-698e-19f4-539013d235b9', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'edit', 'prov2_F_Contratti', 'module', 90, 0),
('e2fe5e52-5554-9a70-4941-53901330a7f6', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'delete', 'prov2_F_Contratti', 'module', 90, 0),
('e4f2532b-2165-acb5-bc19-539013ce9e4c', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'import', 'prov2_F_Contratti', 'module', 90, 0),
('e6e669b6-4adc-3f9e-1fe0-539013241054', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'export', 'prov2_F_Contratti', 'module', 90, 0),
('e918ef76-6003-7f3f-47a2-53901360162b', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'massupdate', 'prov2_F_Contratti', 'module', 90, 0),
('9f4309b7-a8bb-1446-4a84-539013e35c72', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'access', 'prov2_F_Potenziale_Cliente', 'module', 89, 0),
('c26c06e6-5495-ed79-79b2-539013f66a25', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'view', 'prov2_F_Potenziale_Cliente', 'module', 90, 0),
('e1ac06f5-f22f-a331-405f-5390139ce945', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'list', 'prov2_F_Potenziale_Cliente', 'module', 90, 0),
('104d54c0-e7a9-0b5b-bcbe-539013d8ef60', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'edit', 'prov2_F_Potenziale_Cliente', 'module', 90, 0),
('127fd9fd-2c8f-cb94-ddc6-539013c47fca', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'delete', 'prov2_F_Potenziale_Cliente', 'module', 90, 0),
('1473e8de-fe61-c09f-4ebd-5390135d941d', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'import', 'prov2_F_Potenziale_Cliente', 'module', 90, 0),
('16a66962-de61-a20b-e541-5390139ac277', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'export', 'prov2_F_Potenziale_Cliente', 'module', 90, 0),
('189a7d25-5ec5-e068-733b-5390137760bb', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'massupdate', 'prov2_F_Potenziale_Cliente', 'module', 90, 0),
('32ba55f0-1b22-f8b5-6350-53901389f9bd', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'access', 'prov2_F_Upload', 'module', 89, 0),
('34ecd017-ad12-ec07-3d91-539013b216b9', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'view', 'prov2_F_Upload', 'module', 90, 0),
('36e0efd8-f09a-f538-0cb5-5390133267e8', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'list', 'prov2_F_Upload', 'module', 90, 0),
('38d4ee6e-26b5-2667-a72f-53901306355c', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'edit', 'prov2_F_Upload', 'module', 90, 0),
('3ac8f7e2-7e6a-16d7-9e8b-539013f32c6e', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'delete', 'prov2_F_Upload', 'module', 90, 0),
('3cfb721c-e9ea-6219-c30e-539013d1d50b', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'import', 'prov2_F_Upload', 'module', 90, 0),
('3eef8647-173d-3bc8-4231-539013f19dbe', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'export', 'prov2_F_Upload', 'module', 90, 0),
('40e38630-27ab-5567-512d-539013a12515', '2014-06-05 06:51:26', '2014-06-05 06:51:26', '1', '1', 'massupdate', 'prov2_F_Upload', 'module', 90, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `acl_roles`
--

CREATE TABLE IF NOT EXISTS `acl_roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id_del` (`id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `acl_roles_actions`
--

CREATE TABLE IF NOT EXISTS `acl_roles_actions` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `action_id` varchar(36) DEFAULT NULL,
  `access_override` int(3) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acl_role_id` (`role_id`),
  KEY `idx_acl_action_id` (`action_id`),
  KEY `idx_aclrole_action` (`role_id`,`action_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `acl_roles_users`
--

CREATE TABLE IF NOT EXISTS `acl_roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id` (`role_id`),
  KEY `idx_acluser_id` (`user_id`),
  KEY `idx_aclrole_user` (`role_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `address_book`
--

CREATE TABLE IF NOT EXISTS `address_book` (
  `assigned_user_id` char(36) NOT NULL,
  `bean` varchar(50) DEFAULT NULL,
  `bean_id` char(36) NOT NULL,
  KEY `ab_user_bean_idx` (`assigned_user_id`,`bean`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aop_case_events`
--

CREATE TABLE IF NOT EXISTS `aop_case_events` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aop_case_events_audit`
--

CREATE TABLE IF NOT EXISTS `aop_case_events_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aop_case_events_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aop_case_updates`
--

CREATE TABLE IF NOT EXISTS `aop_case_updates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aop_case_updates_audit`
--

CREATE TABLE IF NOT EXISTS `aop_case_updates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aop_case_updates_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aor_charts`
--

CREATE TABLE IF NOT EXISTS `aor_charts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aor_conditions`
--

CREATE TABLE IF NOT EXISTS `aor_conditions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `condition_order` int(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `value_type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aor_conditions_index_report_id` (`aor_report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aor_fields`
--

CREATE TABLE IF NOT EXISTS `aor_fields` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `field_order` int(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `display` tinyint(1) DEFAULT NULL,
  `link` tinyint(1) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `field_function` varchar(100) DEFAULT NULL,
  `sort_by` varchar(100) DEFAULT NULL,
  `sort_order` varchar(100) DEFAULT NULL,
  `group_by` varchar(100) DEFAULT NULL,
  `group_order` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aor_fields_index_report_id` (`aor_report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aor_reports`
--

CREATE TABLE IF NOT EXISTS `aor_reports` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `report_module` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aor_reports_audit`
--

CREATE TABLE IF NOT EXISTS `aor_reports_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aor_reports_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_contracts`
--

CREATE TABLE IF NOT EXISTS `aos_contracts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reference_code` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `total_contract_value` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `customer_signed_date` date DEFAULT NULL,
  `company_signed_date` date DEFAULT NULL,
  `renewal_reminder_date` datetime DEFAULT NULL,
  `contract_type` varchar(100) DEFAULT 'Type',
  `contract_account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `call_id` char(36) DEFAULT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_contracts_audit`
--

CREATE TABLE IF NOT EXISTS `aos_contracts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_contracts_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_contracts_documents`
--

CREATE TABLE IF NOT EXISTS `aos_contracts_documents` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_contracts_id` varchar(36) DEFAULT NULL,
  `documents_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_contracts_documents_alt` (`aos_contracts_id`,`documents_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_invoices`
--

CREATE TABLE IF NOT EXISTS `aos_invoices` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `billing_account_id` char(36) DEFAULT NULL,
  `billing_contact_id` char(36) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `quote_number` int(11) DEFAULT NULL,
  `quote_date` date DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `template_ddown_c` text,
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_invoices_audit`
--

CREATE TABLE IF NOT EXISTS `aos_invoices_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_invoices_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_line_item_groups`
--

CREATE TABLE IF NOT EXISTS `aos_line_item_groups` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_line_item_groups_audit`
--

CREATE TABLE IF NOT EXISTS `aos_line_item_groups_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_line_item_groups_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_pdf_templates`
--

CREATE TABLE IF NOT EXISTS `aos_pdf_templates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `type` varchar(100) DEFAULT NULL,
  `pdfheader` text,
  `pdffooter` text,
  `margin_left` int(255) DEFAULT '15',
  `margin_right` int(255) DEFAULT '15',
  `margin_top` int(255) DEFAULT '16',
  `margin_bottom` int(255) DEFAULT '16',
  `margin_header` int(255) DEFAULT '9',
  `margin_footer` int(255) DEFAULT '9',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_pdf_templates_audit`
--

CREATE TABLE IF NOT EXISTS `aos_pdf_templates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_pdf_templates_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_products`
--

CREATE TABLE IF NOT EXISTS `aos_products` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `maincode` varchar(100) DEFAULT 'XXXX',
  `part_number` varchar(25) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT 'Good',
  `cost` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `price` decimal(26,6) DEFAULT NULL,
  `url` varchar(25) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `aos_product_category_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_products_audit`
--

CREATE TABLE IF NOT EXISTS `aos_products_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_products_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_products_quotes`
--

CREATE TABLE IF NOT EXISTS `aos_products_quotes` (
  `id` char(36) NOT NULL,
  `name` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `item_description` text,
  `number` int(11) DEFAULT NULL,
  `product_qty` decimal(18,4) DEFAULT NULL,
  `product_cost_price` decimal(26,6) DEFAULT NULL,
  `product_list_price` decimal(26,6) DEFAULT NULL,
  `product_discount` decimal(26,6) DEFAULT NULL,
  `product_discount_amount` decimal(26,6) DEFAULT NULL,
  `discount` varchar(255) DEFAULT 'Percentage',
  `product_unit_price` decimal(26,6) DEFAULT NULL,
  `vat_amt` decimal(26,6) DEFAULT NULL,
  `product_total_price` decimal(26,6) DEFAULT NULL,
  `vat` varchar(100) DEFAULT '5.0',
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `product_id` char(36) DEFAULT NULL,
  `group_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aospq_par_del` (`parent_id`,`parent_type`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_products_quotes_audit`
--

CREATE TABLE IF NOT EXISTS `aos_products_quotes_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_products_quotes_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_product_categories`
--

CREATE TABLE IF NOT EXISTS `aos_product_categories` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_parent` tinyint(1) DEFAULT '0',
  `parent_category_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_product_categories_audit`
--

CREATE TABLE IF NOT EXISTS `aos_product_categories_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_product_categories_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_quotes`
--

CREATE TABLE IF NOT EXISTS `aos_quotes` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `approval_issue` text,
  `billing_account_id` char(36) DEFAULT NULL,
  `billing_contact_id` char(36) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `number` int(11) NOT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `template_ddown_c` text,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `stage` varchar(100) DEFAULT 'Draft',
  `term` varchar(100) DEFAULT NULL,
  `terms_c` text,
  `approval_status` varchar(100) DEFAULT NULL,
  `invoice_status` varchar(100) DEFAULT 'Not Invoiced',
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_quotes_aos_invoices_c`
--

CREATE TABLE IF NOT EXISTS `aos_quotes_aos_invoices_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotes77d9_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes6b83nvoices_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_aos_invoices_alt` (`aos_quotes77d9_quotes_ida`,`aos_quotes6b83nvoices_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_quotes_audit`
--

CREATE TABLE IF NOT EXISTS `aos_quotes_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_quotes_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_quotes_os_contracts_c`
--

CREATE TABLE IF NOT EXISTS `aos_quotes_os_contracts_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotese81e_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes4dc0ntracts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_aos_contracts_alt` (`aos_quotese81e_quotes_ida`,`aos_quotes4dc0ntracts_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aos_quotes_project_c`
--

CREATE TABLE IF NOT EXISTS `aos_quotes_project_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotes1112_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes7207project_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_project_alt` (`aos_quotes1112_quotes_ida`,`aos_quotes7207project_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aow_actions`
--

CREATE TABLE IF NOT EXISTS `aow_actions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `action_order` int(255) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `parameters` longtext,
  PRIMARY KEY (`id`),
  KEY `aow_action_index_workflow_id` (`aow_workflow_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aow_conditions`
--

CREATE TABLE IF NOT EXISTS `aow_conditions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `condition_order` int(255) DEFAULT NULL,
  `field` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `value_type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aow_conditions_index_workflow_id` (`aow_workflow_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aow_processed`
--

CREATE TABLE IF NOT EXISTS `aow_processed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Pending',
  PRIMARY KEY (`id`),
  KEY `aow_processed_index_status` (`status`),
  KEY `aow_processed_index_workflow_id` (`aow_workflow_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aow_processed_aow_actions`
--

CREATE TABLE IF NOT EXISTS `aow_processed_aow_actions` (
  `id` varchar(36) NOT NULL,
  `aow_processed_id` varchar(36) DEFAULT NULL,
  `aow_action_id` varchar(36) DEFAULT NULL,
  `status` varchar(36) DEFAULT 'Pending',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aow_processed_aow_actions` (`aow_processed_id`,`aow_action_id`),
  KEY `idx_actid_del_freid` (`aow_action_id`,`deleted`,`aow_processed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aow_workflow`
--

CREATE TABLE IF NOT EXISTS `aow_workflow` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `flow_module` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Active',
  `run_when` varchar(100) DEFAULT 'Create',
  `multiple_runs` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `aow_workflow_index_status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `aow_workflow_audit`
--

CREATE TABLE IF NOT EXISTS `aow_workflow_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aow_workflow_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `bugs`
--

CREATE TABLE IF NOT EXISTS `bugs` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `bug_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` varchar(255) DEFAULT NULL,
  `work_log` text,
  `found_in_release` varchar(255) DEFAULT NULL,
  `fixed_in_release` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `product_category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bugsnumk` (`bug_number`),
  KEY `bug_number` (`bug_number`),
  KEY `idx_bug_name` (`name`),
  KEY `idx_bugs_assigned_user` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `bugs_audit`
--

CREATE TABLE IF NOT EXISTS `bugs_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_bugs_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `calls`
--

CREATE TABLE IF NOT EXISTS `calls` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(2) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `direction` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_call_name` (`name`),
  KEY `idx_status` (`status`),
  KEY `idx_calls_date_start` (`date_start`),
  KEY `idx_calls_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_calls_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `calls_contacts`
--

CREATE TABLE IF NOT EXISTS `calls_contacts` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_call_call` (`call_id`),
  KEY `idx_con_call_con` (`contact_id`),
  KEY `idx_call_contact` (`call_id`,`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `calls_leads`
--

CREATE TABLE IF NOT EXISTS `calls_leads` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_call_call` (`call_id`),
  KEY `idx_lead_call_lead` (`lead_id`),
  KEY `idx_call_lead` (`call_id`,`lead_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `calls_reschedule`
--

CREATE TABLE IF NOT EXISTS `calls_reschedule` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `call_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `calls_reschedule_audit`
--

CREATE TABLE IF NOT EXISTS `calls_reschedule_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_calls_reschedule_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `calls_users`
--

CREATE TABLE IF NOT EXISTS `calls_users` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_call_call` (`call_id`),
  KEY `idx_usr_call_usr` (`user_id`),
  KEY `idx_call_users` (`call_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `campaigns`
--

CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `tracker_count` int(11) DEFAULT '0',
  `refer_url` varchar(255) DEFAULT 'http://',
  `tracker_text` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `impressions` int(11) DEFAULT '0',
  `currency_id` char(36) DEFAULT NULL,
  `budget` double DEFAULT NULL,
  `expected_cost` double DEFAULT NULL,
  `actual_cost` double DEFAULT NULL,
  `expected_revenue` double DEFAULT NULL,
  `campaign_type` varchar(100) DEFAULT NULL,
  `objective` text,
  `content` text,
  `frequency` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `camp_auto_tracker_key` (`tracker_key`),
  KEY `idx_campaign_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `campaigns_audit`
--

CREATE TABLE IF NOT EXISTS `campaigns_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_campaigns_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `campaign_log`
--

CREATE TABLE IF NOT EXISTS `campaign_log` (
  `id` char(36) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `target_tracker_key` varchar(36) DEFAULT NULL,
  `target_id` varchar(36) DEFAULT NULL,
  `target_type` varchar(100) DEFAULT NULL,
  `activity_type` varchar(100) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `list_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `more_information` varchar(100) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_camp_tracker` (`target_tracker_key`),
  KEY `idx_camp_campaign_id` (`campaign_id`),
  KEY `idx_camp_more_info` (`more_information`),
  KEY `idx_target_id` (`target_id`),
  KEY `idx_target_id_deleted` (`target_id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `campaign_trkrs`
--

CREATE TABLE IF NOT EXISTS `campaign_trkrs` (
  `id` char(36) NOT NULL,
  `tracker_name` varchar(30) DEFAULT NULL,
  `tracker_url` varchar(255) DEFAULT 'http://',
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `is_optout` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `campaign_tracker_key_idx` (`tracker_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `cases`
--

CREATE TABLE IF NOT EXISTS `cases` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` text,
  `work_log` text,
  `account_id` char(36) DEFAULT NULL,
  `state` varchar(100) DEFAULT 'Open',
  PRIMARY KEY (`id`),
  UNIQUE KEY `casesnumk` (`case_number`),
  KEY `case_number` (`case_number`),
  KEY `idx_case_name` (`name`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_cases_stat_del` (`assigned_user_id`,`status`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `cases_audit`
--

CREATE TABLE IF NOT EXISTS `cases_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_cases_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `cases_bugs`
--

CREATE TABLE IF NOT EXISTS `cases_bugs` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_cas_bug_cas` (`case_id`),
  KEY `idx_cas_bug_bug` (`bug_id`),
  KEY `idx_case_bug` (`case_id`,`bug_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `cases_cstm`
--

CREATE TABLE IF NOT EXISTS `cases_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `category` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `value` text,
  KEY `idx_config_cat` (`category`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `config`
--

INSERT INTO `config` (`category`, `name`, `value`) VALUES
('notify', 'fromaddress', 'do_not_reply@example.com'),
('notify', 'fromname', 'SugarCRM'),
('notify', 'send_by_default', '1'),
('notify', 'on', '1'),
('notify', 'send_from_assigning_user', '0'),
('info', 'sugar_version', '6.5.16'),
('MySettings', 'tab', 'YTozNjp7czo0OiJIb21lIjtzOjQ6IkhvbWUiO3M6ODoiQWNjb3VudHMiO3M6ODoiQWNjb3VudHMiO3M6ODoiQ29udGFjdHMiO3M6ODoiQ29udGFjdHMiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6NToiTGVhZHMiO3M6NToiTGVhZHMiO3M6MTA6IkFPU19RdW90ZXMiO3M6MTA6IkFPU19RdW90ZXMiO3M6ODoiQ2FsZW5kYXIiO3M6ODoiQ2FsZW5kYXIiO3M6OToiRG9jdW1lbnRzIjtzOjk6IkRvY3VtZW50cyI7czo2OiJFbWFpbHMiO3M6NjoiRW1haWxzIjtzOjk6IkNhbXBhaWducyI7czo5OiJDYW1wYWlnbnMiO3M6NToiQ2FsbHMiO3M6NToiQ2FsbHMiO3M6ODoiTWVldGluZ3MiO3M6ODoiTWVldGluZ3MiO3M6NToiVGFza3MiO3M6NToiVGFza3MiO3M6NToiTm90ZXMiO3M6NToiTm90ZXMiO3M6MTI6IkFPU19JbnZvaWNlcyI7czoxMjoiQU9TX0ludm9pY2VzIjtzOjEzOiJBT1NfQ29udHJhY3RzIjtzOjEzOiJBT1NfQ29udHJhY3RzIjtzOjU6IkNhc2VzIjtzOjU6IkNhc2VzIjtzOjk6IlByb3NwZWN0cyI7czo5OiJQcm9zcGVjdHMiO3M6MTM6IlByb3NwZWN0TGlzdHMiO3M6MTM6IlByb3NwZWN0TGlzdHMiO3M6OToiRlBfZXZlbnRzIjtzOjk6IkZQX2V2ZW50cyI7czoxODoiRlBfRXZlbnRfTG9jYXRpb25zIjtzOjE4OiJGUF9FdmVudF9Mb2NhdGlvbnMiO3M6MTI6IkFPU19Qcm9kdWN0cyI7czoxMjoiQU9TX1Byb2R1Y3RzIjtzOjIyOiJBT1NfUHJvZHVjdF9DYXRlZ29yaWVzIjtzOjIyOiJBT1NfUHJvZHVjdF9DYXRlZ29yaWVzIjtzOjE3OiJBT1NfUERGX1RlbXBsYXRlcyI7czoxNzoiQU9TX1BERl9UZW1wbGF0ZXMiO3M6OToiamp3Z19NYXBzIjtzOjk6Impqd2dfTWFwcyI7czoxMjoiamp3Z19NYXJrZXJzIjtzOjEyOiJqandnX01hcmtlcnMiO3M6MTA6Impqd2dfQXJlYXMiO3M6MTA6Impqd2dfQXJlYXMiO3M6MTg6Impqd2dfQWRkcmVzc19DYWNoZSI7czoxODoiamp3Z19BZGRyZXNzX0NhY2hlIjtzOjExOiJBT1JfUmVwb3J0cyI7czoxMToiQU9SX1JlcG9ydHMiO3M6MTI6IkFPV19Xb3JrRmxvdyI7czoxMjoiQU9XX1dvcmtGbG93IjtzOjE1OiJwcm92MV9DX0F6aWVuZGEiO3M6MTU6InByb3YxX0NfQXppZW5kYSI7czoxODoicHJvdjFfQ19EaXBlbmRlbnRpIjtzOjE4OiJwcm92MV9DX0RpcGVuZGVudGkiO3M6MTU6InByb3YyX0ZfQ2xpZW50ZSI7czoxNToicHJvdjJfRl9DbGllbnRlIjtzOjE3OiJwcm92Ml9GX0NvbnRyYXR0aSI7czoxNzoicHJvdjJfRl9Db250cmF0dGkiO3M6MjY6InByb3YyX0ZfUG90ZW56aWFsZV9DbGllbnRlIjtzOjI2OiJwcm92Ml9GX1BvdGVuemlhbGVfQ2xpZW50ZSI7czoxNDoicHJvdjJfRl9VcGxvYWQiO3M6MTQ6InByb3YyX0ZfVXBsb2FkIjt9'),
('portal', 'on', '0'),
('tracker', 'Tracker', '1'),
('system', 'skypeout_on', '1'),
('sugarfeed', 'enabled', '1'),
('sugarfeed', 'module_UserFeed', '1'),
('sugarfeed', 'module_Cases', '1'),
('sugarfeed', 'module_Contacts', '1'),
('sugarfeed', 'module_Leads', '1'),
('sugarfeed', 'module_Opportunities', '1'),
('Update', 'CheckUpdates', 'manual'),
('system', 'name', 'Solar SRL'),
('system', 'adminwizard', '1'),
('notify', 'allow_default_outbound', '0'),
('proxy', 'on', '0'),
('proxy', 'host', ''),
('proxy', 'port', ''),
('proxy', 'auth', '0'),
('proxy', 'username', 'admin'),
('proxy', 'password', 'MZ/V8OGrgIM=');

-- --------------------------------------------------------

--
-- Struttura della tabella `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `lead_source` varchar(255) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `joomla_account_id` varchar(255) DEFAULT NULL,
  `portal_account_disabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cont_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_contacts_del_last` (`deleted`,`last_name`),
  KEY `idx_cont_del_reports` (`deleted`,`reports_to_id`,`last_name`),
  KEY `idx_reports_to_id` (`reports_to_id`),
  KEY `idx_del_id_user` (`deleted`,`id`,`assigned_user_id`),
  KEY `idx_cont_assigned` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `contacts_audit`
--

CREATE TABLE IF NOT EXISTS `contacts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_contacts_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `contacts_bugs`
--

CREATE TABLE IF NOT EXISTS `contacts_bugs` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_bug_con` (`contact_id`),
  KEY `idx_con_bug_bug` (`bug_id`),
  KEY `idx_contact_bug` (`contact_id`,`bug_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `contacts_cases`
--

CREATE TABLE IF NOT EXISTS `contacts_cases` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_case_con` (`contact_id`),
  KEY `idx_con_case_case` (`case_id`),
  KEY `idx_contacts_cases` (`contact_id`,`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `contacts_cstm`
--

CREATE TABLE IF NOT EXISTS `contacts_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `contacts_users`
--

CREATE TABLE IF NOT EXISTS `contacts_users` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_users_con` (`contact_id`),
  KEY `idx_con_users_user` (`user_id`),
  KEY `idx_contacts_users` (`contact_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `cron_remove_documents`
--

CREATE TABLE IF NOT EXISTS `cron_remove_documents` (
  `id` varchar(36) NOT NULL,
  `bean_id` varchar(36) DEFAULT NULL,
  `module` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cron_remove_document_bean_id` (`bean_id`),
  KEY `idx_cron_remove_document_stamp` (`date_modified`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` char(36) NOT NULL,
  `name` varchar(36) DEFAULT NULL,
  `symbol` varchar(36) DEFAULT NULL,
  `iso4217` varchar(3) DEFAULT NULL,
  `conversion_rate` double DEFAULT '0',
  `status` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_currency_name` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `custom_fields`
--

CREATE TABLE IF NOT EXISTS `custom_fields` (
  `bean_id` varchar(36) DEFAULT NULL,
  `set_num` int(11) DEFAULT '0',
  `field0` varchar(255) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `field9` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_beanid_set_num` (`bean_id`,`set_num`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT 'Sugar',
  `doc_url` varchar(255) DEFAULT NULL,
  `active_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `subcategory_id` varchar(100) DEFAULT NULL,
  `status_id` varchar(100) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `related_doc_id` char(36) DEFAULT NULL,
  `related_doc_rev_id` char(36) DEFAULT NULL,
  `is_template` tinyint(1) DEFAULT '0',
  `template_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_doc_cat` (`category_id`,`subcategory_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `documents_accounts`
--

CREATE TABLE IF NOT EXISTS `documents_accounts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_accounts_account_id` (`account_id`,`document_id`),
  KEY `documents_accounts_document_id` (`document_id`,`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `documents_bugs`
--

CREATE TABLE IF NOT EXISTS `documents_bugs` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_bugs_bug_id` (`bug_id`,`document_id`),
  KEY `documents_bugs_document_id` (`document_id`,`bug_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `documents_cases`
--

CREATE TABLE IF NOT EXISTS `documents_cases` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_cases_case_id` (`case_id`,`document_id`),
  KEY `documents_cases_document_id` (`document_id`,`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `documents_contacts`
--

CREATE TABLE IF NOT EXISTS `documents_contacts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_contacts_contact_id` (`contact_id`,`document_id`),
  KEY `documents_contacts_document_id` (`document_id`,`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `documents_opportunities`
--

CREATE TABLE IF NOT EXISTS `documents_opportunities` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_docu_opps_oppo_id` (`opportunity_id`,`document_id`),
  KEY `idx_docu_oppo_docu_id` (`document_id`,`opportunity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `document_revisions`
--

CREATE TABLE IF NOT EXISTS `document_revisions` (
  `id` varchar(36) NOT NULL,
  `change_log` varchar(255) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT NULL,
  `doc_url` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `revision` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documentrevision_mimetype` (`file_mime_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `eapm`
--

CREATE TABLE IF NOT EXISTS `eapm` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `application` varchar(100) DEFAULT 'webex',
  `api_data` text,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `oauth_token` varchar(255) DEFAULT NULL,
  `oauth_secret` varchar(255) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_app_active` (`assigned_user_id`,`application`,`validated`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `emailman`
--

CREATE TABLE IF NOT EXISTS `emailman` (
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  `list_id` char(36) DEFAULT NULL,
  `send_date_time` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `in_queue` tinyint(1) DEFAULT '0',
  `in_queue_date` datetime DEFAULT NULL,
  `send_attempts` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `related_id` char(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_eman_list` (`list_id`,`user_id`,`deleted`),
  KEY `idx_eman_campaign_id` (`campaign_id`),
  KEY `idx_eman_relid_reltype_id` (`related_id`,`related_type`,`campaign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `emails`
--

CREATE TABLE IF NOT EXISTS `emails` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `flagged` tinyint(1) DEFAULT NULL,
  `reply_to_status` tinyint(1) DEFAULT NULL,
  `intent` varchar(100) DEFAULT 'pick',
  `mailbox_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_name` (`name`),
  KEY `idx_message_id` (`message_id`),
  KEY `idx_email_parent_id` (`parent_id`),
  KEY `idx_email_assigned` (`assigned_user_id`,`type`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `emails_beans`
--

CREATE TABLE IF NOT EXISTS `emails_beans` (
  `id` char(36) NOT NULL,
  `email_id` char(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `campaign_data` text,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emails_beans_bean_id` (`bean_id`),
  KEY `idx_emails_beans_email_bean` (`email_id`,`bean_id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `emails_email_addr_rel`
--

CREATE TABLE IF NOT EXISTS `emails_email_addr_rel` (
  `id` char(36) NOT NULL,
  `email_id` char(36) NOT NULL,
  `address_type` varchar(4) DEFAULT NULL,
  `email_address_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_eearl_email_id` (`email_id`,`address_type`),
  KEY `idx_eearl_address_id` (`email_address_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `emails_text`
--

CREATE TABLE IF NOT EXISTS `emails_text` (
  `email_id` char(36) NOT NULL,
  `from_addr` varchar(255) DEFAULT NULL,
  `reply_to_addr` varchar(255) DEFAULT NULL,
  `to_addrs` text,
  `cc_addrs` text,
  `bcc_addrs` text,
  `description` longtext,
  `description_html` longtext,
  `raw_source` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`email_id`),
  KEY `emails_textfromaddr` (`from_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `email_addresses`
--

CREATE TABLE IF NOT EXISTS `email_addresses` (
  `id` char(36) NOT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `email_address_caps` varchar(255) DEFAULT NULL,
  `invalid_email` tinyint(1) DEFAULT '0',
  `opt_out` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ea_caps_opt_out_invalid` (`email_address_caps`,`opt_out`,`invalid_email`),
  KEY `idx_ea_opt_out_invalid` (`email_address`,`opt_out`,`invalid_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_addresses`
--

INSERT INTO `email_addresses` (`id`, `email_address`, `email_address_caps`, `invalid_email`, `opt_out`, `date_created`, `date_modified`, `deleted`) VALUES
('82a0b52c-4f05-8a31-a999-5388e33422d6', 'jp90jp@hotmail.it', 'JP90JP@HOTMAIL.IT', 0, 0, '2014-05-30 20:02:44', '2014-05-30 20:02:44', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `email_addr_bean_rel`
--

CREATE TABLE IF NOT EXISTS `email_addr_bean_rel` (
  `id` char(36) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `bean_id` char(36) NOT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `primary_address` tinyint(1) DEFAULT '0',
  `reply_to_address` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_email_address_id` (`email_address_id`),
  KEY `idx_bean_id` (`bean_id`,`bean_module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_addr_bean_rel`
--

INSERT INTO `email_addr_bean_rel` (`id`, `email_address_id`, `bean_id`, `bean_module`, `primary_address`, `reply_to_address`, `date_created`, `date_modified`, `deleted`) VALUES
('82623331-e1c6-947b-48ab-5388e3302be8', '82a0b52c-4f05-8a31-a999-5388e33422d6', '1', 'Users', 1, 0, '2014-05-30 20:02:44', '2014-05-30 20:02:44', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `email_cache`
--

CREATE TABLE IF NOT EXISTS `email_cache` (
  `ie_id` char(36) DEFAULT NULL,
  `mbox` varchar(60) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `fromaddr` varchar(100) DEFAULT NULL,
  `toaddr` varchar(255) DEFAULT NULL,
  `senddate` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `mailsize` int(10) unsigned DEFAULT NULL,
  `imap_uid` int(10) unsigned DEFAULT NULL,
  `msgno` int(10) unsigned DEFAULT NULL,
  `recent` tinyint(4) DEFAULT NULL,
  `flagged` tinyint(4) DEFAULT NULL,
  `answered` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `seen` tinyint(4) DEFAULT NULL,
  `draft` tinyint(4) DEFAULT NULL,
  KEY `idx_ie_id` (`ie_id`),
  KEY `idx_mail_date` (`ie_id`,`mbox`,`senddate`),
  KEY `idx_mail_from` (`ie_id`,`mbox`,`fromaddr`),
  KEY `idx_mail_subj` (`subject`),
  KEY `idx_mail_to` (`toaddr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `email_marketing`
--

CREATE TABLE IF NOT EXISTS `email_marketing` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `from_addr` varchar(100) DEFAULT NULL,
  `reply_to_name` varchar(100) DEFAULT NULL,
  `reply_to_addr` varchar(100) DEFAULT NULL,
  `inbound_email_id` varchar(36) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `template_id` char(36) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `all_prospect_lists` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emmkt_name` (`name`),
  KEY `idx_emmkit_del` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `email_marketing_prospect_lists`
--

CREATE TABLE IF NOT EXISTS `email_marketing_prospect_lists` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `email_marketing_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email_mp_prospects` (`email_marketing_id`,`prospect_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `published` varchar(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `body_html` text,
  `deleted` tinyint(1) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `text_only` tinyint(1) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_template_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_templates`
--

INSERT INTO `email_templates` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `published`, `name`, `description`, `subject`, `body`, `body_html`, `deleted`, `assigned_user_id`, `text_only`, `type`) VALUES
('3faa484e-6f29-a1ab-265b-5388e3ad56c1', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'off', 'System-generated password email', 'This template is used when the System Administrator sends a new password to a user.', 'New account information', '\nHere is your account username and temporary password:\nUsername : $contact_user_user_name\nPassword : $contact_user_user_hash\n\n$config_site_url\n\nAfter you log in using the above password, you may be required to reset the password to one of your own choice.', '<div><table width="550"><tbody><tr><td><p>Here is your account username and temporary password:</p><p>Username : $contact_user_user_name </p><p>Password : $contact_user_user_hash </p><br /><p>$config_site_url</p><br /><p>After you log in using the above password, you may be required to reset the password to one of your own choice.</p>   </td>         </tr><tr><td></td>         </tr></tbody></table></div>', 0, NULL, 0, NULL),
('4353e4dd-7529-93c9-14cf-5388e32a201a', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'off', 'Forgot Password email', 'This template is used to send a user a link to click to reset the user''s account password.', 'Reset your account password', '\nYou recently requested on $contact_user_pwd_last_changed to be able to reset your account password.\n\nClick on the link below to reset your password:\n\n$contact_user_link_guid', '<div><table width="550"><tbody><tr><td><p>You recently requested on $contact_user_pwd_last_changed to be able to reset your account password. </p><p>Click on the link below to reset your password:</p><p> $contact_user_link_guid </p>  </td>         </tr><tr><td></td>         </tr></tbody></table></div>', 0, NULL, 0, NULL),
('58d03c11-aabf-2d14-a99e-5388e3cd7eff', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'off', 'Case Closure', 'Template for informing a contact that their case has been closed.', '$acase_name [CASE:$acase_case_number] closed', 'Hi $contact_first_name $contact_last_name,\r\n\r\n					   Your case $acase_name (# $acase_case_number) has been closed on $acase_date_entered\r\n					   Status:				$acase_status\r\n					   Reference:			$acase_case_number\r\n					   Resolution:			$acase_resolution', '<p> Hi $contact_first_name $contact_last_name,</p>\r\n					    <p>Your case $acase_name (# $acase_case_number) has been closed on $acase_date_entered</p>\r\n					    <table border="0"><tbody><tr><td>Status</td><td>$acase_status</td></tr><tr><td>Reference</td><td>$acase_case_number</td></tr><tr><td>Resolution</td><td>$acase_resolution</td></tr></tbody></table>', 0, NULL, NULL, NULL),
('5cb8474c-9eb5-1a59-6334-5388e37e3724', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'off', 'Joomla Account Creation', 'Template used when informing a contact that they''ve been given an account on the joomla portal.', 'Support Portal Account Created', 'Hi $contact_name,\r\n					   An account has been created for you at $portal_address.\r\n					   You may login using this email address and the password $joomla_pass', '<p>Hi $contact_name,</p>\r\n					    <p>An account has been created for you at <a href="$portal_address">$portal_address</a>.</p>\r\n					    <p>You may login using this email address and the password $joomla_pass</p>', 0, NULL, NULL, NULL),
('61d8d66b-b090-30f3-d25f-5388e38760ab', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'off', 'Case Creation', 'Template to send to a contact when a case is received from them.', '$acase_name [CASE:$acase_case_number]', 'Hi $contact_first_name $contact_last_name,\r\n\r\n					   We''ve received your case $acase_name (# $acase_case_number) on $acase_date_entered\r\n					   Status:		$acase_status\r\n					   Reference:	$acase_case_number\r\n					   Description:	$acase_description', '<p> Hi $contact_first_name $contact_last_name,</p>\r\n					    <p>We''ve received your case $acase_name (# $acase_case_number) on $acase_date_entered</p>\r\n					    <table border="0"><tbody><tr><td>Status</td><td>$acase_status</td></tr><tr><td>Reference</td><td>$acase_case_number</td></tr><tr><td>Description</td><td>$acase_description</td></tr></tbody></table>', 0, NULL, NULL, NULL),
('677675bf-4b84-722d-9741-5388e3ce284d', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'off', 'Contact Case Update', 'Template to send to a contact when their case is updated.', '$acase_name update [CASE:$acase_case_number]', 'Hi $user_first_name $user_last_name,\r\n\r\n					   You''ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:\r\n					       $contact_first_name $contact_last_name, said:\r\n					               $aop_case_updates_description', '<p>Hi $contact_first_name $contact_last_name,</p>\r\n					    <p> </p>\r\n					    <p>You''ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:</p>\r\n					    <p><strong>$user_first_name $user_last_name said:</strong></p>\r\n					    <p style="padding-left:30px;">$aop_case_updates_description</p>', 0, NULL, NULL, NULL),
('6bdb8e76-18a3-4da0-a42d-5388e3c36dbe', '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'off', 'User Case Update', 'Email template to send to a Sugar user when their case is updated.', '$acase_name (# $acase_case_number) update', 'Hi $user_first_name $user_last_name,\r\n\r\n					   You''ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:\r\n					       $contact_first_name $contact_last_name, said:\r\n					               $aop_case_updates_description\r\n                        You may review this Case at:\r\n                            $sugarurl/index.php?module=Cases&action=DetailView&record=$acase_id;', '<p>Hi $user_first_name $user_last_name,</p>\r\n					   <p> </p>\r\n					   <p>You''ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:</p>\r\n					   <p><strong>$contact_first_name $contact_last_name, said:</strong></p>\r\n					   <p style="padding-left:30px;">$aop_case_updates_description</p>\r\n					   <p>You may review this Case at: $sugarurl/index.php?module=Cases&action=DetailView&record=$acase_id;</p>\r\n					   ', 0, NULL, NULL, NULL),
('830d54b9-af30-5be8-fcc6-5388e3f2f009', '2013-05-24 14:31:45', '2014-05-30 20:01:02', '1', '1', 'off', 'Event Invite Template', 'Default event invite template.', 'You have been invited to $fp_events_name', 'Dear $contact_name,\r\nYou have been invited to $fp_events_name on $fp_events_date_start to $fp_events_date_end\r\n$fp_events_description\r\nYours Sincerely,\r\n', '\r\n<p>Dear $contact_name,</p>\r\n<p>You have been invited to $fp_events_name on $fp_events_date_start to $fp_events_date_end</p>\r\n<p>$fp_events_description</p>\r\n<p>If you would like to accept this invititation please click accept.</p>\r\n<p> $fp_events_link or $fp_events_link_declined</p>\r\n<p>Yours Sincerely,</p>\r\n', 0, NULL, NULL, 'email');

-- --------------------------------------------------------

--
-- Struttura della tabella `fields_meta_data`
--

CREATE TABLE IF NOT EXISTS `fields_meta_data` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vname` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `help` varchar(255) DEFAULT NULL,
  `custom_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `len` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `default_value` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `audited` tinyint(1) DEFAULT '0',
  `massupdate` tinyint(1) DEFAULT '0',
  `duplicate_merge` smallint(6) DEFAULT '0',
  `reportable` tinyint(1) DEFAULT '1',
  `importable` varchar(255) DEFAULT NULL,
  `ext1` varchar(255) DEFAULT NULL,
  `ext2` varchar(255) DEFAULT NULL,
  `ext3` varchar(255) DEFAULT NULL,
  `ext4` text,
  PRIMARY KEY (`id`),
  KEY `idx_meta_id_del` (`id`,`deleted`),
  KEY `idx_meta_cm_del` (`custom_module`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `fields_meta_data`
--

INSERT INTO `fields_meta_data` (`id`, `name`, `vname`, `comments`, `help`, `custom_module`, `type`, `len`, `required`, `default_value`, `date_modified`, `deleted`, `audited`, `massupdate`, `duplicate_merge`, `reportable`, `importable`, `ext1`, `ext2`, `ext3`, `ext4`) VALUES
('Accountsjjwg_maps_lng_c', 'jjwg_maps_lng_c', 'LBL_JJWG_MAPS_LNG', '', 'Longitude', 'Accounts', 'float', 11, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Accountsjjwg_maps_lat_c', 'jjwg_maps_lat_c', 'LBL_JJWG_MAPS_LAT', '', 'Latitude', 'Accounts', 'float', 10, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Accountsjjwg_maps_geocode_status_c', 'jjwg_maps_geocode_status_c', 'LBL_JJWG_MAPS_GEOCODE_STATUS', 'Geocode Status', 'Geocode Status', 'Accounts', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Accountsjjwg_maps_address_c', 'jjwg_maps_address_c', 'LBL_JJWG_MAPS_ADDRESS', 'Address', 'Address', 'Accounts', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Casesjjwg_maps_lng_c', 'jjwg_maps_lng_c', 'LBL_JJWG_MAPS_LNG', '', 'Longitude', 'Cases', 'float', 11, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Casesjjwg_maps_lat_c', 'jjwg_maps_lat_c', 'LBL_JJWG_MAPS_LAT', '', 'Latitude', 'Cases', 'float', 10, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Casesjjwg_maps_geocode_status_c', 'jjwg_maps_geocode_status_c', 'LBL_JJWG_MAPS_GEOCODE_STATUS', 'Geocode Status', 'Geocode Status', 'Cases', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Casesjjwg_maps_address_c', 'jjwg_maps_address_c', 'LBL_JJWG_MAPS_ADDRESS', 'Address', 'Address', 'Cases', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Contactsjjwg_maps_lng_c', 'jjwg_maps_lng_c', 'LBL_JJWG_MAPS_LNG', '', 'Longitude', 'Contacts', 'float', 11, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Contactsjjwg_maps_lat_c', 'jjwg_maps_lat_c', 'LBL_JJWG_MAPS_LAT', '', 'Latitude', 'Contacts', 'float', 10, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Contactsjjwg_maps_geocode_status_c', 'jjwg_maps_geocode_status_c', 'LBL_JJWG_MAPS_GEOCODE_STATUS', 'Geocode Status', 'Geocode Status', 'Contacts', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Contactsjjwg_maps_address_c', 'jjwg_maps_address_c', 'LBL_JJWG_MAPS_ADDRESS', 'Address', 'Address', 'Contacts', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Leadsjjwg_maps_lng_c', 'jjwg_maps_lng_c', 'LBL_JJWG_MAPS_LNG', '', 'Longitude', 'Leads', 'float', 11, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Leadsjjwg_maps_lat_c', 'jjwg_maps_lat_c', 'LBL_JJWG_MAPS_LAT', '', 'Latitude', 'Leads', 'float', 10, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Leadsjjwg_maps_geocode_status_c', 'jjwg_maps_geocode_status_c', 'LBL_JJWG_MAPS_GEOCODE_STATUS', 'Geocode Status', 'Geocode Status', 'Leads', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Leadsjjwg_maps_address_c', 'jjwg_maps_address_c', 'LBL_JJWG_MAPS_ADDRESS', 'Address', 'Address', 'Leads', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Meetingsjjwg_maps_lng_c', 'jjwg_maps_lng_c', 'LBL_JJWG_MAPS_LNG', '', 'Longitude', 'Meetings', 'float', 11, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Meetingsjjwg_maps_lat_c', 'jjwg_maps_lat_c', 'LBL_JJWG_MAPS_LAT', '', 'Latitude', 'Meetings', 'float', 10, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Meetingsjjwg_maps_geocode_status_c', 'jjwg_maps_geocode_status_c', 'LBL_JJWG_MAPS_GEOCODE_STATUS', 'Geocode Status', 'Geocode Status', 'Meetings', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Meetingsjjwg_maps_address_c', 'jjwg_maps_address_c', 'LBL_JJWG_MAPS_ADDRESS', 'Address', 'Address', 'Meetings', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Opportunitiesjjwg_maps_lng_c', 'jjwg_maps_lng_c', 'LBL_JJWG_MAPS_LNG', '', 'Longitude', 'Opportunities', 'float', 11, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Opportunitiesjjwg_maps_lat_c', 'jjwg_maps_lat_c', 'LBL_JJWG_MAPS_LAT', '', 'Latitude', 'Opportunities', 'float', 10, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Opportunitiesjjwg_maps_geocode_status_c', 'jjwg_maps_geocode_status_c', 'LBL_JJWG_MAPS_GEOCODE_STATUS', 'Geocode Status', 'Geocode Status', 'Opportunities', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Opportunitiesjjwg_maps_address_c', 'jjwg_maps_address_c', 'LBL_JJWG_MAPS_ADDRESS', 'Address', 'Address', 'Opportunities', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Projectjjwg_maps_lng_c', 'jjwg_maps_lng_c', 'LBL_JJWG_MAPS_LNG', '', 'Longitude', 'Project', 'float', 11, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Projectjjwg_maps_lat_c', 'jjwg_maps_lat_c', 'LBL_JJWG_MAPS_LAT', '', 'Latitude', 'Project', 'float', 10, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Projectjjwg_maps_geocode_status_c', 'jjwg_maps_geocode_status_c', 'LBL_JJWG_MAPS_GEOCODE_STATUS', 'Geocode Status', 'Geocode Status', 'Project', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Projectjjwg_maps_address_c', 'jjwg_maps_address_c', 'LBL_JJWG_MAPS_ADDRESS', 'Address', 'Address', 'Project', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Prospectsjjwg_maps_lng_c', 'jjwg_maps_lng_c', 'LBL_JJWG_MAPS_LNG', '', 'Longitude', 'Prospects', 'float', 11, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Prospectsjjwg_maps_lat_c', 'jjwg_maps_lat_c', 'LBL_JJWG_MAPS_LAT', '', 'Latitude', 'Prospects', 'float', 10, 0, '0.00000000', '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', '8', '', '', ''),
('Prospectsjjwg_maps_geocode_status_c', 'jjwg_maps_geocode_status_c', 'LBL_JJWG_MAPS_GEOCODE_STATUS', 'Geocode Status', 'Geocode Status', 'Prospects', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', ''),
('Prospectsjjwg_maps_address_c', 'jjwg_maps_address_c', 'LBL_JJWG_MAPS_ADDRESS', 'Address', 'Address', 'Prospects', 'varchar', 255, 0, NULL, '2014-05-30 20:01:02', 0, 0, 0, 0, 1, 'true', NULL, '', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `folders`
--

CREATE TABLE IF NOT EXISTS `folders` (
  `id` char(36) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `folder_type` varchar(25) DEFAULT NULL,
  `parent_folder` char(36) DEFAULT NULL,
  `has_child` tinyint(1) DEFAULT '0',
  `is_group` tinyint(1) DEFAULT '0',
  `is_dynamic` tinyint(1) DEFAULT '0',
  `dynamic_query` text,
  `assign_to_id` char(36) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `modified_by` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_folder` (`parent_folder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `folders`
--

INSERT INTO `folders` (`id`, `name`, `folder_type`, `parent_folder`, `has_child`, `is_group`, `is_dynamic`, `dynamic_query`, `assign_to_id`, `created_by`, `modified_by`, `deleted`) VALUES
('d715c836-f51a-a635-6712-539059ec2849', 'La mia Email', 'inbound', '', 1, 0, 1, 'SELECT emails.id polymorphic_id, ''Emails'' polymorphic_module FROM emails\n								   JOIN emails_text on emails.id = emails_text.email_id\n                                   WHERE (type = ''inbound'' OR status = ''inbound'') AND assigned_user_id = ''1'' AND emails.deleted = ''0'' AND status NOT IN (''sent'', ''archived'', ''draft'') AND type NOT IN (''out'', ''archived'', ''draft'')', '', '1', '1', 0),
('da80da5b-5ed4-98b4-b8b0-539059b130c9', 'Le mie Bozze', 'draft', 'd715c836-f51a-a635-6712-539059ec2849', 0, 0, 1, 'SELECT emails.id polymorphic_id, ''Emails'' polymorphic_module FROM emails\n								   JOIN emails_text on emails.id = emails_text.email_id\n                                   WHERE (type = ''draft'' OR status = ''draft'') AND assigned_user_id = ''1'' AND emails.deleted = ''0'' AND status NOT IN (''archived'') AND type NOT IN (''archived'')', '', '1', '1', 0),
('dabf5799-7d99-492e-824c-539059242b6f', 'Email Inviate', 'sent', 'd715c836-f51a-a635-6712-539059ec2849', 0, 0, 1, 'SELECT emails.id polymorphic_id, ''Emails'' polymorphic_module FROM emails\n								   JOIN emails_text on emails.id = emails_text.email_id\n                                   WHERE (type = ''out'' OR status = ''sent'') AND assigned_user_id = ''1'' AND emails.deleted = ''0'' AND status NOT IN (''archived'') AND type NOT IN (''archived'')', '', '1', '1', 0),
('db3c5ea2-9792-1d15-3c84-5390593c430a', 'Le mie Email Archiviate', 'archived', 'd715c836-f51a-a635-6712-539059ec2849', 0, 0, 1, '', '', '1', '1', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `folders_rel`
--

CREATE TABLE IF NOT EXISTS `folders_rel` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `polymorphic_module` varchar(25) DEFAULT NULL,
  `polymorphic_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_poly_module_poly_id` (`polymorphic_module`,`polymorphic_id`),
  KEY `idx_fr_id_deleted_poly` (`folder_id`,`deleted`,`polymorphic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `folders_subscriptions`
--

CREATE TABLE IF NOT EXISTS `folders_subscriptions` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `assigned_user_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder_id_assigned_user_id` (`folder_id`,`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `folders_subscriptions`
--

INSERT INTO `folders_subscriptions` (`id`, `folder_id`, `assigned_user_id`) VALUES
('d7544231-d597-1293-c976-53905912ac8f', 'd715c836-f51a-a635-6712-539059ec2849', '1'),
('da80df84-2970-85d8-d5d9-539059f72f09', 'da80da5b-5ed4-98b4-b8b0-539059b130c9', '1'),
('dafdd62b-9889-3fb1-e18d-53905993a813', 'dabf5799-7d99-492e-824c-539059242b6f', '1'),
('db3c5cf9-57e6-a84b-56fe-539059f62862', 'db3c5ea2-9792-1d15-3c84-5390593c430a', '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_events`
--

CREATE TABLE IF NOT EXISTS `fp_events` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `budget` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `invite_templates` varchar(100) DEFAULT NULL,
  `accept_redirect` varchar(255) DEFAULT NULL,
  `decline_redirect` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_events_audit`
--

CREATE TABLE IF NOT EXISTS `fp_events_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_fp_events_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_events_contacts_c`
--

CREATE TABLE IF NOT EXISTS `fp_events_contacts_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_contactsfp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_contactscontacts_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_contacts_alt` (`fp_events_contactsfp_events_ida`,`fp_events_contactscontacts_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_events_fp_event_delegates_1_c`
--

CREATE TABLE IF NOT EXISTS `fp_events_fp_event_delegates_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_fp_event_delegates_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_fp_event_delegates_1fp_event_delegates_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_events_fp_event_delegates_1_ida1` (`fp_events_fp_event_delegates_1fp_events_ida`),
  KEY `fp_events_fp_event_delegates_1_alt` (`fp_events_fp_event_delegates_1fp_event_delegates_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_events_fp_event_locations_1_c`
--

CREATE TABLE IF NOT EXISTS `fp_events_fp_event_locations_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_fp_event_locations_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_fp_event_locations_1fp_event_locations_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_events_fp_event_locations_1_alt` (`fp_events_fp_event_locations_1fp_events_ida`,`fp_events_fp_event_locations_1fp_event_locations_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_events_leads_1_c`
--

CREATE TABLE IF NOT EXISTS `fp_events_leads_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_leads_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_leads_1leads_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_leads_1_alt` (`fp_events_leads_1fp_events_ida`,`fp_events_leads_1leads_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_events_prospects_1_c`
--

CREATE TABLE IF NOT EXISTS `fp_events_prospects_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_prospects_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_prospects_1prospects_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_prospects_1_alt` (`fp_events_prospects_1fp_events_ida`,`fp_events_prospects_1prospects_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_event_locations`
--

CREATE TABLE IF NOT EXISTS `fp_event_locations` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `capacity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_event_locations_audit`
--

CREATE TABLE IF NOT EXISTS `fp_event_locations_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_fp_event_locations_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `fp_event_locations_fp_events_1_c`
--

CREATE TABLE IF NOT EXISTS `fp_event_locations_fp_events_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_event_locations_fp_events_1fp_event_locations_ida` varchar(36) DEFAULT NULL,
  `fp_event_locations_fp_events_1fp_events_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_event_locations_fp_events_1_ida1` (`fp_event_locations_fp_events_1fp_event_locations_ida`),
  KEY `fp_event_locations_fp_events_1_alt` (`fp_event_locations_fp_events_1fp_events_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `import_maps`
--

CREATE TABLE IF NOT EXISTS `import_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `source` varchar(36) DEFAULT NULL,
  `enclosure` varchar(1) DEFAULT ' ',
  `delimiter` varchar(1) DEFAULT ',',
  `module` varchar(36) DEFAULT NULL,
  `content` text,
  `default_values` text,
  `has_header` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_published` varchar(3) DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `idx_owner_module_name` (`assigned_user_id`,`module`,`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `inbound_email`
--

CREATE TABLE IF NOT EXISTS `inbound_email` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Active',
  `server_url` varchar(100) DEFAULT NULL,
  `email_user` varchar(100) DEFAULT NULL,
  `email_password` varchar(100) DEFAULT NULL,
  `port` int(5) DEFAULT NULL,
  `service` varchar(50) DEFAULT NULL,
  `mailbox` text,
  `delete_seen` tinyint(1) DEFAULT '0',
  `mailbox_type` varchar(10) DEFAULT NULL,
  `template_id` char(36) DEFAULT NULL,
  `stored_options` text,
  `group_id` char(36) DEFAULT NULL,
  `is_personal` tinyint(1) DEFAULT '0',
  `groupfolder_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `inbound_email_autoreply`
--

CREATE TABLE IF NOT EXISTS `inbound_email_autoreply` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `autoreplied_to` varchar(100) DEFAULT NULL,
  `ie_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ie_autoreplied_to` (`autoreplied_to`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `inbound_email_cache_ts`
--

CREATE TABLE IF NOT EXISTS `inbound_email_cache_ts` (
  `id` varchar(255) NOT NULL,
  `ie_timestamp` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_address_cache`
--

CREATE TABLE IF NOT EXISTS `jjwg_address_cache` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `lat` float(10,8) DEFAULT NULL,
  `lng` float(11,8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_address_cache_audit`
--

CREATE TABLE IF NOT EXISTS `jjwg_address_cache_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_address_cache_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_areas`
--

CREATE TABLE IF NOT EXISTS `jjwg_areas` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `coordinates` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_areas_audit`
--

CREATE TABLE IF NOT EXISTS `jjwg_areas_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_areas_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_maps`
--

CREATE TABLE IF NOT EXISTS `jjwg_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `distance` float(9,4) DEFAULT NULL,
  `unit_type` varchar(100) DEFAULT 'mi',
  `module_type` varchar(100) DEFAULT 'Accounts',
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_maps_audit`
--

CREATE TABLE IF NOT EXISTS `jjwg_maps_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_maps_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_maps_jjwg_areas_c`
--

CREATE TABLE IF NOT EXISTS `jjwg_maps_jjwg_areas_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `jjwg_maps_5304wg_maps_ida` varchar(36) DEFAULT NULL,
  `jjwg_maps_41f2g_areas_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jjwg_maps_jjwg_areas_alt` (`jjwg_maps_5304wg_maps_ida`,`jjwg_maps_41f2g_areas_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_maps_jjwg_markers_c`
--

CREATE TABLE IF NOT EXISTS `jjwg_maps_jjwg_markers_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `jjwg_maps_b229wg_maps_ida` varchar(36) DEFAULT NULL,
  `jjwg_maps_2e31markers_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jjwg_maps_jjwg_markers_alt` (`jjwg_maps_b229wg_maps_ida`,`jjwg_maps_2e31markers_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_markers`
--

CREATE TABLE IF NOT EXISTS `jjwg_markers` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `jjwg_maps_lat` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_lng` float(11,8) DEFAULT '0.00000000',
  `marker_image` varchar(100) DEFAULT 'company',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `jjwg_markers_audit`
--

CREATE TABLE IF NOT EXISTS `jjwg_markers_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_markers_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `job_queue`
--

CREATE TABLE IF NOT EXISTS `job_queue` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `scheduler_id` char(36) DEFAULT NULL,
  `execute_time` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `resolution` varchar(20) DEFAULT NULL,
  `message` text,
  `target` varchar(255) DEFAULT NULL,
  `data` text,
  `requeue` tinyint(1) DEFAULT '0',
  `retry_count` tinyint(4) DEFAULT NULL,
  `failure_count` tinyint(4) DEFAULT NULL,
  `job_delay` int(11) DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_status_scheduler` (`status`,`scheduler_id`),
  KEY `idx_status_time` (`status`,`execute_time`,`date_entered`),
  KEY `idx_status_entered` (`status`,`date_entered`),
  KEY `idx_status_modified` (`status`,`date_modified`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `leads`
--

CREATE TABLE IF NOT EXISTS `leads` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `converted` tinyint(1) DEFAULT '0',
  `refered_by` varchar(100) DEFAULT NULL,
  `lead_source` varchar(100) DEFAULT NULL,
  `lead_source_description` text,
  `status` varchar(100) DEFAULT NULL,
  `status_description` text,
  `reports_to_id` char(36) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_description` text,
  `contact_id` char(36) DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `opportunity_name` varchar(255) DEFAULT NULL,
  `opportunity_amount` varchar(50) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `portal_name` varchar(255) DEFAULT NULL,
  `portal_app` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lead_acct_name_first` (`account_name`,`deleted`),
  KEY `idx_lead_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_lead_del_stat` (`last_name`,`status`,`deleted`,`first_name`),
  KEY `idx_lead_opp_del` (`opportunity_id`,`deleted`),
  KEY `idx_leads_acct_del` (`account_id`,`deleted`),
  KEY `idx_del_user` (`deleted`,`assigned_user_id`),
  KEY `idx_lead_assigned` (`assigned_user_id`),
  KEY `idx_lead_contact` (`contact_id`),
  KEY `idx_reports_to` (`reports_to_id`),
  KEY `idx_lead_phone_work` (`phone_work`),
  KEY `idx_leads_id_del` (`id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `leads`
--

INSERT INTO `leads` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `salutation`, `first_name`, `last_name`, `title`, `department`, `do_not_call`, `phone_home`, `phone_mobile`, `phone_work`, `phone_other`, `phone_fax`, `primary_address_street`, `primary_address_city`, `primary_address_state`, `primary_address_postalcode`, `primary_address_country`, `alt_address_street`, `alt_address_city`, `alt_address_state`, `alt_address_postalcode`, `alt_address_country`, `assistant`, `assistant_phone`, `converted`, `refered_by`, `lead_source`, `lead_source_description`, `status`, `status_description`, `reports_to_id`, `account_name`, `account_description`, `contact_id`, `account_id`, `opportunity_id`, `opportunity_name`, `opportunity_amount`, `campaign_id`, `birthdate`, `portal_name`, `portal_app`, `website`) VALUES
('667f020f-5560-1b4b-44b4-538ffeb0a947', '2014-06-05 05:23:46', '2014-06-05 05:23:46', '1', '1', '', 0, '1', 'Dr.', 'Jean Paul', 'Sanchez', '', '', 0, NULL, '', '', NULL, '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, 0, '', 'Partner', '', 'Dead', '', NULL, '', NULL, '', '', '', NULL, '', '', NULL, NULL, NULL, 'http://');

-- --------------------------------------------------------

--
-- Struttura della tabella `leads_audit`
--

CREATE TABLE IF NOT EXISTS `leads_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_leads_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `leads_cstm`
--

CREATE TABLE IF NOT EXISTS `leads_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `leads_cstm`
--

INSERT INTO `leads_cstm` (`id_c`, `jjwg_maps_lng_c`, `jjwg_maps_lat_c`, `jjwg_maps_geocode_status_c`, `jjwg_maps_address_c`) VALUES
('667f020f-5560-1b4b-44b4-538ffeb0a947', 0.00000000, 0.00000000, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `linked_documents`
--

CREATE TABLE IF NOT EXISTS `linked_documents` (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_document` (`parent_type`,`parent_id`,`document_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `meetings`
--

CREATE TABLE IF NOT EXISTS `meetings` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `join_url` varchar(200) DEFAULT NULL,
  `host_url` varchar(400) DEFAULT NULL,
  `displayed_url` varchar(400) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `external_id` varchar(50) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `type` varchar(255) DEFAULT 'Sugar',
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT '0',
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_mtg_name` (`name`),
  KEY `idx_meet_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_meet_stat_del` (`assigned_user_id`,`status`,`deleted`),
  KEY `idx_meet_date_start` (`date_start`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `meetings_contacts`
--

CREATE TABLE IF NOT EXISTS `meetings_contacts` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_mtg_mtg` (`meeting_id`),
  KEY `idx_con_mtg_con` (`contact_id`),
  KEY `idx_meeting_contact` (`meeting_id`,`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `meetings_cstm`
--

CREATE TABLE IF NOT EXISTS `meetings_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `meetings_leads`
--

CREATE TABLE IF NOT EXISTS `meetings_leads` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_meeting_meeting` (`meeting_id`),
  KEY `idx_lead_meeting_lead` (`lead_id`),
  KEY `idx_meeting_lead` (`meeting_id`,`lead_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `meetings_users`
--

CREATE TABLE IF NOT EXISTS `meetings_users` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_mtg_mtg` (`meeting_id`),
  KEY `idx_usr_mtg_usr` (`user_id`),
  KEY `idx_meeting_users` (`meeting_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `portal_flag` tinyint(1) DEFAULT NULL,
  `embed_flag` tinyint(1) DEFAULT '0',
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_note_name` (`name`),
  KEY `idx_notes_parent` (`parent_id`,`parent_type`),
  KEY `idx_note_contact` (`contact_id`),
  KEY `idx_notes_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_consumer`
--

CREATE TABLE IF NOT EXISTS `oauth_consumer` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `c_key` varchar(255) DEFAULT NULL,
  `c_secret` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ckey` (`c_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_nonce`
--

CREATE TABLE IF NOT EXISTS `oauth_nonce` (
  `conskey` varchar(32) NOT NULL,
  `nonce` varchar(32) NOT NULL,
  `nonce_ts` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`conskey`,`nonce`),
  KEY `oauth_nonce_keyts` (`conskey`,`nonce_ts`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `oauth_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_tokens` (
  `id` char(36) NOT NULL,
  `secret` varchar(32) DEFAULT NULL,
  `tstate` varchar(1) DEFAULT NULL,
  `consumer` char(36) NOT NULL,
  `token_ts` bigint(20) DEFAULT NULL,
  `verify` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `callback_url` varchar(255) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`,`deleted`),
  KEY `oauth_state_ts` (`tstate`,`token_ts`),
  KEY `constoken_key` (`consumer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `opportunities`
--

CREATE TABLE IF NOT EXISTS `opportunities` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `opportunity_type` varchar(255) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `lead_source` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_usdollar` double DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `date_closed` date DEFAULT NULL,
  `next_step` varchar(100) DEFAULT NULL,
  `sales_stage` varchar(255) DEFAULT NULL,
  `probability` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_opp_name` (`name`),
  KEY `idx_opp_assigned` (`assigned_user_id`),
  KEY `idx_opp_id_deleted` (`id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `opportunities_audit`
--

CREATE TABLE IF NOT EXISTS `opportunities_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_opportunities_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `opportunities_contacts`
--

CREATE TABLE IF NOT EXISTS `opportunities_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_opp_con` (`contact_id`),
  KEY `idx_con_opp_opp` (`opportunity_id`),
  KEY `idx_opportunities_contacts` (`opportunity_id`,`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `opportunities_cstm`
--

CREATE TABLE IF NOT EXISTS `opportunities_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `outbound_email`
--

CREATE TABLE IF NOT EXISTS `outbound_email` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(15) DEFAULT 'user',
  `user_id` char(36) NOT NULL,
  `mail_sendtype` varchar(8) DEFAULT 'smtp',
  `mail_smtptype` varchar(20) DEFAULT 'other',
  `mail_smtpserver` varchar(100) DEFAULT NULL,
  `mail_smtpport` int(5) DEFAULT '0',
  `mail_smtpuser` varchar(100) DEFAULT NULL,
  `mail_smtppass` varchar(100) DEFAULT NULL,
  `mail_smtpauth_req` tinyint(1) DEFAULT '0',
  `mail_smtpssl` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `oe_user_id_idx` (`id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `outbound_email`
--

INSERT INTO `outbound_email` (`id`, `name`, `type`, `user_id`, `mail_sendtype`, `mail_smtptype`, `mail_smtpserver`, `mail_smtpport`, `mail_smtpuser`, `mail_smtppass`, `mail_smtpauth_req`, `mail_smtpssl`) VALUES
('c1b04291-cf04-24bf-4941-5388e3552de6', 'system', 'system', '1', 'SMTP', 'other', '', 25, '', '', 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `estimated_start_date` date DEFAULT NULL,
  `estimated_end_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `projects_accounts`
--

CREATE TABLE IF NOT EXISTS `projects_accounts` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_acct_proj` (`project_id`),
  KEY `idx_proj_acct_acct` (`account_id`),
  KEY `projects_accounts_alt` (`project_id`,`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `projects_bugs`
--

CREATE TABLE IF NOT EXISTS `projects_bugs` (
  `id` varchar(36) NOT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_bug_proj` (`project_id`),
  KEY `idx_proj_bug_bug` (`bug_id`),
  KEY `projects_bugs_alt` (`project_id`,`bug_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `projects_cases`
--

CREATE TABLE IF NOT EXISTS `projects_cases` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_case_proj` (`project_id`),
  KEY `idx_proj_case_case` (`case_id`),
  KEY `projects_cases_alt` (`project_id`,`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `projects_contacts`
--

CREATE TABLE IF NOT EXISTS `projects_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_con_proj` (`project_id`),
  KEY `idx_proj_con_con` (`contact_id`),
  KEY `projects_contacts_alt` (`project_id`,`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `projects_opportunities`
--

CREATE TABLE IF NOT EXISTS `projects_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_opp_proj` (`project_id`),
  KEY `idx_proj_opp_opp` (`opportunity_id`),
  KEY `projects_opportunities_alt` (`project_id`,`opportunity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `projects_products`
--

CREATE TABLE IF NOT EXISTS `projects_products` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_prod_project` (`project_id`),
  KEY `idx_proj_prod_product` (`product_id`),
  KEY `projects_products_alt` (`project_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `project_cstm`
--

CREATE TABLE IF NOT EXISTS `project_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `project_task`
--

CREATE TABLE IF NOT EXISTS `project_task` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `project_id` char(36) NOT NULL,
  `project_task_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `description` text,
  `predecessors` text,
  `date_start` date DEFAULT NULL,
  `time_start` int(11) DEFAULT NULL,
  `time_finish` int(11) DEFAULT NULL,
  `date_finish` date DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `duration_unit` text,
  `actual_duration` int(11) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  `date_due` date DEFAULT NULL,
  `time_due` time DEFAULT NULL,
  `parent_task_id` int(11) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT NULL,
  `order_number` int(11) DEFAULT '1',
  `task_number` int(11) DEFAULT NULL,
  `estimated_effort` int(11) DEFAULT NULL,
  `actual_effort` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `utilization` int(11) DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `project_task_audit`
--

CREATE TABLE IF NOT EXISTS `project_task_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_project_task_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prospects`
--

CREATE TABLE IF NOT EXISTS `prospects` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` date DEFAULT NULL,
  `lead_id` char(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_auto_tracker_key` (`tracker_key`),
  KEY `idx_prospects_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_prospecs_del_last` (`last_name`,`deleted`),
  KEY `idx_prospects_id_del` (`id`,`deleted`),
  KEY `idx_prospects_assigned` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `prospects_cstm`
--

CREATE TABLE IF NOT EXISTS `prospects_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prospect_lists`
--

CREATE TABLE IF NOT EXISTS `prospect_lists` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_type` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `description` text,
  `domain_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_prospect_list_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prospect_lists_prospects`
--

CREATE TABLE IF NOT EXISTS `prospect_lists_prospects` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_plp_pro_id` (`prospect_list_id`),
  KEY `idx_plp_rel_id` (`related_id`,`related_type`,`prospect_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prospect_list_campaigns`
--

CREATE TABLE IF NOT EXISTS `prospect_list_campaigns` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `campaign_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_pro_id` (`prospect_list_id`),
  KEY `idx_cam_id` (`campaign_id`),
  KEY `idx_prospect_list_campaigns` (`prospect_list_id`,`campaign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov1_aziende_audit`
--

CREATE TABLE IF NOT EXISTS `prov1_aziende_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_prov1_aziende_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov1_c_azienda`
--

CREATE TABLE IF NOT EXISTS `prov1_c_azienda` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `via` varchar(255) DEFAULT 'Nessun Indirizzo',
  `citta` varchar(255) DEFAULT 'Nessuna Citta',
  `provincia` varchar(255) DEFAULT 'Nessuna Provincia',
  `cap` int(255) DEFAULT '1',
  `stato` varchar(255) DEFAULT 'Nessun Stato',
  `partita_iva` varchar(255) DEFAULT 'Nessuna Partita Iva',
  `telefono` varchar(255) DEFAULT 'Nessun Numero',
  `fax` varchar(255) DEFAULT 'Nessun Fax',
  `cellulare` varchar(255) DEFAULT 'Nessun Cellulare',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov1_c_azienda`
--

INSERT INTO `prov1_c_azienda` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `via`, `citta`, `provincia`, `cap`, `stato`, `partita_iva`, `telefono`, `fax`, `cellulare`) VALUES
('e0950c3d-bea7-3cc0-265f-538fa20d6eed', 'Double-box Inc', '2014-06-04 22:48:04', '2014-06-04 22:48:04', '1', '1', NULL, 0, NULL, 'Matteoti', 'Pavia', 'Pavia', 12345, 'Italia', '123456789', '123456789', '123456789', '123456789'),
('601bebca-0dcd-85b1-af98-538fa22a9889', 'AstreaProject Srl', '2014-06-04 22:49:28', '2014-06-04 22:49:28', '1', '1', NULL, 0, NULL, 'Luca', 'Binasco', 'Milano', 12345, 'Italia', '123456788', '123456778', '123456778', '123456778'),
('c961c424-8fa7-5b08-33fd-538fa26032da', 'BENQ', '2014-06-04 22:50:06', '2014-06-04 22:50:06', '1', '1', NULL, 0, NULL, 'Mouse', 'Retorbido', 'Pavia', 27050, 'Italia', '123445678', '123345678', '123345678', '123345678');

-- --------------------------------------------------------

--
-- Struttura della tabella `prov1_c_azienda_audit`
--

CREATE TABLE IF NOT EXISTS `prov1_c_azienda_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_prov1_c_azienda_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov1_c_azienda_prov1_c_dipendenti_c`
--

CREATE TABLE IF NOT EXISTS `prov1_c_azienda_prov1_c_dipendenti_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida` varchar(36) DEFAULT NULL,
  `prov1_c_azienda_prov1_c_dipendentiprov1_c_dipendenti_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prov1_c_azienda_prov1_c_dipendenti_ida1` (`prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida`),
  KEY `prov1_c_azienda_prov1_c_dipendenti_alt` (`prov1_c_azienda_prov1_c_dipendentiprov1_c_dipendenti_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov1_c_azienda_prov1_c_dipendenti_c`
--

INSERT INTO `prov1_c_azienda_prov1_c_dipendenti_c` (`id`, `date_modified`, `deleted`, `prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida`, `prov1_c_azienda_prov1_c_dipendentiprov1_c_dipendenti_idb`) VALUES
('b1e8b46c-a8d1-e227-8763-538fa22d002e', '2014-06-04 22:50:56', 0, 'e0950c3d-bea7-3cc0-265f-538fa20d6eed', 'b0332842-6a2f-c034-f290-538fa2d2cfeb'),
('41b95908-caed-1523-432a-538fa212749e', '2014-06-04 22:51:40', 0, '601bebca-0dcd-85b1-af98-538fa22a9889', '4003d734-26c5-86cb-efdf-538fa2ac4b4a'),
('565bfdf2-56ee-b5f4-ed38-538fa3a082ea', '2014-06-04 22:52:24', 0, 'e0950c3d-bea7-3cc0-265f-538fa20d6eed', '4b5f453b-874d-fb4c-029f-538fa3f7a1df'),
('effd13c4-f28f-e7b8-a5a7-538fa372a442', '2014-06-04 22:53:03', 0, 'c961c424-8fa7-5b08-33fd-538fa26032da', 'ee479d9c-6f99-a151-b076-538fa361090c');

-- --------------------------------------------------------

--
-- Struttura della tabella `prov1_c_dipendenti`
--

CREATE TABLE IF NOT EXISTS `prov1_c_dipendenti` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `cognome` varchar(255) DEFAULT 'Nessun Cognome',
  `via` varchar(255) DEFAULT 'Nessuna Via',
  `citta` varchar(255) DEFAULT 'Nessuna Citta',
  `cap` int(255) DEFAULT '1',
  `provincia` varchar(255) DEFAULT 'Nessuna Provincia',
  `codice_fiscale` varchar(255) DEFAULT 'Nessun Codice Fiscale',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov1_c_dipendenti`
--

INSERT INTO `prov1_c_dipendenti` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `cognome`, `via`, `citta`, `cap`, `provincia`, `codice_fiscale`) VALUES
('b0332842-6a2f-c034-f290-538fa2d2cfeb', 'Jean Paul', '2014-06-04 22:50:56', '2014-06-04 22:50:56', '1', '1', NULL, 0, NULL, 'Sanchez', 'Cristoforo Colombo', 'Retorbido', 27050, 'Pavia', '12345789'),
('4003d734-26c5-86cb-efdf-538fa2ac4b4a', 'Ivan', '2014-06-04 22:51:40', '2014-06-04 22:51:40', '1', '1', NULL, 0, NULL, 'Giuliano', 'A.Volta', 'Retoribido', 27050, 'Pavia', '1234567898'),
('4b5f453b-874d-fb4c-029f-538fa3f7a1df', 'Valeria', '2014-06-04 22:52:24', '2014-06-04 22:52:24', '1', '1', NULL, 0, NULL, 'Galli', 'Cristoforo Colombo', 'Retorbido', 27050, 'Pavia', '12345678'),
('ee479d9c-6f99-a151-b076-538fa361090c', 'Micky', '2014-06-04 22:53:03', '2014-06-04 22:53:03', '1', '1', NULL, 0, NULL, 'Mouse', 'Buco', 'Pavia', 12345, 'Pavia', '123456787');

-- --------------------------------------------------------

--
-- Struttura della tabella `prov1_c_dipendenti_audit`
--

CREATE TABLE IF NOT EXISTS `prov1_c_dipendenti_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_prov1_c_dipendenti_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov1_dipendenti_audit`
--

CREATE TABLE IF NOT EXISTS `prov1_dipendenti_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_prov1_dipendenti_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_aziende`
--

CREATE TABLE IF NOT EXISTS `prov2_aziende` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov2_aziende`
--

INSERT INTO `prov2_aziende` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`) VALUES
('b1b90c4a-5434-341f-7f5e-538ae65a4228', 'DOUBLE-BOX GHZ', '2014-06-01 08:39:03', '2014-06-01 08:39:03', '1', '1', NULL, 0, '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_aziende_audit`
--

CREATE TABLE IF NOT EXISTS `prov2_aziende_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_prov2_aziende_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_cliente`
--

CREATE TABLE IF NOT EXISTS `prov2_f_cliente` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `cognome` varchar(255) DEFAULT 'Nessun Cognome',
  `codice_fiscale` varchar(255) DEFAULT 'Nessun Codice Fiscale',
  `via` varchar(255) DEFAULT 'Nessuna Via',
  `citta` varchar(255) DEFAULT 'Nessuna Citta',
  `provincia` varchar(255) DEFAULT 'Nessuna Provincia',
  `stato` varchar(255) DEFAULT 'Nessuno Stato',
  `telefono` varchar(255) DEFAULT 'Nessun Numero Telefonico',
  `data_nascita` date DEFAULT NULL,
  `stipula_contratto` date DEFAULT NULL,
  `fine_contratto` date DEFAULT NULL,
  `prezzo_affitto` decimal(26,6) DEFAULT '0.000000',
  `currency_id` char(36) DEFAULT NULL,
  `cauzione` decimal(26,6) DEFAULT '0.000000',
  `prezzo_totale` decimal(26,6) DEFAULT NULL,
  `iva` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov2_f_cliente`
--

INSERT INTO `prov2_f_cliente` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `cognome`, `codice_fiscale`, `via`, `citta`, `provincia`, `stato`, `telefono`, `data_nascita`, `stipula_contratto`, `fine_contratto`, `prezzo_affitto`, `currency_id`, `cauzione`, `prezzo_totale`, `iva`) VALUES
('3e27931a-c616-8474-5217-539015710b0e', 'Jean Paul', '2014-06-05 07:00:44', '2014-06-05 07:07:16', '1', '1', NULL, 1, NULL, 'Sanchez', 'Nessun Codice Fiscale', 'Nessuna Via', 'Nessuna Citta', 'Nessuna Provincia', 'Nessuno Stato', 'Nessun Numero Telefonico', '1990-08-15', '2014-06-05', '2014-07-03', '0.000000', NULL, '0.000000', '0.000000', 0),
('46c2e655-406c-d083-1ce3-539017cb6b78', 'Jean Paul', '2014-06-05 07:07:48', '2014-06-06 10:35:04', '1', '1', NULL, 0, '', 'Sanchez', '123456', 'Nessuna Via', 'Nessuna Citta', 'Nessuna Provincia', 'Nessuno Stato', 'Nessun Numero Telefonico', '1990-08-15', '2014-06-05', '2014-07-05', '400.000000', '', '150.000000', '665.500000', 21);

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_cliente_audit`
--

CREATE TABLE IF NOT EXISTS `prov2_f_cliente_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_prov2_f_cliente_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_cliente_prov2_f_contratti_c`
--

CREATE TABLE IF NOT EXISTS `prov2_f_cliente_prov2_f_contratti_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida` varchar(36) DEFAULT NULL,
  `prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prov2_f_cliente_prov2_f_contratti_ida1` (`prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida`),
  KEY `prov2_f_cliente_prov2_f_contratti_idb2` (`prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov2_f_cliente_prov2_f_contratti_c`
--

INSERT INTO `prov2_f_cliente_prov2_f_contratti_c` (`id`, `date_modified`, `deleted`, `prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida`, `prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb`) VALUES
('4098a397-79d7-d3b6-f2ef-5390159b028f', '2014-06-05 07:03:44', 1, '3e27931a-c616-8474-5217-539015710b0e', '580895b4-1648-8488-5430-539015f5639a'),
('4933f281-4cdf-a177-a27f-5390179ca156', '2014-06-05 07:07:48', 0, '46c2e655-406c-d083-1ce3-539017cb6b78', 'a58da1aa-9e84-d318-0203-5390167dfa5f');

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_contratti`
--

CREATE TABLE IF NOT EXISTS `prov2_f_contratti` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `seleziona_contratto` varchar(100) DEFAULT 'Giornaliero',
  `descrizione` varchar(255) DEFAULT 'Nessuna Descrizione',
  `prezzo_fisso` int(255) DEFAULT '1',
  `descrizione_veicolo` varchar(255) DEFAULT 'Nessuna Descrizione',
  `potenza_veicolo` int(255) DEFAULT '1',
  `velocita_massima` int(255) DEFAULT '50',
  `proprietario_veicolo` varchar(255) DEFAULT 'Nessuno',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov2_f_contratti`
--

INSERT INTO `prov2_f_contratti` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `seleziona_contratto`, `descrizione`, `prezzo_fisso`, `descrizione_veicolo`, `potenza_veicolo`, `velocita_massima`, `proprietario_veicolo`) VALUES
('874140f8-906e-04f8-c1ef-539013f37193', '12345', '2014-06-05 06:52:24', '2014-06-05 07:03:44', '1', '1', NULL, 1, NULL, 'Giornaliero', 'Nessuna Descrizione', 500, 'Nessuna Descrizione', 50, 50, 'Nessuno'),
('580895b4-1648-8488-5430-539015f5639a', '12346', '2014-06-05 06:58:59', '2014-06-05 07:03:44', '1', '1', NULL, 1, NULL, 'Giornaliero', 'Nessuna Descrizione', 1, 'Nessuna Descrizione', 1, 50, 'Nessuno'),
('a58da1aa-9e84-d318-0203-5390167dfa5f', '12345', '2014-06-05 07:04:05', '2014-06-05 07:04:05', '1', '1', NULL, 0, NULL, 'Giornaliero', 'Nessuna Descrizione', 1, 'Nessuna Descrizione', 1, 50, 'Paul'),
('c3370320-29f2-e94c-286b-53901677d6b1', '12347', '2014-06-05 07:04:31', '2014-06-05 07:04:31', '1', '1', NULL, 0, NULL, 'Giornaliero', 'Nessuna Descrizione', 1, 'Nessuna Descrizione', 1, 50, 'Ivan');

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_contratti_audit`
--

CREATE TABLE IF NOT EXISTS `prov2_f_contratti_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_prov2_f_contratti_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_contratti_prov2_f_cliente_c`
--

CREATE TABLE IF NOT EXISTS `prov2_f_contratti_prov2_f_cliente_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida` varchar(36) DEFAULT NULL,
  `prov2_f_contratti_prov2_f_clienteprov2_f_cliente_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prov2_f_contratti_prov2_f_cliente_ida1` (`prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida`),
  KEY `prov2_f_contratti_prov2_f_cliente_idb2` (`prov2_f_contratti_prov2_f_clienteprov2_f_cliente_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_potenziale_cliente`
--

CREATE TABLE IF NOT EXISTS `prov2_f_potenziale_cliente` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `cognome` varchar(255) DEFAULT 'Nessun Cognome',
  `numero_patente` varchar(255) DEFAULT 'Nessuno',
  `codice_fiscale` varchar(255) DEFAULT 'Nessun Codice Fiscale',
  `via` varchar(255) DEFAULT 'Nessuna Via',
  `citta` varchar(255) DEFAULT 'Nessuna Citta',
  `provincia` varchar(255) DEFAULT 'Nessuna Provincia',
  `stato` varchar(255) DEFAULT 'Nessuno Stato',
  `data_nascita` date DEFAULT NULL,
  `telefono` varchar(255) DEFAULT 'Nessuno',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov2_f_potenziale_cliente`
--

INSERT INTO `prov2_f_potenziale_cliente` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `cognome`, `numero_patente`, `codice_fiscale`, `via`, `citta`, `provincia`, `stato`, `data_nascita`, `telefono`) VALUES
('5858496c-fb2a-a1bc-d91c-53901819ab5f', 'Ivan', '2014-06-05 07:13:03', '2014-06-05 07:13:03', '1', '1', NULL, 0, NULL, 'Torso', '123456789', 'Nessun Codice Fiscale', 'Nessuna Via', 'Nessuna Citta', 'Nessuna Provincia', 'Nessuno Stato', NULL, 'Nessuno');

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_potenziale_cliente_audit`
--

CREATE TABLE IF NOT EXISTS `prov2_f_potenziale_cliente_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_prov2_f_potenziale_cliente_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_upload`
--

CREATE TABLE IF NOT EXISTS `prov2_f_upload` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `active_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `subcategory_id` varchar(100) DEFAULT NULL,
  `status_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov2_f_upload`
--

INSERT INTO `prov2_f_upload` (`id`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `document_name`, `filename`, `file_ext`, `file_mime_type`, `active_date`, `exp_date`, `category_id`, `subcategory_id`, `status_id`) VALUES
('a0f94aa1-76e8-7cd3-e8a7-5390f16f05aa', '2014-06-05 22:36:54', '2014-06-05 22:36:54', '1', '1', NULL, 0, NULL, 'contratto_01', 'capitanfindus.jpg', 'jpg', 'image/jpeg', '2014-06-06', NULL, 'Sales', 'Marketing Collateral', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_upload_audit`
--

CREATE TABLE IF NOT EXISTS `prov2_f_upload_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_prov2_f_upload_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `prov2_f_upload_prov2_f_cliente_c`
--

CREATE TABLE IF NOT EXISTS `prov2_f_upload_prov2_f_cliente_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `prov2_f_upload_prov2_f_clienteprov2_f_upload_ida` varchar(36) DEFAULT NULL,
  `prov2_f_upload_prov2_f_clienteprov2_f_cliente_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prov2_f_upload_prov2_f_cliente_ida1` (`prov2_f_upload_prov2_f_clienteprov2_f_upload_ida`),
  KEY `prov2_f_upload_prov2_f_cliente_idb2` (`prov2_f_upload_prov2_f_clienteprov2_f_cliente_idb`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prov2_f_upload_prov2_f_cliente_c`
--

INSERT INTO `prov2_f_upload_prov2_f_cliente_c` (`id`, `date_modified`, `deleted`, `prov2_f_upload_prov2_f_clienteprov2_f_upload_ida`, `prov2_f_upload_prov2_f_clienteprov2_f_cliente_idb`) VALUES
('a75262eb-6507-d46e-28df-5390f121a583', '2014-06-05 22:36:54', 0, 'a0f94aa1-76e8-7cd3-e8a7-5390f16f05aa', '46c2e655-406c-d083-1ce3-539017cb6b78');

-- --------------------------------------------------------

--
-- Struttura della tabella `relationships`
--

CREATE TABLE IF NOT EXISTS `relationships` (
  `id` char(36) NOT NULL,
  `relationship_name` varchar(150) DEFAULT NULL,
  `lhs_module` varchar(100) DEFAULT NULL,
  `lhs_table` varchar(64) DEFAULT NULL,
  `lhs_key` varchar(64) DEFAULT NULL,
  `rhs_module` varchar(100) DEFAULT NULL,
  `rhs_table` varchar(64) DEFAULT NULL,
  `rhs_key` varchar(64) DEFAULT NULL,
  `join_table` varchar(64) DEFAULT NULL,
  `join_key_lhs` varchar(64) DEFAULT NULL,
  `join_key_rhs` varchar(64) DEFAULT NULL,
  `relationship_type` varchar(64) DEFAULT NULL,
  `relationship_role_column` varchar(64) DEFAULT NULL,
  `relationship_role_column_value` varchar(50) DEFAULT NULL,
  `reverse` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_rel_name` (`relationship_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `relationships`
--

INSERT INTO `relationships` (`id`, `relationship_name`, `lhs_module`, `lhs_table`, `lhs_key`, `rhs_module`, `rhs_table`, `rhs_key`, `join_table`, `join_key_lhs`, `join_key_rhs`, `relationship_type`, `relationship_role_column`, `relationship_role_column_value`, `reverse`, `deleted`) VALUES
('cff5f8fc-82eb-9577-f6c8-539198e0292c', 'leads_modified_user', 'Users', 'users', 'id', 'Leads', 'leads', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d034760b-3ce8-19d5-05e7-53919803f176', 'leads_created_by', 'Users', 'users', 'id', 'Leads', 'leads', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d0730f17-7c08-4038-a064-539198175a88', 'leads_assigned_user', 'Users', 'users', 'id', 'Leads', 'leads', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d0b185dd-7b6d-0b30-2226-539198992fc9', 'leads_email_addresses', 'Leads', 'leads', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Leads', 0, 0),
('d0f00003-b01e-7f50-66de-53919800d61d', 'leads_email_addresses_primary', 'Leads', 'leads', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('d12e8fb1-84ea-a41c-ae7e-539198f02257', 'lead_direct_reports', 'Leads', 'leads', 'id', 'Leads', 'leads', 'reports_to_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d16d0e15-cb49-af61-3709-53919870571b', 'lead_tasks', 'Leads', 'leads', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('d1ab806d-b219-06fd-93f5-53919810e94e', 'lead_notes', 'Leads', 'leads', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('d1ea0cff-24e7-95c9-3cb5-539198d2022e', 'lead_meetings', 'Leads', 'leads', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('d22882b8-6350-b5ea-e5ec-539198d0db6d', 'lead_calls', 'Leads', 'leads', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('d2670a6f-8282-1196-d03e-53919859e7dd', 'lead_emails', 'Leads', 'leads', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Leads', 0, 0),
('d2a5880d-3a3d-0f1d-24fb-539198de059d', 'lead_campaign_log', 'Leads', 'leads', 'id', 'CampaignLog', 'campaign_log', 'target_id', NULL, NULL, NULL, 'one-to-many', 'target_type', 'Leads', 0, 0),
('d39f8490-ade0-5753-084e-539198a10bc9', 'cases_modified_user', 'Users', 'users', 'id', 'Cases', 'cases', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d3de0d53-136a-1502-e1a4-539198b5b8b4', 'cases_created_by', 'Users', 'users', 'id', 'Cases', 'cases', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d41c8aa9-330c-7e11-b5c4-539198299414', 'cases_assigned_user', 'Users', 'users', 'id', 'Cases', 'cases', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d45b0af9-5c5b-8aad-ce81-5391985f89b0', 'case_calls', 'Cases', 'cases', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('d4998e43-ade2-0672-9a19-539198b20e91', 'case_tasks', 'Cases', 'cases', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('d4d8196c-7b2e-cadb-ef4a-5391982ff0d5', 'case_notes', 'Cases', 'cases', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('d5169cad-b206-c86d-3971-5391986f02e9', 'case_meetings', 'Cases', 'cases', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('d5551c89-f0d4-3512-6824-5391986f14e1', 'case_emails', 'Cases', 'cases', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Cases', 0, 0),
('d61092bd-1fd4-eb98-3b5c-53919801ae23', 'bugs_modified_user', 'Users', 'users', 'id', 'Bugs', 'bugs', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d64f1e8f-757f-de1a-92b8-5391985a9057', 'bugs_created_by', 'Users', 'users', 'id', 'Bugs', 'bugs', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d68d9029-8c2c-23e6-8120-5391985c33a3', 'bugs_assigned_user', 'Users', 'users', 'id', 'Bugs', 'bugs', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d68d926b-dd01-0218-e4ef-539198e64d30', 'bug_tasks', 'Bugs', 'bugs', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('d6cc1961-4622-54f0-3066-539198f1278f', 'bug_meetings', 'Bugs', 'bugs', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('d7491a29-2ada-6258-ba82-53919872e19a', 'bug_calls', 'Bugs', 'bugs', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('d7879dbc-7ce0-4fcf-956c-53919855aa87', 'bug_emails', 'Bugs', 'bugs', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('d7c61266-043d-440a-ded4-5391986c5a2b', 'bug_notes', 'Bugs', 'bugs', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Bugs', 0, 0),
('d8049672-c076-bf5c-793f-5391982aa4dc', 'bugs_release', 'Releases', 'releases', 'id', 'Bugs', 'bugs', 'found_in_release', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d84310f6-0524-b842-46bd-539198304818', 'bugs_fixed_in_release', 'Releases', 'releases', 'id', 'Bugs', 'bugs', 'fixed_in_release', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d8feab12-c684-db59-64f3-5391985f3066', 'user_direct_reports', 'Users', 'users', 'id', 'Users', 'users', 'reports_to_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('d93d23dc-9fdf-7d82-7384-53919822cff7', 'users_email_addresses', 'Users', 'users', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Users', 0, 0),
('d97ba484-db0b-932a-b438-539198229db3', 'users_email_addresses_primary', 'Users', 'users', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('d9f8a0ca-ed6b-be49-390f-539198acdb7b', 'campaignlog_contact', 'CampaignLog', 'campaign_log', 'related_id', 'Contacts', 'contacts', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('da3725c2-382f-be20-cc9b-5391980f5120', 'campaignlog_lead', 'CampaignLog', 'campaign_log', 'related_id', 'Leads', 'leads', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('da75a21a-407d-7a78-4cf3-539198a429c3', 'campaignlog_created_opportunities', 'CampaignLog', 'campaign_log', 'related_id', 'Opportunities', 'opportunities', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('dab42a84-c692-4aa0-d21a-53919806803c', 'campaignlog_targeted_users', 'CampaignLog', 'campaign_log', 'target_id', 'Users', 'users', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('daf2a8b7-24ee-9ee2-9e29-539198eafbc3', 'campaignlog_sent_emails', 'CampaignLog', 'campaign_log', 'related_id', 'Emails', 'emails', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('db6fa6c8-eb7c-fbae-d1dc-539198257c87', 'projects_notes', 'Project', 'project', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0),
('dbae289e-ed46-0184-5034-539198959ef6', 'projects_tasks', 'Project', 'project', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0),
('dbeca04e-1379-952d-8171-539198d6ac72', 'projects_meetings', 'Project', 'project', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0),
('dc2b21cb-26d6-313a-50d7-539198a835da', 'projects_calls', 'Project', 'project', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0),
('dc69aeca-d831-4d40-0c02-53919806bb09', 'projects_emails', 'Project', 'project', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Project', 0, 0),
('dca82ea7-2968-209d-4826-5391982c1b1b', 'projects_project_tasks', 'Project', 'project', 'id', 'ProjectTask', 'project_task', 'project_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('dce6a55a-2f81-bcf6-31a8-53919841ef4b', 'projects_assigned_user', 'Users', 'users', 'id', 'Project', 'project', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('dd252ff5-ce2a-ee19-8f18-539198d8f326', 'projects_modified_user', 'Users', 'users', 'id', 'Project', 'project', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('dd63b307-a664-e442-7a87-5391987adab9', 'projects_created_by', 'Users', 'users', 'id', 'Project', 'project', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('dde0bf66-2575-c481-684a-539198a33359', 'project_tasks_notes', 'ProjectTask', 'project_task', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('de1f3418-5925-0041-7c7d-5391985bbb1c', 'project_tasks_tasks', 'ProjectTask', 'project_task', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('de5db28a-4d52-8bfe-6bb6-5391988897e5', 'project_tasks_meetings', 'ProjectTask', 'project_task', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('de9c3109-1d72-94ab-e4c7-539198a5715c', 'project_tasks_calls', 'ProjectTask', 'project_task', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('dedab06a-7fb6-09b6-0893-5391984da4b7', 'project_tasks_emails', 'ProjectTask', 'project_task', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'ProjectTask', 0, 0),
('df1938dc-2e37-58aa-d135-539198bcf0fd', 'project_tasks_assigned_user', 'Users', 'users', 'id', 'ProjectTask', 'project_task', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('df57b052-7b72-3afa-9bec-5391989d2062', 'project_tasks_modified_user', 'Users', 'users', 'id', 'ProjectTask', 'project_task', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('df96302f-e2b0-da94-1c0d-539198e60bfa', 'project_tasks_created_by', 'Users', 'users', 'id', 'ProjectTask', 'project_task', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e051bdfd-2855-8792-c5f4-5391983f837c', 'campaigns_modified_user', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e0903dad-ed80-415f-0ac3-539198c8f5bb', 'campaigns_created_by', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e0ceb58f-35a3-87a6-c3c3-53919887665b', 'campaigns_assigned_user', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e10d3d67-9d1f-f63f-304e-539198d3e24c', 'campaign_accounts', 'Campaigns', 'campaigns', 'id', 'Accounts', 'accounts', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e14bbd2f-e4a7-d7b6-2156-539198ac2c93', 'campaign_contacts', 'Campaigns', 'campaigns', 'id', 'Contacts', 'contacts', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e18a4325-af87-9e6b-ed38-53919841fe62', 'campaign_leads', 'Campaigns', 'campaigns', 'id', 'Leads', 'leads', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e1c8c677-8cf6-9d63-e838-539198f22c5f', 'campaign_prospects', 'Campaigns', 'campaigns', 'id', 'Prospects', 'prospects', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e20748fb-6572-82df-a538-5391985ae6b9', 'campaign_opportunities', 'Campaigns', 'campaigns', 'id', 'Opportunities', 'opportunities', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e245cd4e-b02e-eb44-0518-5391983356de', 'campaign_email_marketing', 'Campaigns', 'campaigns', 'id', 'EmailMarketing', 'email_marketing', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e284407c-16ae-cddb-3ea8-5391980e228b', 'campaign_emailman', 'Campaigns', 'campaigns', 'id', 'EmailMan', 'emailman', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e2c2ca2f-9c98-52ee-3ce6-53919855d736', 'campaign_campaignlog', 'Campaigns', 'campaigns', 'id', 'CampaignLog', 'campaign_log', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e3014274-2215-0785-4862-5391989af7b4', 'campaign_assigned_user', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e33fc112-6463-6d84-487a-5391989409f5', 'campaign_modified_user', 'Users', 'users', 'id', 'Campaigns', 'campaigns', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e3fb4be3-1ead-d4de-1757-539198b06bc2', 'prospectlists_assigned_user', 'Users', 'users', 'id', 'prospectlists', 'prospect_lists', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e4f54faf-4e14-9a3b-1c45-539198f55da7', 'prospects_modified_user', 'Users', 'users', 'id', 'Prospects', 'prospects', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e533cebd-74ab-7c61-bbca-5391985e9a22', 'prospects_created_by', 'Users', 'users', 'id', 'Prospects', 'prospects', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e5724ced-2531-33c0-8317-5391981d3a24', 'prospects_assigned_user', 'Users', 'users', 'id', 'Prospects', 'prospects', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e5b0cbf7-efa8-b4cb-c6ae-5391988b50ea', 'prospects_email_addresses', 'Prospects', 'prospects', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Prospects', 0, 0),
('e5ef5d38-8711-4fae-9d02-5391983ffed5', 'prospects_email_addresses_primary', 'Prospects', 'prospects', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('e62dd452-2954-841b-a283-5391989649ae', 'prospect_tasks', 'Prospects', 'prospects', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('e66c5fe4-f38b-2336-ac8e-539198b63912', 'prospect_notes', 'Prospects', 'prospects', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('e6aaddb3-4732-fabc-58cf-5391984ef129', 'prospect_meetings', 'Prospects', 'prospects', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('e6e95a25-c253-aba0-2fd1-539198e10f7c', 'prospect_calls', 'Prospects', 'prospects', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('e727d35d-5555-6476-363d-539198c0c2e7', 'prospect_emails', 'Prospects', 'prospects', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Prospects', 0, 0),
('e7665baf-60f5-1342-2b73-5391989934b9', 'prospect_campaign_log', 'Prospects', 'prospects', 'id', 'CampaignLog', 'campaign_log', 'target_id', NULL, NULL, NULL, 'one-to-many', 'target_type', 'Prospects', 0, 0),
('e7e3586f-b9a1-573f-1425-539198ffcbff', 'email_template_email_marketings', 'EmailTemplates', 'email_templates', 'id', 'EmailMarketing', 'email_marketing', 'template_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e821d9f8-cea1-7b8b-2ab0-53919858ed3b', 'campaign_campaigntrakers', 'Campaigns', 'campaigns', 'id', 'CampaignTrackers', 'campaign_trkrs', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e95a55de-0f30-f18a-fe4c-5391985eadd9', 'schedulers_created_by_rel', 'Users', 'users', 'id', 'Schedulers', 'schedulers', 'created_by', NULL, NULL, NULL, 'one-to-one', NULL, NULL, 0, 0),
('e998d4e7-f4ac-b14d-cca2-539198f28de8', 'schedulers_modified_user_id_rel', 'Users', 'users', 'id', 'Schedulers', 'schedulers', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('e9d750ac-4670-20f8-0933-539198b2d33d', 'schedulers_jobs_rel', 'Schedulers', 'schedulers', 'id', 'SchedulersJobs', 'job_queue', 'scheduler_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ea546ef1-5eb4-d2b0-12a4-5391982aab15', 'schedulersjobs_assigned_user', 'Users', 'users', 'id', 'SchedulersJobs', 'schedulersjobs', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('eb4e693d-ec1a-27cf-d1ea-539198225866', 'contacts_modified_user', 'Users', 'users', 'id', 'Contacts', 'contacts', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('eb8ce7a3-ba76-2709-478d-5391987dc5db', 'contacts_created_by', 'Users', 'users', 'id', 'Contacts', 'contacts', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ebcb6a08-ddd3-9870-d421-539198a8e710', 'contacts_assigned_user', 'Users', 'users', 'id', 'Contacts', 'contacts', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ec09e432-ebc4-f844-c872-5391983765bb', 'contacts_email_addresses', 'Contacts', 'contacts', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Contacts', 0, 0),
('ec486d9c-b69d-a466-07d0-539198a80630', 'contacts_email_addresses_primary', 'Contacts', 'contacts', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('ec86ed31-1ee4-5b44-0c73-5391989e9443', 'contact_direct_reports', 'Contacts', 'contacts', 'id', 'Contacts', 'contacts', 'reports_to_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ecc5626e-0f7e-3f51-c464-53919895697c', 'contact_leads', 'Contacts', 'contacts', 'id', 'Leads', 'leads', 'contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ecc5619d-86cc-9896-4f76-539198425ba1', 'contact_notes', 'Contacts', 'contacts', 'id', 'Notes', 'notes', 'contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ed03ed8a-35e5-f3ad-f69e-539198cd11ee', 'contact_tasks', 'Contacts', 'contacts', 'id', 'Tasks', 'tasks', 'contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ed426347-d87b-47ff-0d6f-539198705138', 'contact_tasks_parent', 'Contacts', 'contacts', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Contacts', 0, 0),
('ed80efd2-1b8e-ace1-5dc0-5391984e653b', 'contact_notes_parent', 'Contacts', 'contacts', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Contacts', 0, 0),
('edbf60f7-efbd-1930-ddf7-53919828d291', 'contact_campaign_log', 'Contacts', 'contacts', 'id', 'CampaignLog', 'campaign_log', 'target_id', NULL, NULL, NULL, 'one-to-many', 'target_type', 'Contacts', 0, 0),
('edfde18b-c04a-e991-6f5c-5391986295d8', 'contact_aos_quotes', 'Contacts', 'contacts', 'id', 'AOS_Quotes', 'aos_quotes', 'billing_contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ee3c6592-01e9-1015-6b75-5391980f5983', 'contact_aos_invoices', 'Contacts', 'contacts', 'id', 'AOS_Invoices', 'aos_invoices', 'billing_contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ee7af4c0-13c2-99e8-1730-5391983d752a', 'contact_aos_contracts', 'Contacts', 'contacts', 'id', 'AOS_Contracts', 'aos_contracts', 'contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('eeb97be4-3a34-2c67-0852-539198029bd7', 'contacts_aop_case_updates', 'Contacts', 'contacts', 'id', 'AOP_Case_Updates', 'aop_case_updates', 'contact_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('efb37c52-177f-9d62-04d3-5391985d527c', 'accounts_modified_user', 'Users', 'users', 'id', 'Accounts', 'accounts', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f030795b-df76-7bde-36dc-5391987f9eaa', 'accounts_created_by', 'Users', 'users', 'id', 'Accounts', 'accounts', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f06efa4d-7cf3-6852-723c-539198dc55ba', 'accounts_assigned_user', 'Users', 'users', 'id', 'Accounts', 'accounts', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f0ad7bc1-ba03-a0f2-c3e5-5391981b1294', 'accounts_email_addresses', 'Accounts', 'accounts', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'Accounts', 0, 0),
('f0ebf474-2844-8843-4734-539198823f6a', 'accounts_email_addresses_primary', 'Accounts', 'accounts', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('f12a7cd8-fc25-5dd6-056c-53919852238d', 'member_accounts', 'Accounts', 'accounts', 'id', 'Accounts', 'accounts', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f12a7a2c-4633-16e1-6a54-539198fdcc84', 'account_cases', 'Accounts', 'accounts', 'id', 'Cases', 'cases', 'account_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f168f58f-d6cb-fd47-25dd-539198711843', 'account_tasks', 'Accounts', 'accounts', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('f1a774e0-b45d-b64c-cbd4-539198533dfa', 'account_notes', 'Accounts', 'accounts', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('f1e5f7d7-203d-e539-27ac-5391987c588e', 'account_meetings', 'Accounts', 'accounts', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('f22476bb-8c31-63d9-8a5b-5391983de211', 'account_calls', 'Accounts', 'accounts', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('f262f965-8f82-9c3b-2d15-539198a4ad7c', 'account_emails', 'Accounts', 'accounts', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Accounts', 0, 0),
('f2a18a64-11e3-2258-60c3-5391987372a5', 'account_leads', 'Accounts', 'accounts', 'id', 'Leads', 'leads', 'account_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f2e00a72-7f3c-3c2b-34bc-539198efe38c', 'account_campaign_log', 'Accounts', 'accounts', 'id', 'CampaignLog', 'campaign_log', 'target_id', NULL, NULL, NULL, 'one-to-many', 'target_type', 'Accounts', 0, 0),
('f31e8a20-042a-3443-52e8-5391981c6663', 'account_aos_quotes', 'Accounts', 'accounts', 'id', 'AOS_Quotes', 'aos_quotes', 'billing_account_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f35d013e-b4d0-558a-0cb6-5391985f0cb6', 'account_aos_invoices', 'Accounts', 'accounts', 'id', 'AOS_Invoices', 'aos_invoices', 'billing_account_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f39b821a-318f-aad6-5d79-5391986c7361', 'account_aos_contracts', 'Accounts', 'accounts', 'id', 'AOS_Contracts', 'aos_contracts', 'contract_account_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('33000c69-e44b-0d37-8aba-539198503e56', 'opportunities_modified_user', 'Users', 'users', 'id', 'Opportunities', 'opportunities', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('71800747-a825-e25a-48ed-539198beb089', 'opportunities_created_by', 'Users', 'users', 'id', 'Opportunities', 'opportunities', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('b000009a-0dd9-4ea5-e0f5-5391982f59f2', 'opportunities_assigned_user', 'Users', 'users', 'id', 'Opportunities', 'opportunities', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('ee800d84-3f3a-3add-ec5a-5391984fca42', 'opportunity_calls', 'Opportunities', 'opportunities', 'id', 'Calls', 'calls', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('12d006f0-e675-6232-8524-539198622772', 'opportunity_meetings', 'Opportunities', 'opportunities', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('16b80f94-6ad7-9ba6-2ca8-5391982374a3', 'opportunity_tasks', 'Opportunities', 'opportunities', 'id', 'Tasks', 'tasks', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('1aa00c48-8891-c7ff-cbe1-53919814bf9a', 'opportunity_notes', 'Opportunities', 'opportunities', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('1aa00737-09ae-f7f5-4e00-539198580846', 'opportunity_emails', 'Opportunities', 'opportunities', 'id', 'Emails', 'emails', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Opportunities', 0, 0),
('1e880a96-bd96-8b7e-25f2-5391984cbf7d', 'opportunity_leads', 'Opportunities', 'opportunities', 'id', 'Leads', 'leads', 'opportunity_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('227000a9-6b96-4e27-b8cc-539198714f3f', 'opportunity_currencies', 'Opportunities', 'opportunities', 'currency_id', 'Currencies', 'currencies', 'id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('265806d0-a8e0-a529-bc00-539198af2cdf', 'opportunities_campaign', 'Campaigns', 'campaigns', 'id', 'Opportunities', 'opportunities', 'campaign_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2a4009d2-391e-d530-9cff-539198057953', 'opportunity_aos_quotes', 'Opportunities', 'opportunities', 'id', 'AOS_Quotes', 'aos_quotes', 'opportunity_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2e290bd8-19cb-8dc0-e75c-5391988afa05', 'opportunity_aos_contracts', 'Opportunities', 'opportunities', 'id', 'AOS_Contracts', 'aos_contracts', 'opportunity_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('35f9003e-5ff5-28c8-f489-539198923621', 'emailtemplates_assigned_user', 'Users', 'users', 'id', 'EmailTemplates', 'email_templates', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('41b10aef-0adf-ec60-5e4e-53919812abd8', 'notes_assigned_user', 'Users', 'users', 'id', 'Notes', 'notes', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('45990a81-9eb8-ee3d-64db-53919809d178', 'notes_modified_user', 'Users', 'users', 'id', 'Notes', 'notes', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('498106bc-c19b-6ee2-cd56-53919875e1e5', 'notes_created_by', 'Users', 'users', 'id', 'Notes', 'notes', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('55390f0c-7ea8-e57b-9ed6-5391987e7184', 'calls_modified_user', 'Users', 'users', 'id', 'Calls', 'calls', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('59210fcc-8137-7fe0-818d-53919888b9d4', 'calls_created_by', 'Users', 'users', 'id', 'Calls', 'calls', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('5d0908aa-4fb4-af1a-06ad-53919840052c', 'calls_assigned_user', 'Users', 'users', 'id', 'Calls', 'calls', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('60f10f9e-6a8d-f385-09a2-539198c0bac0', 'calls_notes', 'Calls', 'calls', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Calls', 0, 0),
('64d906f1-95bc-b661-5035-539198d3a466', 'calls_reschedule', 'Calls', 'calls', 'id', 'Calls_Reschedule', 'calls_reschedule', 'call_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('709208bd-bde1-dd0d-6a22-539198fb39e1', 'emails_assigned_user', 'Users', 'users', 'id', 'Emails', 'emails', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('747a024f-d900-4d8d-8b0a-539198c697b9', 'emails_modified_user', 'Users', 'users', 'id', 'Emails', 'emails', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('7862028b-704d-8eab-ed59-539198aa98cb', 'emails_created_by', 'Users', 'users', 'id', 'Emails', 'emails', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('7c4a0de1-080b-5b4f-9622-5391981a77d7', 'emails_notes_rel', 'Emails', 'emails', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('8032027d-57b9-929c-888f-539198f8c025', 'emails_contacts_rel', 'Emails', 'emails', 'id', 'Contacts', 'contacts', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Contacts', 0, 0),
('841a044c-ae00-5890-de2b-539198da70ee', 'emails_accounts_rel', 'Emails', 'emails', 'id', 'Accounts', 'accounts', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Accounts', 0, 0),
('841a0e5b-f5a2-5455-e03c-5391983b167f', 'emails_leads_rel', 'Emails', 'emails', 'id', 'Leads', 'leads', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Leads', 0, 0),
('88020a8e-3fd5-b431-5bd2-539198952782', 'emails_meetings_rel', 'Emails', 'emails', 'id', 'Meetings', 'meetings', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('97a20919-9947-95ac-1d85-53919874c1a7', 'meetings_modified_user', 'Users', 'users', 'id', 'Meetings', 'meetings', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('97a20e2e-ea3d-c593-1141-539198a8630a', 'meetings_created_by', 'Users', 'users', 'id', 'Meetings', 'meetings', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('9b8a0ebe-e987-6646-d36e-53919849e616', 'meetings_assigned_user', 'Users', 'users', 'id', 'Meetings', 'meetings', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('9f720378-1ebe-a899-bf46-539198ef26be', 'meetings_notes', 'Meetings', 'meetings', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'Meetings', 0, 0),
('ab2a0571-e363-d980-5f31-539198f2d341', 'tasks_modified_user', 'Users', 'users', 'id', 'Tasks', 'tasks', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('af120282-69c4-efb4-a54a-539198f74cc2', 'tasks_created_by', 'Users', 'users', 'id', 'Tasks', 'tasks', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('b2fa069f-fbfc-f101-800b-5391983b4d6f', 'tasks_assigned_user', 'Users', 'users', 'id', 'Tasks', 'tasks', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('b6e30f82-83f5-4834-9a71-539198ec3018', 'tasks_notes', 'Tasks', 'tasks', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f563027e-9f19-59ac-9dd3-5391986285a1', 'documents_modified_user', 'Users', 'users', 'id', 'Documents', 'documents', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('f94c0581-98f8-a926-6bd7-539198a39298', 'documents_created_by', 'Users', 'users', 'id', 'Documents', 'documents', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('fd340587-17ba-0d2b-3f3e-539198fa05cb', 'documents_assigned_user', 'Users', 'users', 'id', 'Documents', 'documents', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1011cccd-5f06-761b-793c-539198e21c28', 'document_revisions', 'Documents', 'documents', 'id', 'DocumentRevisions', 'document_revisions', 'document_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('108ec7cc-96e1-d66c-0db9-5391987ec0de', 'revisions_created_by', 'Users', 'users', 'id', 'DocumentRevisions', 'document_revisions', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1188c3b2-3762-ea94-dfb1-5391985f20d5', 'inbound_email_created_by', 'Users', 'users', 'id', 'InboundEmail', 'inbound_email', 'created_by', NULL, NULL, NULL, 'one-to-one', NULL, NULL, 0, 0),
('11c74c28-6164-6c6f-a1e7-539198c4801b', 'inbound_email_modified_user_id', 'Users', 'users', 'id', 'InboundEmail', 'inbound_email', 'modified_user_id', NULL, NULL, NULL, 'one-to-one', NULL, NULL, 0, 0),
('12444017-a1d2-d5cd-224e-539198aae3b1', 'saved_search_assigned_user', 'Users', 'users', 'id', 'SavedSearch', 'saved_search', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('13bb49a2-31b0-da02-d06a-539198d76338', 'sugarfeed_modified_user', 'Users', 'users', 'id', 'SugarFeed', 'sugarfeed', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('13f9dab5-3f72-7e29-4bb4-539198d87083', 'sugarfeed_created_by', 'Users', 'users', 'id', 'SugarFeed', 'sugarfeed', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('14385c8b-f56e-75a3-9615-539198e6b8d4', 'sugarfeed_assigned_user', 'Users', 'users', 'id', 'SugarFeed', 'sugarfeed', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('14f3d752-e318-b372-3cca-5391989e9020', 'eapm_modified_user', 'Users', 'users', 'id', 'EAPM', 'eapm', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('15325f4b-8937-1e85-b47e-53919872105a', 'eapm_created_by', 'Users', 'users', 'id', 'EAPM', 'eapm', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1570dffa-1aca-eb7c-589b-5391982e814d', 'eapm_assigned_user', 'Users', 'users', 'id', 'EAPM', 'eapm', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('15edda6b-f945-386b-99ec-53919850a658', 'oauthkeys_modified_user', 'Users', 'users', 'id', 'OAuthKeys', 'oauthkeys', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('162c5f9b-181a-d6ff-5329-539198b23f59', 'oauthkeys_created_by', 'Users', 'users', 'id', 'OAuthKeys', 'oauthkeys', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('166ad42d-e234-98d0-6b64-539198f0861a', 'oauthkeys_assigned_user', 'Users', 'users', 'id', 'OAuthKeys', 'oauthkeys', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('16e7d783-3910-a209-49ff-5391983d6a91', 'consumer_tokens', 'OAuthKeys', 'oauth_consumer', 'id', 'OAuthTokens', 'oauth_tokens', 'consumer', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('17265514-6458-c6ca-e2d2-539198f4e301', 'oauthtokens_assigned_user', 'Users', 'users', 'id', 'OAuthTokens', 'oauth_tokens', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('17e1d515-3328-5ac5-b9f2-53919871b968', 'fp_events_modified_user', 'Users', 'users', 'id', 'FP_events', 'fp_events', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('18206d22-c9df-b753-6b1c-5391985bb9e8', 'fp_events_created_by', 'Users', 'users', 'id', 'FP_events', 'fp_events', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('185ee3b7-cd6e-669c-da10-5391986ca0cd', 'fp_events_assigned_user', 'Users', 'users', 'id', 'FP_events', 'fp_events', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('191a667d-8b7a-148d-6521-53919802915e', 'fp_event_locations_modified_user', 'Users', 'users', 'id', 'FP_Event_Locations', 'fp_event_locations', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1958efd6-0c64-c8d7-7635-53919869007c', 'fp_event_locations_created_by', 'Users', 'users', 'id', 'FP_Event_Locations', 'fp_event_locations', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('199762be-ff20-1b0a-be94-539198f4a884', 'fp_event_locations_assigned_user', 'Users', 'users', 'id', 'FP_Event_Locations', 'fp_event_locations', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('19d5e539-4255-80bf-d876-53919873f5da', 'optimistic_locking', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
('1a146550-36c0-9dc8-4849-53919830e5e2', 'unified_search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
('1acfe60a-5948-43f5-9946-539198d6b8c3', 'aop_case_events_modified_user', 'Users', 'users', 'id', 'AOP_Case_Events', 'aop_case_events', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1b0e68f4-3d67-4f73-f689-539198940c62', 'aop_case_events_created_by', 'Users', 'users', 'id', 'AOP_Case_Events', 'aop_case_events', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1b4ce9c9-5578-254d-1fd4-539198da24a6', 'aop_case_events_assigned_user', 'Users', 'users', 'id', 'AOP_Case_Events', 'aop_case_events', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1b8b6364-4b55-407b-db52-539198edaf3a', 'cases_aop_case_events', 'Cases', 'cases', 'id', 'AOP_Case_Events', 'aop_case_events', 'case_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1c0865ec-e9b3-6d71-ab8f-539198695374', 'aop_case_updates_modified_user', 'Users', 'users', 'id', 'AOP_Case_Updates', 'aop_case_updates', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1c46e206-9005-9a88-e979-5391987b2119', 'aop_case_updates_created_by', 'Users', 'users', 'id', 'AOP_Case_Updates', 'aop_case_updates', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1c857953-f6c9-c502-535d-5391980554d5', 'aop_case_updates_assigned_user', 'Users', 'users', 'id', 'AOP_Case_Updates', 'aop_case_updates', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1cc3f23d-2fe6-1e94-3f6e-539198dcda8e', 'cases_aop_case_updates', 'Cases', 'cases', 'id', 'AOP_Case_Updates', 'aop_case_updates', 'case_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1d027e81-bc08-a734-6fa1-5391980189f5', 'aop_case_updates_notes', 'AOP_Case_Updates', 'aop_case_updates', 'id', 'Notes', 'notes', 'parent_id', NULL, NULL, NULL, 'one-to-many', 'parent_type', 'AOP_Case_Updates', 0, 0),
('1dfc7ef8-05b3-c34f-a7d4-5391989fde45', 'aor_reports_modified_user', 'Users', 'users', 'id', 'AOR_Reports', 'aor_reports', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1e3af59e-13d3-f694-474b-539198e5a462', 'aor_reports_created_by', 'Users', 'users', 'id', 'AOR_Reports', 'aor_reports', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1e797453-bfca-cca7-1b50-539198e2709f', 'aor_reports_assigned_user', 'Users', 'users', 'id', 'AOR_Reports', 'aor_reports', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1eb7eadd-3c92-1d64-d4db-539198b5ace6', 'aor_reports_aor_fields', 'AOR_Reports', 'aor_reports', 'id', 'AOR_Fields', 'aor_field', 'aor_report_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1ef662bb-4998-bf24-a541-5391983f9e0d', 'aor_reports_aor_conditions', 'AOR_Reports', 'aor_reports', 'id', 'AOR_Conditions', 'aor_conditions', 'aor_report_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1fb1f522-9f13-9aba-93e5-5391987aa12d', 'aor_fields_modified_user', 'Users', 'users', 'id', 'AOR_Fields', 'aor_fields', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('1ff07ab8-14b9-7221-ab0c-5391987af6b6', 'aor_fields_created_by', 'Users', 'users', 'id', 'AOR_Fields', 'aor_fields', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('206d7063-6ef6-fe65-6492-539198ff58ec', 'aor_charts_modified_user', 'Users', 'users', 'id', 'AOR_Charts', 'aor_charts', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('20ac0fa1-13b8-7732-7836-539198fdf679', 'aor_charts_created_by', 'Users', 'users', 'id', 'AOR_Charts', 'aor_charts', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2129045f-357f-be0b-2ac1-539198f9b4f4', 'aor_conditions_modified_user', 'Users', 'users', 'id', 'AOR_Conditions', 'aor_conditions', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('21678e80-e2ab-c931-69df-5391985af2b0', 'aor_conditions_created_by', 'Users', 'users', 'id', 'AOR_Conditions', 'aor_conditions', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('22618ff3-4312-00de-eef1-539198dd6777', 'aos_contracts_modified_user', 'Users', 'users', 'id', 'AOS_Contracts', 'aos_contracts', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('22a00fda-2075-9365-4092-539198681d6a', 'aos_contracts_created_by', 'Users', 'users', 'id', 'AOS_Contracts', 'aos_contracts', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('22de875f-ddde-37cc-8bc5-5391989d1189', 'aos_contracts_assigned_user', 'Users', 'users', 'id', 'AOS_Contracts', 'aos_contracts', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('231d0921-0633-e8ba-d4f6-53919886fdb3', 'aos_contracts_aos_product_quotes', 'AOS_Contracts', 'aos_contracts', 'id', 'AOS_Products_Quotes', 'aos_products_quotes', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('24170af3-d232-08cb-d5e9-5391987c40ee', 'aos_invoices_modified_user', 'Users', 'users', 'id', 'AOS_Invoices', 'aos_invoices', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('24558f66-820b-1420-7f0b-5391982b4dff', 'aos_invoices_created_by', 'Users', 'users', 'id', 'AOS_Invoices', 'aos_invoices', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2494053b-a469-9c70-2d3f-539198a8faaf', 'aos_invoices_assigned_user', 'Users', 'users', 'id', 'AOS_Invoices', 'aos_invoices', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('24940e23-ec04-c3fa-f27c-539198f227a5', 'aos_invoices_aos_product_quotes', 'AOS_Invoices', 'aos_invoices', 'id', 'AOS_Products_Quotes', 'aos_products_quotes', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('254f9fb5-569e-f37e-7364-539198a4d527', 'aos_pdf_templates_modified_user', 'Users', 'users', 'id', 'AOS_PDF_Templates', 'aos_pdf_templates', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('258e156a-2c69-9ef4-025a-5391984ef367', 'aos_pdf_templates_created_by', 'Users', 'users', 'id', 'AOS_PDF_Templates', 'aos_pdf_templates', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('25cc90a0-0b0e-58b1-be5a-539198a3fe3a', 'aos_pdf_templates_assigned_user', 'Users', 'users', 'id', 'AOS_PDF_Templates', 'aos_pdf_templates', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('26881dea-fcf1-2f7a-51c4-539198b082e8', 'aos_product_categories_modified_user', 'Users', 'users', 'id', 'AOS_Product_Categories', 'aos_product_categories', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('26c69dff-6bd7-10d1-ebc7-539198b688ba', 'aos_product_categories_created_by', 'Users', 'users', 'id', 'AOS_Product_Categories', 'aos_product_categories', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('270517a5-434f-249f-1799-5391989cb9dc', 'aos_product_categories_assigned_user', 'Users', 'users', 'id', 'AOS_Product_Categories', 'aos_product_categories', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('27439a68-6f76-2e3c-1f57-53919818043d', 'sub_product_categories', 'AOS_Product_Categories', 'aos_product_categories', 'id', 'AOS_Product_Categories', 'aos_product_categories', 'parent_category_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('27ff129f-fe5b-8662-e92b-539198553396', 'aos_products_modified_user', 'Users', 'users', 'id', 'AOS_Products', 'aos_products', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('283d9abf-beff-17f1-13a5-5391985c4a8d', 'aos_products_created_by', 'Users', 'users', 'id', 'AOS_Products', 'aos_products', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('287c1a51-c950-3686-8b4b-539198597aa0', 'aos_products_assigned_user', 'Users', 'users', 'id', 'AOS_Products', 'aos_products', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('28ba97e0-b831-9e35-68e8-539198337bb3', 'product_categories', 'AOS_Product_Categories', 'aos_product_categories', 'id', 'AOS_Products', 'aos_products', 'aos_product_category_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('297622b6-deda-89f0-36fd-539198287f7f', 'aos_products_quotes_modified_user', 'Users', 'users', 'id', 'AOS_Products_Quotes', 'aos_products_quotes', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('29762977-4b37-5653-f61c-5391986d6239', 'aos_products_quotes_created_by', 'Users', 'users', 'id', 'AOS_Products_Quotes', 'aos_products_quotes', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('29b4a518-e621-f281-1269-539198ff43a1', 'aos_products_quotes_assigned_user', 'Users', 'users', 'id', 'AOS_Products_Quotes', 'aos_products_quotes', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('29f32edb-1693-7ec1-9e1b-53919875cd50', 'aos_product_quotes_aos_products', 'AOS_Products', 'aos_products', 'id', 'AOS_Products_Quotes', 'aos_products_quotes', 'product_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2aaea128-58cb-9426-3f20-53919869fc3d', 'aos_line_item_groups_modified_user', 'Users', 'users', 'id', 'AOS_Line_Item_Groups', 'aos_line_item_groups', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2aed2b90-be57-52f4-cecb-539198797ab1', 'aos_line_item_groups_created_by', 'Users', 'users', 'id', 'AOS_Line_Item_Groups', 'aos_line_item_groups', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2b2bab00-d38b-8e02-b7cf-539198557f42', 'aos_line_item_groups_assigned_user', 'Users', 'users', 'id', 'AOS_Line_Item_Groups', 'aos_line_item_groups', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2be720a9-95a8-5f5b-8351-5391982a0d79', 'aos_quotes_modified_user', 'Users', 'users', 'id', 'AOS_Quotes', 'aos_quotes', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2c25aede-1cb8-429e-3a9c-539198adc7a3', 'aos_quotes_created_by', 'Users', 'users', 'id', 'AOS_Quotes', 'aos_quotes', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2c6423b4-ef14-bff6-0bf7-539198f42fd0', 'aos_quotes_assigned_user', 'Users', 'users', 'id', 'AOS_Quotes', 'aos_quotes', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2ca2aed0-2c88-8060-449e-53919853666f', 'aos_quotes_aos_product_quotes', 'AOS_Quotes', 'aos_quotes', 'id', 'AOS_Products_Quotes', 'aos_products_quotes', 'parent_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2d1fa52f-2541-ca67-3eff-539198c1a267', 'aow_actions_modified_user', 'Users', 'users', 'id', 'AOW_Actions', 'aow_actions', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2d5e2b6c-5c6c-a472-c263-53919897c173', 'aow_actions_created_by', 'Users', 'users', 'id', 'AOW_Actions', 'aow_actions', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2e583e65-0c56-69e1-5e1e-5391982c20bc', 'aow_workflow_modified_user', 'Users', 'users', 'id', 'AOW_WorkFlow', 'aow_workflow', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2e96b5d7-3290-6279-c2c4-5391986db1cd', 'aow_workflow_created_by', 'Users', 'users', 'id', 'AOW_WorkFlow', 'aow_workflow', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2ed5321c-b3d6-3e36-b798-5391987f6a4f', 'aow_workflow_assigned_user', 'Users', 'users', 'id', 'AOW_WorkFlow', 'aow_workflow', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2f13b22b-7ad5-6104-5cc0-5391989d379c', 'aow_workflow_aow_conditions', 'AOW_WorkFlow', 'aow_workflow', 'id', 'AOW_Conditions', 'aow_conditions', 'aow_workflow_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2f523264-99b0-cbb7-e62f-539198d55d79', 'aow_workflow_aow_actions', 'AOW_WorkFlow', 'aow_workflow', 'id', 'AOW_Actions', 'aow_actions', 'aow_workflow_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('2f90b679-c26f-a99b-2080-5391986a1d65', 'aow_workflow_aow_processed', 'AOW_WorkFlow', 'aow_workflow', 'id', 'AOW_Processed', 'aow_processed', 'aow_workflow_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('300dbd9e-97f5-4f8e-5150-539198b44e46', 'aow_processed_modified_user', 'Users', 'users', 'id', 'AOW_Processed', 'aow_processed', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('304c3575-28b8-4992-0eb8-53919857381e', 'aow_processed_created_by', 'Users', 'users', 'id', 'AOW_Processed', 'aow_processed', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('30c9349d-0c29-6854-597e-5391985b0b89', 'aow_conditions_modified_user', 'Users', 'users', 'id', 'AOW_Conditions', 'aow_conditions', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3107bea9-aeaf-f806-e904-539198a216e0', 'aow_conditions_created_by', 'Users', 'users', 'id', 'AOW_Conditions', 'aow_conditions', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('31c342f2-f316-a0c4-58c5-539198c03402', 'prov1_c_azienda_modified_user', 'Users', 'users', 'id', 'prov1_C_Azienda', 'prov1_c_azienda', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3201ccdc-d83d-44bc-c897-5391987868b0', 'prov1_c_azienda_created_by', 'Users', 'users', 'id', 'prov1_C_Azienda', 'prov1_c_azienda', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('32404dd7-615c-3347-2c23-539198869f03', 'prov1_c_azienda_assigned_user', 'Users', 'users', 'id', 'prov1_C_Azienda', 'prov1_c_azienda', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('32fbce4a-d24a-910f-ac03-5391987b5fc4', 'prov1_c_dipendenti_modified_user', 'Users', 'users', 'id', 'prov1_C_Dipendenti', 'prov1_c_dipendenti', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('333a4942-2fac-63f9-7d81-5391987c4038', 'prov1_c_dipendenti_created_by', 'Users', 'users', 'id', 'prov1_C_Dipendenti', 'prov1_c_dipendenti', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3378c6a7-1bc7-a077-bb5c-539198d89660', 'prov1_c_dipendenti_assigned_user', 'Users', 'users', 'id', 'prov1_C_Dipendenti', 'prov1_c_dipendenti', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('352e4b60-b22e-d043-93fb-5391984e78bd', 'prov2_f_cliente_modified_user', 'Users', 'users', 'id', 'prov2_F_Cliente', 'prov2_f_cliente', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('356cce82-0143-726a-3de4-539198c4e41f', 'prov2_f_cliente_created_by', 'Users', 'users', 'id', 'prov2_F_Cliente', 'prov2_f_cliente', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('35ab4ae3-51a1-cf5f-9101-539198af6898', 'prov2_f_cliente_assigned_user', 'Users', 'users', 'id', 'prov2_F_Cliente', 'prov2_f_cliente', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('37225365-f113-ccdd-5682-5391981b1a92', 'prov2_f_contratti_modified_user', 'Users', 'users', 'id', 'prov2_F_Contratti', 'prov2_f_contratti', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3760dcab-e4e2-1b58-dfc4-5391981b084f', 'prov2_f_contratti_created_by', 'Users', 'users', 'id', 'prov2_F_Contratti', 'prov2_f_contratti', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0);
INSERT INTO `relationships` (`id`, `relationship_name`, `lhs_module`, `lhs_table`, `lhs_key`, `rhs_module`, `rhs_table`, `rhs_key`, `join_table`, `join_key_lhs`, `join_key_rhs`, `relationship_type`, `relationship_role_column`, `relationship_role_column_value`, `reverse`, `deleted`) VALUES
('379f5d64-a158-06cd-63d3-53919874296d', 'prov2_f_contratti_assigned_user', 'Users', 'users', 'id', 'prov2_F_Contratti', 'prov2_f_contratti', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('39165799-ac7c-235f-f6e8-5391989d0e99', 'prov2_f_potenziale_cliente_modified_user', 'Users', 'users', 'id', 'prov2_F_Potenziale_Cliente', 'prov2_f_potenziale_cliente', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3954d45d-828f-6b2d-c27c-539198293d49', 'prov2_f_potenziale_cliente_created_by', 'Users', 'users', 'id', 'prov2_F_Potenziale_Cliente', 'prov2_f_potenziale_cliente', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('39935e61-4fff-122d-98ed-5391983d6126', 'prov2_f_potenziale_cliente_assigned_user', 'Users', 'users', 'id', 'prov2_F_Potenziale_Cliente', 'prov2_f_potenziale_cliente', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3b48e1b9-b331-a6ba-d8e9-539198e6b73d', 'prov2_f_upload_modified_user', 'Users', 'users', 'id', 'prov2_F_Upload', 'prov2_f_upload', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3b876aab-b67d-4d55-31dd-53919877ab14', 'prov2_f_upload_created_by', 'Users', 'users', 'id', 'prov2_F_Upload', 'prov2_f_upload', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3bc5e049-b8e1-7a84-acb5-539198404148', 'prov2_f_upload_assigned_user', 'Users', 'users', 'id', 'prov2_F_Upload', 'prov2_f_upload', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3c816b08-d033-da87-0fee-5391986c01c0', 'prov1_aziende_modified_user', 'Users', 'users', 'id', 'prov1_Aziende', 'prov1_aziende', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3cbfe352-70f4-557c-996a-53919869eb8d', 'prov1_aziende_created_by', 'Users', 'users', 'id', 'prov1_Aziende', 'prov1_aziende', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3cfe6507-e318-7833-5a0a-539198438fef', 'prov1_aziende_assigned_user', 'Users', 'users', 'id', 'prov1_Aziende', 'prov1_aziende', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3d3cea5c-76d6-6064-309e-539198722cef', 'prov1_aziende_email_addresses', 'prov1_Aziende', 'prov1_aziende', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'prov1_Aziende', 0, 0),
('3d7b6afa-2d53-eacf-3523-539198661de3', 'prov1_aziende_email_addresses_primary', 'prov1_Aziende', 'prov1_aziende', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('3e7564ef-f23c-77fd-5bc2-539198a9aa59', 'prov1_dipendenti_modified_user', 'Users', 'users', 'id', 'prov1_Dipendenti', 'prov1_dipendenti', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3eb3ec14-1b91-9963-23f6-539198067dd3', 'prov1_dipendenti_created_by', 'Users', 'users', 'id', 'prov1_Dipendenti', 'prov1_dipendenti', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3ef2614b-52f5-dd94-0bb8-539198da1368', 'prov1_dipendenti_assigned_user', 'Users', 'users', 'id', 'prov1_Dipendenti', 'prov1_dipendenti', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('3f6f6950-b4a7-35ce-6a53-539198a65259', 'prov1_dipendenti_email_addresses', 'prov1_Dipendenti', 'prov1_dipendenti', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'bean_module', 'prov1_Dipendenti', 0, 0),
('3fade204-647c-b3e3-664f-539198c81061', 'prov1_dipendenti_email_addresses_primary', 'prov1_Dipendenti', 'prov1_dipendenti', 'id', 'EmailAddresses', 'email_addresses', 'id', 'email_addr_bean_rel', 'bean_id', 'email_address_id', 'many-to-many', 'primary_address', '1', 0, 0),
('40a7fd0c-b1e1-aff8-5e53-539198f796b4', 'jjwg_maps_modified_user', 'Users', 'users', 'id', 'jjwg_Maps', 'jjwg_maps', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('40e67543-320f-b883-8919-539198d38087', 'jjwg_maps_created_by', 'Users', 'users', 'id', 'jjwg_Maps', 'jjwg_maps', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('4124fcb6-7d74-8c94-eaae-539198df6340', 'jjwg_maps_assigned_user', 'Users', 'users', 'id', 'jjwg_Maps', 'jjwg_maps', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('421ef8a5-baf7-0b32-fc08-539198d169d6', 'jjwg_markers_modified_user', 'Users', 'users', 'id', 'jjwg_Markers', 'jjwg_markers', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('425d74dc-863a-50e6-2495-539198633642', 'jjwg_markers_created_by', 'Users', 'users', 'id', 'jjwg_Markers', 'jjwg_markers', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('429bf9df-7281-9b3f-1a33-5391986252b4', 'jjwg_markers_assigned_user', 'Users', 'users', 'id', 'jjwg_Markers', 'jjwg_markers', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('43d480ad-5e41-c9a1-0f69-539198570465', 'jjwg_areas_modified_user', 'Users', 'users', 'id', 'jjwg_Areas', 'jjwg_areas', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('4413005f-282a-a53c-8b2e-53919891b487', 'jjwg_areas_created_by', 'Users', 'users', 'id', 'jjwg_Areas', 'jjwg_areas', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('4413039a-9ba3-7c5e-e340-539198687a72', 'jjwg_areas_assigned_user', 'Users', 'users', 'id', 'jjwg_Areas', 'jjwg_areas', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('454b8450-35d5-883a-0f13-539198c3df59', 'jjwg_address_cache_modified_user', 'Users', 'users', 'id', 'jjwg_Address_Cache', 'jjwg_address_cache', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('458a01b3-b54e-cff4-5c67-5391988484cc', 'jjwg_address_cache_created_by', 'Users', 'users', 'id', 'jjwg_Address_Cache', 'jjwg_address_cache', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('45c88dd7-45e2-f462-347b-539198e8fe85', 'jjwg_address_cache_assigned_user', 'Users', 'users', 'id', 'jjwg_Address_Cache', 'jjwg_address_cache', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('46840b61-d3ca-5220-ce9b-5391983bbba7', 'calls_reschedule_modified_user', 'Users', 'users', 'id', 'Calls_Reschedule', 'calls_reschedule', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('46c2898c-9905-6c5f-4ab2-53919872abca', 'calls_reschedule_created_by', 'Users', 'users', 'id', 'Calls_Reschedule', 'calls_reschedule', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('47011cc2-76e7-85a0-4570-5391989fd7fd', 'calls_reschedule_assigned_user', 'Users', 'users', 'id', 'Calls_Reschedule', 'calls_reschedule', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('477e144b-4d66-39de-3585-5391989938ac', 'securitygroups_modified_user', 'Users', 'users', 'id', 'SecurityGroups', 'securitygroups', 'modified_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('47bc9602-1105-0af3-f96d-5391984a16a2', 'securitygroups_created_by', 'Users', 'users', 'id', 'SecurityGroups', 'securitygroups', 'created_by', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('47fb1642-7dd4-1635-ba71-539198ae9b04', 'securitygroups_assigned_user', 'Users', 'users', 'id', 'SecurityGroups', 'securitygroups', 'assigned_user_id', NULL, NULL, NULL, 'one-to-many', NULL, NULL, 0, 0),
('77d5cb7b-5f21-0cc3-0135-53919890aa8c', 'accounts_bugs', 'Accounts', 'accounts', 'id', 'Bugs', 'bugs', 'id', 'accounts_bugs', 'account_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('78144a8f-0a7a-41ab-43dd-539198fc8512', 'accounts_contacts', 'Accounts', 'accounts', 'id', 'Contacts', 'contacts', 'id', 'accounts_contacts', 'account_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('7852ce68-d49b-5cc8-b48c-5391981ff35a', 'accounts_opportunities', 'Accounts', 'accounts', 'id', 'Opportunities', 'opportunities', 'id', 'accounts_opportunities', 'account_id', 'opportunity_id', 'many-to-many', NULL, NULL, 0, 0),
('78cfcc21-afee-5223-39ab-539198cf35cd', 'calls_contacts', 'Calls', 'calls', 'id', 'Contacts', 'contacts', 'id', 'calls_contacts', 'call_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('790e4ef7-2e09-474d-cb01-539198fbfb9a', 'calls_users', 'Calls', 'calls', 'id', 'Users', 'users', 'id', 'calls_users', 'call_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('794cc5ae-2ded-c639-1ef7-539198b73914', 'calls_leads', 'Calls', 'calls', 'id', 'Leads', 'leads', 'id', 'calls_leads', 'call_id', 'lead_id', 'many-to-many', NULL, NULL, 0, 0),
('798b4800-61a0-a366-7899-5391982ab285', 'cases_bugs', 'Cases', 'cases', 'id', 'Bugs', 'bugs', 'id', 'cases_bugs', 'case_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('7a084522-c5d7-fa0d-7353-539198c4aa38', 'contacts_bugs', 'Contacts', 'contacts', 'id', 'Bugs', 'bugs', 'id', 'contacts_bugs', 'contact_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('7a46d23d-cad6-f8ff-bbe4-5391985a2e9c', 'contacts_cases', 'Contacts', 'contacts', 'id', 'Cases', 'cases', 'id', 'contacts_cases', 'contact_id', 'case_id', 'many-to-many', NULL, NULL, 0, 0),
('7a855e28-6877-c3eb-cbe2-539198380710', 'contacts_users', 'Contacts', 'contacts', 'id', 'Users', 'users', 'id', 'contacts_users', 'contact_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('7b025fbd-0d48-452e-72eb-5391988a0a0a', 'emails_bugs_rel', 'Emails', 'emails', 'id', 'Bugs', 'bugs', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Bugs', 0, 0),
('7b40dce9-fb94-5927-1a87-53919873a2d8', 'emails_cases_rel', 'Emails', 'emails', 'id', 'Cases', 'cases', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Cases', 0, 0),
('7bbdda9a-09ff-30d6-0b30-53919844f279', 'emails_opportunities_rel', 'Emails', 'emails', 'id', 'Opportunities', 'opportunities', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Opportunities', 0, 0),
('7bfc5384-f58f-f6cb-f433-53919801aa80', 'emails_tasks_rel', 'Emails', 'emails', 'id', 'Tasks', 'tasks', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Tasks', 0, 0),
('7c3ad4f8-e072-2759-ab59-539198af353f', 'emails_users_rel', 'Emails', 'emails', 'id', 'Users', 'users', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Users', 0, 0),
('7c795c74-5868-3b5a-3db1-5391980292d1', 'emails_project_task_rel', 'Emails', 'emails', 'id', 'ProjectTask', 'project_task', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'ProjectTask', 0, 0),
('7cb7d0ca-f85a-6591-1525-5391982ef3f4', 'emails_projects_rel', 'Emails', 'emails', 'id', 'Project', 'project', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Project', 0, 0),
('7cf651c1-f305-901d-2ea8-53919828efb5', 'emails_prospects_rel', 'Emails', 'emails', 'id', 'Prospects', 'prospects', 'id', 'emails_beans', 'email_id', 'bean_id', 'many-to-many', 'bean_module', 'Prospects', 0, 0),
('7d34de90-4697-a2dc-3698-5391982ff018', 'meetings_contacts', 'Meetings', 'meetings', 'id', 'Contacts', 'contacts', 'id', 'meetings_contacts', 'meeting_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('7d73529e-377e-9c8a-c9f2-539198fc572e', 'meetings_users', 'Meetings', 'meetings', 'id', 'Users', 'users', 'id', 'meetings_users', 'meeting_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('7db1d8b8-6e70-4680-c7aa-539198ce2943', 'meetings_leads', 'Meetings', 'meetings', 'id', 'Leads', 'leads', 'id', 'meetings_leads', 'meeting_id', 'lead_id', 'many-to-many', NULL, NULL, 0, 0),
('7e2ed7ee-5604-ff19-ad89-539198d64b2d', 'opportunities_contacts', 'Opportunities', 'opportunities', 'id', 'Contacts', 'contacts', 'id', 'opportunities_contacts', 'opportunity_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('7e6d592a-cafc-1637-f3fd-539198bdaa70', 'prospect_list_campaigns', 'ProspectLists', 'prospect_lists', 'id', 'Campaigns', 'campaigns', 'id', 'prospect_list_campaigns', 'prospect_list_id', 'campaign_id', 'many-to-many', NULL, NULL, 0, 0),
('7eabe4d6-d352-0b58-9225-539198ff34df', 'prospect_list_contacts', 'ProspectLists', 'prospect_lists', 'id', 'Contacts', 'contacts', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Contacts', 0, 0),
('7eea6af7-4d7c-cf82-ba1c-5391980a443b', 'prospect_list_prospects', 'ProspectLists', 'prospect_lists', 'id', 'Prospects', 'prospects', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Prospects', 0, 0),
('7f28e1fc-f78f-f50b-9db3-53919823d5b2', 'prospect_list_leads', 'ProspectLists', 'prospect_lists', 'id', 'Leads', 'leads', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Leads', 0, 0),
('7f67634b-d986-58d8-0858-53919819ab26', 'prospect_list_users', 'ProspectLists', 'prospect_lists', 'id', 'Users', 'users', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Users', 0, 0),
('7fa5e82c-4cba-278d-0944-539198ef9c7a', 'prospect_list_accounts', 'ProspectLists', 'prospect_lists', 'id', 'Accounts', 'accounts', 'id', 'prospect_lists_prospects', 'prospect_list_id', 'related_id', 'many-to-many', 'related_type', 'Accounts', 0, 0),
('7fe464c9-273f-e58b-8f0f-53919865fb87', 'roles_users', 'Roles', 'roles', 'id', 'Users', 'users', 'id', 'roles_users', 'role_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('806168a2-0d15-b974-011f-5391986b565e', 'projects_bugs', 'Project', 'project', 'id', 'Bugs', 'bugs', 'id', 'projects_bugs', 'project_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('809fe8e3-3c7d-7bdf-3b98-539198153992', 'projects_cases', 'Project', 'project', 'id', 'Cases', 'cases', 'id', 'projects_cases', 'project_id', 'case_id', 'many-to-many', NULL, NULL, 0, 0),
('80de6013-c7bd-589c-a263-539198db79f7', 'projects_accounts', 'Project', 'project', 'id', 'Accounts', 'accounts', 'id', 'projects_accounts', 'project_id', 'account_id', 'many-to-many', NULL, NULL, 0, 0),
('815b6af6-ad88-7da4-cd3f-539198463586', 'projects_contacts', 'Project', 'project', 'id', 'Contacts', 'contacts', 'id', 'projects_contacts', 'project_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('8199ec72-0ac3-f5c6-297d-5391984c9269', 'projects_opportunities', 'Project', 'project', 'id', 'Opportunities', 'opportunities', 'id', 'projects_opportunities', 'project_id', 'opportunity_id', 'many-to-many', NULL, NULL, 0, 0),
('81d86ee2-025c-1a91-565a-53919893ce4b', 'acl_roles_actions', 'ACLRoles', 'acl_roles', 'id', 'ACLActions', 'acl_actions', 'id', 'acl_roles_actions', 'role_id', 'action_id', 'many-to-many', NULL, NULL, 0, 0),
('8216eef2-fe8e-8fb7-3086-539198b72831', 'acl_roles_users', 'ACLRoles', 'acl_roles', 'id', 'Users', 'users', 'id', 'acl_roles_users', 'role_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0),
('825563c4-a51c-6ae7-449a-5391986e85dd', 'email_marketing_prospect_lists', 'EmailMarketing', 'email_marketing', 'id', 'ProspectLists', 'prospect_lists', 'id', 'email_marketing_prospect_lists', 'email_marketing_id', 'prospect_list_id', 'many-to-many', NULL, NULL, 0, 0),
('82d27482-524a-0114-75d0-539198de8c7e', 'leads_documents', 'Leads', 'leads', 'id', 'Documents', 'documents', 'id', 'linked_documents', 'parent_id', 'document_id', 'many-to-many', 'parent_type', 'Leads', 0, 0),
('8310f33f-8291-988b-9527-539198514a13', 'documents_accounts', 'Documents', 'documents', 'id', 'Accounts', 'accounts', 'id', 'documents_accounts', 'document_id', 'account_id', 'many-to-many', NULL, NULL, 0, 0),
('834f7eda-22f6-b406-ffbb-539198e948f4', 'documents_contacts', 'Documents', 'documents', 'id', 'Contacts', 'contacts', 'id', 'documents_contacts', 'document_id', 'contact_id', 'many-to-many', NULL, NULL, 0, 0),
('83cc720b-c9a0-1db7-fdd0-53919819626e', 'documents_opportunities', 'Documents', 'documents', 'id', 'Opportunities', 'opportunities', 'id', 'documents_opportunities', 'document_id', 'opportunity_id', 'many-to-many', NULL, NULL, 0, 0),
('840afd63-c831-19f2-15fc-539198b10cb6', 'documents_cases', 'Documents', 'documents', 'id', 'Cases', 'cases', 'id', 'documents_cases', 'document_id', 'case_id', 'many-to-many', NULL, NULL, 0, 0),
('844979a6-9cdd-ac10-c2cd-539198356ff5', 'documents_bugs', 'Documents', 'documents', 'id', 'Bugs', 'bugs', 'id', 'documents_bugs', 'document_id', 'bug_id', 'many-to-many', NULL, NULL, 0, 0),
('8487f365-ec11-18b9-c2a4-5391981e0156', 'aos_contracts_documents', 'AOS_Contracts', 'aos_contracts', 'id', 'Documents', 'documents', 'id', 'aos_contracts_documents', 'aos_contracts_id', 'documents_id', 'many-to-many', NULL, NULL, 0, 0),
('84c67295-9157-7918-c623-539198ce25a4', 'aos_quotes_aos_contracts', 'AOS_Quotes', 'aos_quotes', 'id', 'AOS_Contracts', 'aos_contracts', 'id', 'aos_quotes_os_contracts_c', 'aos_quotese81e_quotes_ida', 'aos_quotes4dc0ntracts_idb', 'many-to-many', NULL, NULL, 0, 0),
('85437758-7740-1837-344a-53919849d804', 'aos_quotes_aos_invoices', 'AOS_Quotes', 'aos_quotes', 'id', 'AOS_Invoices', 'aos_invoices', 'id', 'aos_quotes_aos_invoices_c', 'aos_quotes77d9_quotes_ida', 'aos_quotes6b83nvoices_idb', 'many-to-many', NULL, NULL, 0, 0),
('8581f848-b64d-bff3-5b74-539198f3a02a', 'aos_quotes_project', 'AOS_Quotes', 'aos_quotes', 'id', 'Project', 'project', 'id', 'aos_quotes_project_c', 'aos_quotes1112_quotes_ida', 'aos_quotes7207project_idb', 'many-to-many', NULL, NULL, 0, 0),
('85c078a1-cd67-0817-d037-539198c20293', 'aow_processed_aow_actions', 'AOW_Processed', 'aow_processed', 'id', 'AOW_Actions', 'aow_actions', 'id', 'aow_processed_aow_actions', 'aow_processed_id', 'aow_action_id', 'many-to-many', NULL, NULL, 0, 0),
('85fefc8f-8b18-1e6f-db73-539198c3230d', 'fp_events_contacts', 'FP_events', 'fp_events', 'id', 'Contacts', 'contacts', 'id', 'fp_events_contacts_c', 'fp_events_contactsfp_events_ida', 'fp_events_contactscontacts_idb', 'many-to-many', NULL, NULL, 0, 0),
('863d73ce-3a4a-95d7-4f13-5391982ed867', 'fp_events_fp_event_locations_1', 'FP_events', 'fp_events', 'id', 'FP_Event_Locations', 'fp_event_locations', 'id', 'fp_events_fp_event_locations_1_c', 'fp_events_fp_event_locations_1fp_events_ida', 'fp_events_fp_event_locations_1fp_event_locations_idb', 'many-to-many', NULL, NULL, 0, 0),
('86ba7271-ddda-ab18-5e85-539198767824', 'fp_events_leads_1', 'FP_events', 'fp_events', 'id', 'Leads', 'leads', 'id', 'fp_events_leads_1_c', 'fp_events_leads_1fp_events_ida', 'fp_events_leads_1leads_idb', 'many-to-many', NULL, NULL, 0, 0),
('86f8f244-04d6-5971-0eb5-539198210541', 'fp_events_prospects_1', 'FP_events', 'fp_events', 'id', 'Prospects', 'prospects', 'id', 'fp_events_prospects_1_c', 'fp_events_prospects_1fp_events_ida', 'fp_events_prospects_1prospects_idb', 'many-to-many', NULL, NULL, 0, 0),
('873783a6-0989-ce0c-4a3d-5391982ec08c', 'fp_event_locations_fp_events_1', 'FP_Event_Locations', 'fp_event_locations', 'id', 'FP_events', 'fp_events', 'id', 'fp_event_locations_fp_events_1_c', 'fp_event_locations_fp_events_1fp_event_locations_ida', 'fp_event_locations_fp_events_1fp_events_idb', 'many-to-many', NULL, NULL, 0, 0),
('87760cc7-c94c-8bf2-beb2-539198b5f930', 'jjwg_maps_jjwg_areas', 'jjwg_Maps', 'jjwg_maps', 'id', 'jjwg_Areas', 'jjwg_areas', 'id', 'jjwg_maps_jjwg_areas_c', 'jjwg_maps_5304wg_maps_ida', 'jjwg_maps_41f2g_areas_idb', 'many-to-many', NULL, NULL, 0, 0),
('87b48f87-e712-8041-e103-5391982d0b55', 'jjwg_maps_jjwg_markers', 'jjwg_Maps', 'jjwg_maps', 'id', 'jjwg_Markers', 'jjwg_markers', 'id', 'jjwg_maps_jjwg_markers_c', 'jjwg_maps_b229wg_maps_ida', 'jjwg_maps_2e31markers_idb', 'many-to-many', NULL, NULL, 0, 0),
('88318c51-2139-1b6e-4531-5391986ba8d4', 'prov1_aziende_prov1_dipendenti', 'prov1_Aziende', 'prov1_aziende', 'id', 'prov1_Dipendenti', 'prov1_dipendenti', 'id', 'prov1_aziende_prov1_dipendenti_c', 'prov1_aziende_prov1_dipendentiprov1_aziende_ida', 'prov1_aziende_prov1_dipendentiprov1_dipendenti_idb', 'many-to-many', NULL, NULL, 0, 0),
('887007c3-f61f-f452-0167-539198898a4d', 'prov1_c_azienda_prov1_c_dipendenti', 'prov1_C_Azienda', 'prov1_c_azienda', 'id', 'prov1_C_Dipendenti', 'prov1_c_dipendenti', 'id', 'prov1_c_azienda_prov1_c_dipendenti_c', 'prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida', 'prov1_c_azienda_prov1_c_dipendentiprov1_c_dipendenti_idb', 'many-to-many', NULL, NULL, 0, 0),
('88ae859b-a603-e20a-2958-539198592688', 'prov2_f_cliente_prov2_f_contratti', 'prov2_F_Cliente', 'prov2_f_cliente', 'id', 'prov2_F_Contratti', 'prov2_f_contratti', 'id', 'prov2_f_cliente_prov2_f_contratti_c', 'prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida', 'prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb', 'many-to-many', NULL, NULL, 0, 0),
('88ed0c8a-ce54-7f62-b1e8-539198a0136d', 'prov2_f_contratti_prov2_f_cliente', 'prov2_F_Contratti', 'prov2_f_contratti', 'id', 'prov2_F_Cliente', 'prov2_f_cliente', 'id', 'prov2_f_contratti_prov2_f_cliente_c', 'prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida', 'prov2_f_contratti_prov2_f_clienteprov2_f_cliente_idb', 'many-to-many', NULL, NULL, 0, 0),
('892b8459-0288-5b10-b31a-539198e9a811', 'prov2_f_upload_prov2_f_cliente', 'prov2_F_Upload', 'prov2_f_upload', 'id', 'prov2_F_Cliente', 'prov2_f_cliente', 'id', 'prov2_f_upload_prov2_f_cliente_c', 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida', 'prov2_f_upload_prov2_f_clienteprov2_f_cliente_idb', 'many-to-many', NULL, NULL, 0, 0),
('896a05f3-f291-bb33-7995-5391985a3149', 'securitygroups_acl_roles', 'SecurityGroups', 'securitygroups', 'id', 'ACLRoles', 'acl_roles', 'id', 'securitygroups_acl_roles', 'securitygroup_id', 'role_id', 'many-to-many', NULL, NULL, 0, 0),
('89a882ed-24c3-b42c-a52d-5391984a9ba9', 'securitygroups_accounts', 'SecurityGroups', 'securitygroups', 'id', 'Accounts', 'accounts', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Accounts', 0, 0),
('89e70fa2-935c-2015-a616-539198067596', 'securitygroups_bugs', 'SecurityGroups', 'securitygroups', 'id', 'Bugs', 'bugs', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Bugs', 0, 0),
('8a2586c5-3d90-4c73-3aed-539198703341', 'securitygroups_calls', 'SecurityGroups', 'securitygroups', 'id', 'Calls', 'calls', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Calls', 0, 0),
('8a6401f1-a108-a077-fafd-53919898e8b6', 'securitygroups_campaigns', 'SecurityGroups', 'securitygroups', 'id', 'Campaigns', 'campaigns', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Campaigns', 0, 0),
('8aa28031-e9d8-a3c4-bf28-5391988c6083', 'securitygroups_cases', 'SecurityGroups', 'securitygroups', 'id', 'Cases', 'cases', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Cases', 0, 0),
('8ae1034c-65f5-5206-cdb0-539198a6d2d0', 'securitygroups_contacts', 'SecurityGroups', 'securitygroups', 'id', 'Contacts', 'contacts', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Contacts', 0, 0),
('8b1f8e6d-8380-8a29-9a3a-5391986e43c8', 'securitygroups_documents', 'SecurityGroups', 'securitygroups', 'id', 'Documents', 'documents', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Documents', 0, 0),
('8b9c9b82-d1bb-4d15-1ba1-5391982e89a9', 'securitygroups_emails', 'SecurityGroups', 'securitygroups', 'id', 'Emails', 'emails', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Emails', 0, 0),
('8bdb159c-3150-2dbd-4944-5391984cc1c0', 'securitygroups_emailtemplates', 'SecurityGroups', 'securitygroups', 'id', 'EmailTemplates', 'email_templates', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'EmailTemplates', 0, 0),
('8c199084-a212-f321-3f40-5391988c0ab4', 'securitygroups_leads', 'SecurityGroups', 'securitygroups', 'id', 'Leads', 'leads', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Leads', 0, 0),
('8c581b6f-730b-18d1-0d1d-539198fcd772', 'securitygroups_meetings', 'SecurityGroups', 'securitygroups', 'id', 'Meetings', 'meetings', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Meetings', 0, 0),
('8c969bb2-a20d-260e-ea38-5391988c0584', 'securitygroups_notes', 'SecurityGroups', 'securitygroups', 'id', 'Notes', 'notes', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Notes', 0, 0),
('8cd51d79-0447-8b7f-3f73-5391980acc5f', 'securitygroups_opportunities', 'SecurityGroups', 'securitygroups', 'id', 'Opportunities', 'opportunities', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Opportunities', 0, 0),
('8d139d61-7626-9e40-53ce-5391989f61ee', 'securitygroups_project', 'SecurityGroups', 'securitygroups', 'id', 'Project', 'project', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Project', 0, 0),
('8d521e23-26c0-93bc-1ead-539198c01d72', 'securitygroups_project_task', 'SecurityGroups', 'securitygroups', 'id', 'ProjectTask', 'project_task', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'ProjectTask', 0, 0),
('8d909bdf-3d49-595f-ad5c-539198e1a5a3', 'securitygroups_prospect_lists', 'SecurityGroups', 'securitygroups', 'id', 'ProspectLists', 'prospect_lists', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'ProspectLists', 0, 0),
('8dcf1390-386b-f5ca-64c6-539198930004', 'securitygroups_prospects', 'SecurityGroups', 'securitygroups', 'id', 'Prospects', 'prospects', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Prospects', 0, 0),
('8e0d9da4-9810-caeb-6e32-539198b63c0a', 'securitygroups_tasks', 'SecurityGroups', 'securitygroups', 'id', 'Tasks', 'tasks', 'id', 'securitygroups_records', 'securitygroup_id', 'record_id', 'many-to-many', 'module', 'Tasks', 0, 0),
('8e4c1b3a-8e4b-286c-b3b0-5391987fe7bf', 'securitygroups_users', 'SecurityGroups', 'securitygroups', 'id', 'Users', 'users', 'id', 'securitygroups_users', 'securitygroup_id', 'user_id', 'many-to-many', NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `releases`
--

CREATE TABLE IF NOT EXISTS `releases` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_order` int(4) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_releases` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `modules` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_role_id_del` (`id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `roles_modules`
--

CREATE TABLE IF NOT EXISTS `roles_modules` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `module_id` varchar(36) DEFAULT NULL,
  `allow` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_module_id` (`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `roles_users`
--

CREATE TABLE IF NOT EXISTS `roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ru_role_id` (`role_id`),
  KEY `idx_ru_user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `saved_search`
--

CREATE TABLE IF NOT EXISTS `saved_search` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `search_module` varchar(150) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` text,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `idx_desc` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `schedulers`
--

CREATE TABLE IF NOT EXISTS `schedulers` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `date_time_start` datetime DEFAULT NULL,
  `date_time_end` datetime DEFAULT NULL,
  `job_interval` varchar(100) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `catch_up` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_schedule` (`date_time_start`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `schedulers`
--

INSERT INTO `schedulers` (`id`, `deleted`, `date_entered`, `date_modified`, `created_by`, `modified_user_id`, `name`, `job`, `date_time_start`, `date_time_end`, `job_interval`, `time_from`, `time_to`, `last_run`, `status`, `catch_up`) VALUES
('ef32dea8-2f0b-f914-cf11-5388e3b46226', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Prune Tracker Tables', 'function::trimTracker', '2005-01-01 06:15:01', '2020-12-31 23:59:59', '0::2::1::*::*', NULL, NULL, NULL, 'Active', 1),
('f0e867f5-0993-4b62-25b2-5388e3b7117d', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Check Inbound Mailboxes', 'function::pollMonitoredInboxes', '2005-01-01 18:30:01', '2020-12-31 23:59:59', '*::*::*::*::*', NULL, NULL, NULL, 'Inactive', 0),
('f220efc8-ea1f-548d-0cf2-5388e33648d6', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Run Nightly Process Bounced Campaign Emails', 'function::pollMonitoredInboxesForBouncedCampaignEmails', '2005-01-01 17:00:01', '2020-12-31 23:59:59', '0::2-6::*::*::*', NULL, NULL, NULL, 'Active', 1),
('f35961a1-810c-8754-8f91-5388e3191c0c', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Run Nightly Mass Email Campaigns', 'function::runMassEmailCampaign', '2005-01-01 10:15:01', '2020-12-31 23:59:59', '0::2-6::*::*::*', NULL, NULL, NULL, 'Active', 1),
('6de000b0-c3c5-2211-5dfc-5388e33a7605', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Prune Database on 1st of Month', 'function::pruneDatabase', '2005-01-01 16:30:01', '2020-12-31 23:59:59', '0::4::1::*::*', NULL, NULL, NULL, 'Inactive', 0),
('1a670e04-6988-45f8-29c8-5388e38a1091', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Run Email Reminder Notifications', 'function::sendEmailReminders', '2008-01-01 11:00:01', '2020-12-31 23:59:59', '*::*::*::*::*', NULL, NULL, NULL, 'Active', 0),
('2def0a0b-2238-b148-9a15-5388e336b118', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Clean Jobs Queue', 'function::cleanJobQueue', '2012-01-01 07:30:01', '2030-12-31 23:59:59', '0::5::*::*::*', NULL, NULL, NULL, 'Active', 0),
('41770287-cca6-b6d7-ad89-5388e368432b', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Removal of documents from filesystem', 'function::removeDocumentsFromFS', '2012-01-01 08:00:01', '2030-12-31 23:59:59', '0::3::1::*::*', NULL, NULL, NULL, 'Active', 0),
('55000846-df89-e774-2738-5388e32bcf22', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Prune SugarFeed Tables', 'function::trimSugarFeeds', '2005-01-01 13:45:01', '2020-12-31 23:59:59', '0::2::1::*::*', NULL, NULL, NULL, 'Active', 1),
('53712dd4-032d-5936-c781-5388e3523666', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Run AOW WorkFlow', 'function::processAOW_Workflow', '2005-01-01 06:45:01', '2020-12-31 23:59:59', '*::*::*::*::*', NULL, NULL, NULL, 'Active', 0),
('7d6fc39a-515e-8216-9ef3-5388e3a360f2', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'AOP Check Inbound Mailboxes', 'function::pollMonitoredInboxesCustomAOP', '2005-01-01 11:15:00', '2020-12-31 00:00:00', '*/1::*::*::*::*', NULL, NULL, NULL, 'Active', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `securitygroups`
--

CREATE TABLE IF NOT EXISTS `securitygroups` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `noninheritable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `securitygroups_acl_roles`
--

CREATE TABLE IF NOT EXISTS `securitygroups_acl_roles` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `role_id` char(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `securitygroups_audit`
--

CREATE TABLE IF NOT EXISTS `securitygroups_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_securitygroups_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `securitygroups_default`
--

CREATE TABLE IF NOT EXISTS `securitygroups_default` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `securitygroups_records`
--

CREATE TABLE IF NOT EXISTS `securitygroups_records` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `record_id` char(36) DEFAULT NULL,
  `module` char(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_securitygroups_records_mod` (`module`,`deleted`,`record_id`,`securitygroup_id`),
  KEY `idx_securitygroups_records_del` (`deleted`,`record_id`,`module`,`securitygroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `securitygroups_users`
--

CREATE TABLE IF NOT EXISTS `securitygroups_users` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `securitygroup_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `primary_group` tinyint(1) DEFAULT NULL,
  `noninheritable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `securitygroups_users_idxa` (`securitygroup_id`),
  KEY `securitygroups_users_idxb` (`user_id`),
  KEY `securitygroups_users_idxc` (`user_id`,`deleted`,`securitygroup_id`,`id`),
  KEY `securitygroups_users_idxd` (`user_id`,`deleted`,`securitygroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `sugarfeed`
--

CREATE TABLE IF NOT EXISTS `sugarfeed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `related_module` varchar(100) DEFAULT NULL,
  `related_id` char(36) DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sgrfeed_date` (`date_entered`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `sugarfeed`
--

INSERT INTO `sugarfeed` (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `assigned_user_id`, `related_module`, `related_id`, `link_url`, `link_type`) VALUES
('8d8f9e3a-21b1-fd42-0233-538ffe20fe5d', '<b>{this.CREATED_BY}</b> {SugarFeed.CREATED_LEAD} [Leads:667f020f-5560-1b4b-44b4-538ffeb0a947:Jean Paul Sanchez]', '2014-06-05 05:23:46', '2014-06-05 05:23:46', '1', '1', NULL, 0, '1', 'Leads', '667f020f-5560-1b4b-44b4-538ffeb0a947', NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `date_due_flag` tinyint(1) DEFAULT '0',
  `date_due` datetime DEFAULT NULL,
  `date_start_flag` tinyint(1) DEFAULT '0',
  `date_start` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tsk_name` (`name`),
  KEY `idx_task_con_del` (`contact_id`,`deleted`),
  KEY `idx_task_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_task_assigned` (`assigned_user_id`),
  KEY `idx_task_status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `tracker`
--

CREATE TABLE IF NOT EXISTS `tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitor_id` char(36) NOT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` varchar(36) DEFAULT NULL,
  `item_summary` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `session_id` varchar(36) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_tracker_iid` (`item_id`),
  KEY `idx_tracker_userid_vis_id` (`user_id`,`visible`,`id`),
  KEY `idx_tracker_userid_itemid_vis` (`user_id`,`item_id`,`visible`),
  KEY `idx_tracker_monitor_id` (`monitor_id`),
  KEY `idx_tracker_date_modified` (`date_modified`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Dump dei dati per la tabella `tracker`
--

INSERT INTO `tracker` (`id`, `monitor_id`, `user_id`, `module_name`, `item_id`, `item_summary`, `date_modified`, `action`, `session_id`, `visible`, `deleted`) VALUES
(1, 'd8964520-85a1-e5e6-b834-5388ed6eacee', '1', 'prov1_Aziende', '962d5285-601b-05a2-fd08-5388ed75e930', 'DOUBLE-BOX GHZ', '2014-05-30 20:41:48', 'detailview', 'a7sto38o66tris8lvshd77gfe0', 1, 0),
(2, '8e365999-e13d-ab1a-28e7-5388ed66c22b', '1', 'prov1_Dipendenti', '4e7ce051-92dc-ae5c-60c0-5388ed772d25', 'Sig. Jean Paul Sanchez', '2014-05-30 20:43:57', 'detailview', 'a7sto38o66tris8lvshd77gfe0', 1, 0),
(12, '539f23a4-1e88-fefc-9504-538ae603bc06', '1', 'prov2_Aziende', 'b1b90c4a-5434-341f-7f5e-538ae65a4228', 'DOUBLE-BOX GHZ', '2014-06-01 08:39:04', 'detailview', 'un4mqboonv93mnn1i5389qgch4', 1, 0),
(4, '93bf8321-0e8a-19c5-2988-53899638b12d', '1', 'prov2_Aziende_A', '96eb9ab5-f9da-1486-9eb3-5389960405aa', 'Prova1', '2014-05-31 08:45:03', 'editview', 'a7sto38o66tris8lvshd77gfe0', 1, 0),
(42, '923590ec-9ec1-9a1e-7f3a-53901841ed0d', '1', 'prov2_F_Potenziale_Cliente', '5858496c-fb2a-a1bc-d91c-53901819ab5f', 'Ivan', '2014-06-05 07:13:03', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(8, '2e3aa152-657f-6100-aee5-538998204248', '1', 'prov2_Aziende_A', '65920729-4dbe-1ef2-9621-538998b581f5', 'DOUBLE-BOX GHZ', '2014-05-31 08:54:26', 'detailview', 'a7sto38o66tris8lvshd77gfe0', 1, 0),
(10, '1787d960-27f6-5bd3-53ff-53899bc6c662', '1', 'prov1_Dipendenti', 'd1ce72f7-3c43-1b66-6456-53899bee0127', 'Sanchez', '2014-05-31 09:04:21', 'detailview', 'a7sto38o66tris8lvshd77gfe0', 1, 0),
(11, '8c707047-e426-21d6-4e97-53899bf4fdda', '1', 'prov1_Aziende', '2f687096-ca5a-83e9-4dcb-53899a37bda9', 'DOUBLE-BOX GHZ', '2014-05-31 09:04:53', 'detailview', 'a7sto38o66tris8lvshd77gfe0', 1, 0),
(13, '552949f7-91ae-b42f-d531-538aeaa4c046', '1', 'prov2_az1', '1bc8e6ea-34b1-fdb2-f6e2-538aea71dc1d', 'DOUBLE-BOX GHZ', '2014-06-01 08:54:01', 'detailview', 'un4mqboonv93mnn1i5389qgch4', 1, 0),
(20, '2b2f7e4c-63fa-6b4f-2d81-538f94652e6b', '1', 'prov1_C_Azienda', '842b1b7d-e3bf-fa1c-f444-538f92be6a60', 'DOUBLE-BOX GHZ', '2014-06-04 21:50:23', 'editview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(15, 'c278008a-4673-b4ab-67b4-538b16d0d805', '1', 'prov2_dip1', 'e70a50d5-d442-d730-93fa-538aeae5d735', 'Jean Paul', '2014-06-01 12:02:49', 'detailview', 'un4mqboonv93mnn1i5389qgch4', 1, 0),
(25, '28fdec78-4c60-a0a9-df45-538fa2f57c68', '1', 'prov1_C_Azienda', 'e0950c3d-bea7-3cc0-265f-538fa20d6eed', 'Double-box Inc', '2014-06-04 22:48:05', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(24, '90f3e0b8-e99b-7e1e-28df-538f9d748dcd', '1', 'prov1_C_Azienda', '6f77c367-a8b5-69c9-6a05-538f9d0b4df6', 'DOUBLE-BOX GHZ', '2014-06-04 22:30:15', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(23, '9344c893-b14a-e052-f96d-538f9da00d1b', '1', 'prov1_C_Dipendenti', '1501038f-df57-946e-a3b6-538f9dbad792', 'Jean Paul', '2014-06-04 22:30:06', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(26, '970b3f46-a8d0-3df9-592b-538fa2219a96', '1', 'prov1_C_Azienda', '601bebca-0dcd-85b1-af98-538fa22a9889', 'AstreaProject Srl', '2014-06-04 22:49:28', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(27, 'c6b9092f-4bf9-0d85-c6b9-538fa263befd', '1', 'prov1_C_Azienda', 'c961c424-8fa7-5b08-33fd-538fa26032da', 'BENQ', '2014-06-04 22:50:06', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(35, 'c5c22066-415d-3bec-4e82-53901316366f', '1', 'prov2_F_Contratti', '874140f8-906e-04f8-c1ef-539013f37193', '12345', '2014-06-05 06:52:24', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 0, 0),
(29, '7adb28f9-c688-6845-f512-538fa21c6621', '1', 'prov1_C_Dipendenti', '4003d734-26c5-86cb-efdf-538fa2ac4b4a', 'Ivan', '2014-06-04 22:51:40', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(30, '91334512-8e6e-8e95-e4bc-538fa3e58102', '1', 'prov1_C_Dipendenti', '4b5f453b-874d-fb4c-029f-538fa3f7a1df', 'Valeria', '2014-06-04 22:52:24', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(31, '36eef8c2-0bd7-0dd6-fcb6-538fa37a9f8a', '1', 'prov1_C_Dipendenti', 'ee479d9c-6f99-a151-b076-538fa361090c', 'Micky', '2014-06-04 22:53:04', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(32, '72adcf6f-901f-d366-a3c0-538fa442c372', '1', 'prov1_C_Dipendenti', 'b0332842-6a2f-c034-f290-538fa2d2cfeb', 'Jean Paul', '2014-06-04 22:58:31', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(34, '5883011b-a7aa-0497-e568-538ffe130984', '1', 'Leads', '667f020f-5560-1b4b-44b4-538ffeb0a947', 'Dott. Jean Paul Sanchez', '2014-06-05 05:24:00', 'editview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(36, '939b7bbb-ffc7-a650-3ef7-539015a892b8', '1', 'prov2_F_Contratti', '580895b4-1648-8488-5430-539015f5639a', '12346', '2014-06-05 06:58:59', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 0, 0),
(37, '7becfe89-623f-73fa-486d-53901516635c', '1', 'prov2_F_Cliente', '3e27931a-c616-8474-5217-539015710b0e', 'Jean Paul', '2014-06-05 07:00:44', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 0, 0),
(38, 'de327b0e-f078-717c-4e70-539016c9b986', '1', 'prov2_F_Contratti', 'a58da1aa-9e84-d318-0203-5390167dfa5f', '12345', '2014-06-05 07:04:05', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(39, '9ea6048f-8d16-e13b-370a-5390161807ef', '1', 'prov2_F_Contratti', 'c3370320-29f2-e94c-286b-53901677d6b1', '12347', '2014-06-05 07:04:31', 'detailview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(57, 'bb74c693-e5d2-a9dd-4504-539199e4e4a6', '1', 'prov2_F_Cliente', '46c2e655-406c-d083-1ce3-539017cb6b78', 'Jean Paul', '2014-06-06 10:35:19', 'detailview', 'mtq9o2315oaenjdi7pq1s1ag00', 1, 0),
(41, '5f6ae6e5-1ddf-406d-767c-539018f1ed39', '1', 'Users', '1', 'Jean Paul Sanchez', '2014-06-05 07:12:09', 'editview', 'uhc20f35989qs8jn59gl5raiu2', 1, 0),
(43, 'e26844f7-f819-9aab-d87b-5390f1d6ad0a', '1', 'prov2_F_Upload', 'a0f94aa1-76e8-7cd3-e8a7-5390f16f05aa', 'contratto_01', '2014-06-05 22:36:54', 'detailview', '64u3iqc6ts1ap1iubeqbsft063', 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `upgrade_history`
--

CREATE TABLE IF NOT EXISTS `upgrade_history` (
  `id` char(36) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `md5sum` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `version` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `id_name` varchar(255) DEFAULT NULL,
  `manifest` longtext,
  `date_entered` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `upgrade_history_md5_uk` (`md5sum`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `upgrade_history`
--

INSERT INTO `upgrade_history` (`id`, `filename`, `md5sum`, `type`, `status`, `version`, `name`, `description`, `id_name`, `manifest`, `date_entered`, `enabled`) VALUES
('20072a0d-2dc7-163b-05b4-5388e72e0519', 'upload/upgrades/langpack/Sugar_it_IT_CE_PRO_ENT_6.5.x-rev3.zip', 'f43b2678ba7e1bccae1a09016e18f9c1', 'langpack', 'installed', '6.5.x-rev3', 'it_IT Lang Pack OPENSYMBOL', 'Italian language pack for Sugar 6.5.x CE PRO ENT by OPENSYMBOL', NULL, 'YTozOntzOjg6Im1hbmlmZXN0IjthOjExOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e2k6MDthOjA6e319czoyNDoiYWNjZXB0YWJsZV9zdWdhcl9mbGF2b3JzIjthOjU6e2k6MDtzOjI6IkNFIjtpOjE7czozOiJQUk8iO2k6MjtzOjM6IkVOVCI7aTozO3M6NDoiQ09SUCI7aTo0O3M6MzoiVUxUIjt9czoxNjoibGFuZ19maWxlX3N1ZmZpeCI7czo1OiJpdF9pdCI7czo0OiJuYW1lIjtzOjI2OiJpdF9JVCBMYW5nIFBhY2sgT1BFTlNZTUJPTCI7czoxMToiZGVzY3JpcHRpb24iO3M6NjI6Ikl0YWxpYW4gbGFuZ3VhZ2UgcGFjayBmb3IgU3VnYXIgNi41LnggQ0UgUFJPIEVOVCBieSBPUEVOU1lNQk9MIjtzOjY6ImF1dGhvciI7czoxMDoiT3BlbnN5bWJvbCI7czoxNDoicHVibGlzaGVkX2RhdGUiO3M6MTA6IjIwMTMtMDctMjQiO3M6NzoidmVyc2lvbiI7czoxMDoiNi41LngtcmV2MyI7czo0OiJ0eXBlIjtzOjg6ImxhbmdwYWNrIjtzOjQ6Imljb24iO3M6Mjk6ImluY2x1ZGUvaW1hZ2VzL2ZsYWdfaXRfSVQuZ2lmIjtzOjE2OiJpc191bmluc3RhbGxhYmxlIjtzOjM6IlllcyI7fXM6MTE6Imluc3RhbGxkZWZzIjtzOjA6IiI7czoxNjoidXBncmFkZV9tYW5pZmVzdCI7czowOiIiO30=', '2014-05-30 20:16:10', 1),
('dd666e5d-c54f-7a25-e77c-538fa1efdb75', 'upload/upgrades/module/compito12014_06_05_010222.zip', '32ac127b1742da4bb382563800bf2489', 'module', 'installed', '1401922942', 'compito1', NULL, 'compito1', 'YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e2k6MDtzOjA6IiI7fX1pOjE7YToxOntzOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6Mzp7aTowO3M6MjoiQ0UiO2k6MTtzOjM6IlBSTyI7aToyO3M6MzoiRU5UIjt9fXM6NjoicmVhZG1lIjtzOjA6IiI7czozOiJrZXkiO3M6NToicHJvdjEiO3M6NjoiYXV0aG9yIjtzOjE3OiJTYW5jaGV6IEplYW4gUGF1bCI7czoxMToiZGVzY3JpcHRpb24iO3M6MDoiIjtzOjQ6Imljb24iO3M6MDoiIjtzOjE2OiJpc191bmluc3RhbGxhYmxlIjtiOjE7czo0OiJuYW1lIjtzOjg6ImNvbXBpdG8xIjtzOjE0OiJwdWJsaXNoZWRfZGF0ZSI7czoxOToiMjAxNC0wNi0wNCAyMzowMjoyMiI7czo0OiJ0eXBlIjtzOjY6Im1vZHVsZSI7czo3OiJ2ZXJzaW9uIjtpOjE0MDE5MjI5NDI7czoxMzoicmVtb3ZlX3RhYmxlcyI7czo2OiJwcm9tcHQiO31zOjExOiJpbnN0YWxsZGVmcyI7YTo5OntzOjI6ImlkIjtzOjg6ImNvbXBpdG8xIjtzOjU6ImJlYW5zIjthOjI6e2k6MDthOjQ6e3M6NjoibW9kdWxlIjtzOjE1OiJwcm92MV9DX0F6aWVuZGEiO3M6NToiY2xhc3MiO3M6MTU6InByb3YxX0NfQXppZW5kYSI7czo0OiJwYXRoIjtzOjQzOiJtb2R1bGVzL3Byb3YxX0NfQXppZW5kYS9wcm92MV9DX0F6aWVuZGEucGhwIjtzOjM6InRhYiI7YjoxO31pOjE7YTo0OntzOjY6Im1vZHVsZSI7czoxODoicHJvdjFfQ19EaXBlbmRlbnRpIjtzOjU6ImNsYXNzIjtzOjE4OiJwcm92MV9DX0RpcGVuZGVudGkiO3M6NDoicGF0aCI7czo0OToibW9kdWxlcy9wcm92MV9DX0RpcGVuZGVudGkvcHJvdjFfQ19EaXBlbmRlbnRpLnBocCI7czozOiJ0YWIiO2I6MTt9fXM6MTA6ImxheW91dGRlZnMiO2E6MTp7aTowO2E6Mjp7czo0OiJmcm9tIjtzOjEwMzoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9sYXlvdXRkZWZzL3Byb3YxX2NfYXppZW5kYV9wcm92MV9jX2RpcGVuZGVudGlfcHJvdjFfQ19BemllbmRhLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTU6InByb3YxX0NfQXppZW5kYSI7fX1zOjEzOiJyZWxhdGlvbnNoaXBzIjthOjE6e2k6MDthOjE6e3M6OToibWV0YV9kYXRhIjtzOjk4OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL3JlbGF0aW9uc2hpcHMvcHJvdjFfY19hemllbmRhX3Byb3YxX2NfZGlwZW5kZW50aU1ldGFEYXRhLnBocCI7fX1zOjk6ImltYWdlX2RpciI7czoxNjoiPGJhc2VwYXRoPi9pY29ucyI7czo0OiJjb3B5IjthOjI6e2k6MDthOjI6e3M6NDoiZnJvbSI7czo0NzoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9wcm92MV9DX0F6aWVuZGEiO3M6MjoidG8iO3M6MjM6Im1vZHVsZXMvcHJvdjFfQ19BemllbmRhIjt9aToxO2E6Mjp7czo0OiJmcm9tIjtzOjUwOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL3Byb3YxX0NfRGlwZW5kZW50aSI7czoyOiJ0byI7czoyNjoibW9kdWxlcy9wcm92MV9DX0RpcGVuZGVudGkiO319czo4OiJsYW5ndWFnZSI7YToxMDp7aTowO2E6Mzp7czo0OiJmcm9tIjtzOjY5OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YxX0NfRGlwZW5kZW50aS5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE4OiJwcm92MV9DX0RpcGVuZGVudGkiO3M6ODoibGFuZ3VhZ2UiO3M6NToiZW5fdXMiO31pOjE7YTozOntzOjQ6ImZyb20iO3M6Njk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL3JlbGF0aW9uc2hpcHMvbGFuZ3VhZ2UvcHJvdjFfQ19EaXBlbmRlbnRpLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTg6InByb3YxX0NfRGlwZW5kZW50aSI7czo4OiJsYW5ndWFnZSI7czo1OiJlc19lcyI7fWk6MjthOjM6e3M6NDoiZnJvbSI7czo2OToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9sYW5ndWFnZS9wcm92MV9DX0RpcGVuZGVudGkucGhwIjtzOjk6InRvX21vZHVsZSI7czoxODoicHJvdjFfQ19EaXBlbmRlbnRpIjtzOjg6Imxhbmd1YWdlIjtzOjU6InJ1X3J1Ijt9aTozO2E6Mzp7czo0OiJmcm9tIjtzOjY5OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YxX0NfRGlwZW5kZW50aS5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE4OiJwcm92MV9DX0RpcGVuZGVudGkiO3M6ODoibGFuZ3VhZ2UiO3M6NToiaXRfaXQiO31pOjQ7YTozOntzOjQ6ImZyb20iO3M6NjY6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL3JlbGF0aW9uc2hpcHMvbGFuZ3VhZ2UvcHJvdjFfQ19BemllbmRhLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTU6InByb3YxX0NfQXppZW5kYSI7czo4OiJsYW5ndWFnZSI7czo1OiJlbl91cyI7fWk6NTthOjM6e3M6NDoiZnJvbSI7czo2NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9sYW5ndWFnZS9wcm92MV9DX0F6aWVuZGEucGhwIjtzOjk6InRvX21vZHVsZSI7czoxNToicHJvdjFfQ19BemllbmRhIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVzX2VzIjt9aTo2O2E6Mzp7czo0OiJmcm9tIjtzOjY2OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YxX0NfQXppZW5kYS5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE1OiJwcm92MV9DX0F6aWVuZGEiO3M6ODoibGFuZ3VhZ2UiO3M6NToicnVfcnUiO31pOjc7YTozOntzOjQ6ImZyb20iO3M6NjY6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL3JlbGF0aW9uc2hpcHMvbGFuZ3VhZ2UvcHJvdjFfQ19BemllbmRhLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTU6InByb3YxX0NfQXppZW5kYSI7czo4OiJsYW5ndWFnZSI7czo1OiJpdF9pdCI7fWk6ODthOjM6e3M6NDoiZnJvbSI7czo1OToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbGFuZ3VhZ2UvYXBwbGljYXRpb24vZW5fdXMubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjExOiJhcHBsaWNhdGlvbiI7czo4OiJsYW5ndWFnZSI7czo1OiJlbl91cyI7fWk6OTthOjM6e3M6NDoiZnJvbSI7czo1OToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbGFuZ3VhZ2UvYXBwbGljYXRpb24vaXRfaXQubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjExOiJhcHBsaWNhdGlvbiI7czo4OiJsYW5ndWFnZSI7czo1OiJpdF9pdCI7fX1zOjc6InZhcmRlZnMiO2E6Mjp7aTowO2E6Mjp7czo0OiJmcm9tIjtzOjEwMzoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy92YXJkZWZzL3Byb3YxX2NfYXppZW5kYV9wcm92MV9jX2RpcGVuZGVudGlfcHJvdjFfQ19EaXBlbmRlbnRpLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTg6InByb3YxX0NfRGlwZW5kZW50aSI7fWk6MTthOjI6e3M6NDoiZnJvbSI7czoxMDA6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL3JlbGF0aW9uc2hpcHMvdmFyZGVmcy9wcm92MV9jX2F6aWVuZGFfcHJvdjFfY19kaXBlbmRlbnRpX3Byb3YxX0NfQXppZW5kYS5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE1OiJwcm92MV9DX0F6aWVuZGEiO319czoxMjoibGF5b3V0ZmllbGRzIjthOjE6e2k6MDthOjE6e3M6MTc6ImFkZGl0aW9uYWxfZmllbGRzIjthOjA6e319fX1zOjE2OiJ1cGdyYWRlX21hbmlmZXN0IjtzOjA6IiI7fQ==', '2014-06-04 22:46:32', 1),
('6264d9b8-d15f-d713-f237-539013344741', 'upload/upgrades/module/Compito22014_06_06_122935.zip', 'a4027b4864fafc59f4e37088f0799533', 'module', 'installed', '1402050574', 'Compito2', NULL, 'Compito2', 'YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e2k6MDtzOjA6IiI7fX1pOjE7YToxOntzOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6Mzp7aTowO3M6MjoiQ0UiO2k6MTtzOjM6IlBSTyI7aToyO3M6MzoiRU5UIjt9fXM6NjoicmVhZG1lIjtzOjA6IiI7czozOiJrZXkiO3M6NToicHJvdjIiO3M6NjoiYXV0aG9yIjtzOjE3OiJTYW5jaGV6IEplYW4gUGF1bCI7czoxMToiZGVzY3JpcHRpb24iO3M6MDoiIjtzOjQ6Imljb24iO3M6MDoiIjtzOjE2OiJpc191bmluc3RhbGxhYmxlIjtiOjE7czo0OiJuYW1lIjtzOjg6IkNvbXBpdG8yIjtzOjE0OiJwdWJsaXNoZWRfZGF0ZSI7czoxOToiMjAxNC0wNi0wNiAxMDoyOTozNCI7czo0OiJ0eXBlIjtzOjY6Im1vZHVsZSI7czo3OiJ2ZXJzaW9uIjtpOjE0MDIwNTA1NzQ7czoxMzoicmVtb3ZlX3RhYmxlcyI7czo2OiJwcm9tcHQiO31zOjExOiJpbnN0YWxsZGVmcyI7YTo5OntzOjI6ImlkIjtzOjg6IkNvbXBpdG8yIjtzOjU6ImJlYW5zIjthOjQ6e2k6MDthOjQ6e3M6NjoibW9kdWxlIjtzOjE1OiJwcm92Ml9GX0NsaWVudGUiO3M6NToiY2xhc3MiO3M6MTU6InByb3YyX0ZfQ2xpZW50ZSI7czo0OiJwYXRoIjtzOjQzOiJtb2R1bGVzL3Byb3YyX0ZfQ2xpZW50ZS9wcm92Ml9GX0NsaWVudGUucGhwIjtzOjM6InRhYiI7YjoxO31pOjE7YTo0OntzOjY6Im1vZHVsZSI7czoxNzoicHJvdjJfRl9Db250cmF0dGkiO3M6NToiY2xhc3MiO3M6MTc6InByb3YyX0ZfQ29udHJhdHRpIjtzOjQ6InBhdGgiO3M6NDc6Im1vZHVsZXMvcHJvdjJfRl9Db250cmF0dGkvcHJvdjJfRl9Db250cmF0dGkucGhwIjtzOjM6InRhYiI7YjoxO31pOjI7YTo0OntzOjY6Im1vZHVsZSI7czoyNjoicHJvdjJfRl9Qb3RlbnppYWxlX0NsaWVudGUiO3M6NToiY2xhc3MiO3M6MjY6InByb3YyX0ZfUG90ZW56aWFsZV9DbGllbnRlIjtzOjQ6InBhdGgiO3M6NjU6Im1vZHVsZXMvcHJvdjJfRl9Qb3RlbnppYWxlX0NsaWVudGUvcHJvdjJfRl9Qb3RlbnppYWxlX0NsaWVudGUucGhwIjtzOjM6InRhYiI7YjoxO31pOjM7YTo0OntzOjY6Im1vZHVsZSI7czoxNDoicHJvdjJfRl9VcGxvYWQiO3M6NToiY2xhc3MiO3M6MTQ6InByb3YyX0ZfVXBsb2FkIjtzOjQ6InBhdGgiO3M6NDE6Im1vZHVsZXMvcHJvdjJfRl9VcGxvYWQvcHJvdjJfRl9VcGxvYWQucGhwIjtzOjM6InRhYiI7YjoxO319czoxMDoibGF5b3V0ZGVmcyI7YTowOnt9czoxMzoicmVsYXRpb25zaGlwcyI7YToyOntpOjA7YToxOntzOjk6Im1ldGFfZGF0YSI7czo5NzoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9yZWxhdGlvbnNoaXBzL3Byb3YyX2ZfY2xpZW50ZV9wcm92Ml9mX2NvbnRyYXR0aU1ldGFEYXRhLnBocCI7fWk6MTthOjE6e3M6OToibWV0YV9kYXRhIjtzOjk0OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL3JlbGF0aW9uc2hpcHMvcHJvdjJfZl91cGxvYWRfcHJvdjJfZl9jbGllbnRlTWV0YURhdGEucGhwIjt9fXM6OToiaW1hZ2VfZGlyIjtzOjE2OiI8YmFzZXBhdGg+L2ljb25zIjtzOjQ6ImNvcHkiO2E6NDp7aTowO2E6Mjp7czo0OiJmcm9tIjtzOjQ3OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL3Byb3YyX0ZfQ2xpZW50ZSI7czoyOiJ0byI7czoyMzoibW9kdWxlcy9wcm92Ml9GX0NsaWVudGUiO31pOjE7YToyOntzOjQ6ImZyb20iO3M6NDk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvcHJvdjJfRl9Db250cmF0dGkiO3M6MjoidG8iO3M6MjU6Im1vZHVsZXMvcHJvdjJfRl9Db250cmF0dGkiO31pOjI7YToyOntzOjQ6ImZyb20iO3M6NTg6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvcHJvdjJfRl9Qb3RlbnppYWxlX0NsaWVudGUiO3M6MjoidG8iO3M6MzQ6Im1vZHVsZXMvcHJvdjJfRl9Qb3RlbnppYWxlX0NsaWVudGUiO31pOjM7YToyOntzOjQ6ImZyb20iO3M6NDY6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvcHJvdjJfRl9VcGxvYWQiO3M6MjoidG8iO3M6MjI6Im1vZHVsZXMvcHJvdjJfRl9VcGxvYWQiO319czo4OiJsYW5ndWFnZSI7YToyMDp7aTowO2E6Mzp7czo0OiJmcm9tIjtzOjY4OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YyX0ZfQ29udHJhdHRpLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTc6InByb3YyX0ZfQ29udHJhdHRpIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVuX3VzIjt9aToxO2E6Mzp7czo0OiJmcm9tIjtzOjY4OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YyX0ZfQ29udHJhdHRpLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTc6InByb3YyX0ZfQ29udHJhdHRpIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVzX2VzIjt9aToyO2E6Mzp7czo0OiJmcm9tIjtzOjY4OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YyX0ZfQ29udHJhdHRpLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTc6InByb3YyX0ZfQ29udHJhdHRpIjtzOjg6Imxhbmd1YWdlIjtzOjU6InJ1X3J1Ijt9aTozO2E6Mzp7czo0OiJmcm9tIjtzOjY4OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YyX0ZfQ29udHJhdHRpLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTc6InByb3YyX0ZfQ29udHJhdHRpIjtzOjg6Imxhbmd1YWdlIjtzOjU6Iml0X2l0Ijt9aTo0O2E6Mzp7czo0OiJmcm9tIjtzOjY2OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YyX0ZfQ2xpZW50ZS5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE1OiJwcm92Ml9GX0NsaWVudGUiO3M6ODoibGFuZ3VhZ2UiO3M6NToiZW5fdXMiO31pOjU7YTozOntzOjQ6ImZyb20iO3M6NjY6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL3JlbGF0aW9uc2hpcHMvbGFuZ3VhZ2UvcHJvdjJfRl9DbGllbnRlLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTU6InByb3YyX0ZfQ2xpZW50ZSI7czo4OiJsYW5ndWFnZSI7czo1OiJlc19lcyI7fWk6NjthOjM6e3M6NDoiZnJvbSI7czo2NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9sYW5ndWFnZS9wcm92Ml9GX0NsaWVudGUucGhwIjtzOjk6InRvX21vZHVsZSI7czoxNToicHJvdjJfRl9DbGllbnRlIjtzOjg6Imxhbmd1YWdlIjtzOjU6InJ1X3J1Ijt9aTo3O2E6Mzp7czo0OiJmcm9tIjtzOjY2OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YyX0ZfQ2xpZW50ZS5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE1OiJwcm92Ml9GX0NsaWVudGUiO3M6ODoibGFuZ3VhZ2UiO3M6NToiaXRfaXQiO31pOjg7YTozOntzOjQ6ImZyb20iO3M6NjY6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL3JlbGF0aW9uc2hpcHMvbGFuZ3VhZ2UvcHJvdjJfRl9DbGllbnRlLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTU6InByb3YyX0ZfQ2xpZW50ZSI7czo4OiJsYW5ndWFnZSI7czo1OiJlbl91cyI7fWk6OTthOjM6e3M6NDoiZnJvbSI7czo2NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9sYW5ndWFnZS9wcm92Ml9GX0NsaWVudGUucGhwIjtzOjk6InRvX21vZHVsZSI7czoxNToicHJvdjJfRl9DbGllbnRlIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVzX2VzIjt9aToxMDthOjM6e3M6NDoiZnJvbSI7czo2NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9sYW5ndWFnZS9wcm92Ml9GX0NsaWVudGUucGhwIjtzOjk6InRvX21vZHVsZSI7czoxNToicHJvdjJfRl9DbGllbnRlIjtzOjg6Imxhbmd1YWdlIjtzOjU6InJ1X3J1Ijt9aToxMTthOjM6e3M6NDoiZnJvbSI7czo2NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9sYW5ndWFnZS9wcm92Ml9GX0NsaWVudGUucGhwIjtzOjk6InRvX21vZHVsZSI7czoxNToicHJvdjJfRl9DbGllbnRlIjtzOjg6Imxhbmd1YWdlIjtzOjU6Iml0X2l0Ijt9aToxMjthOjM6e3M6NDoiZnJvbSI7czo2NToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9sYW5ndWFnZS9wcm92Ml9GX1VwbG9hZC5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE0OiJwcm92Ml9GX1VwbG9hZCI7czo4OiJsYW5ndWFnZSI7czo1OiJlbl91cyI7fWk6MTM7YTozOntzOjQ6ImZyb20iO3M6NjU6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL3JlbGF0aW9uc2hpcHMvbGFuZ3VhZ2UvcHJvdjJfRl9VcGxvYWQucGhwIjtzOjk6InRvX21vZHVsZSI7czoxNDoicHJvdjJfRl9VcGxvYWQiO3M6ODoibGFuZ3VhZ2UiO3M6NToiZXNfZXMiO31pOjE0O2E6Mzp7czo0OiJmcm9tIjtzOjY1OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL2xhbmd1YWdlL3Byb3YyX0ZfVXBsb2FkLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTQ6InByb3YyX0ZfVXBsb2FkIjtzOjg6Imxhbmd1YWdlIjtzOjU6InJ1X3J1Ijt9aToxNTthOjM6e3M6NDoiZnJvbSI7czo2NToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy9sYW5ndWFnZS9wcm92Ml9GX1VwbG9hZC5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE0OiJwcm92Ml9GX1VwbG9hZCI7czo4OiJsYW5ndWFnZSI7czo1OiJpdF9pdCI7fWk6MTY7YTozOntzOjQ6ImZyb20iO3M6NTk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL2xhbmd1YWdlL2FwcGxpY2F0aW9uL2VuX3VzLmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxMToiYXBwbGljYXRpb24iO3M6ODoibGFuZ3VhZ2UiO3M6NToiZW5fdXMiO31pOjE3O2E6Mzp7czo0OiJmcm9tIjtzOjU5OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9sYW5ndWFnZS9hcHBsaWNhdGlvbi9lc19lcy5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTE6ImFwcGxpY2F0aW9uIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVzX2VzIjt9aToxODthOjM6e3M6NDoiZnJvbSI7czo1OToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbGFuZ3VhZ2UvYXBwbGljYXRpb24vaXRfaXQubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjExOiJhcHBsaWNhdGlvbiI7czo4OiJsYW5ndWFnZSI7czo1OiJpdF9pdCI7fWk6MTk7YTozOntzOjQ6ImZyb20iO3M6NTk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL2xhbmd1YWdlL2FwcGxpY2F0aW9uL3J1X3J1LmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxMToiYXBwbGljYXRpb24iO3M6ODoibGFuZ3VhZ2UiO3M6NToicnVfcnUiO319czo3OiJ2YXJkZWZzIjthOjQ6e2k6MDthOjI6e3M6NDoiZnJvbSI7czoxMDE6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL3JlbGF0aW9uc2hpcHMvdmFyZGVmcy9wcm92Ml9mX2NsaWVudGVfcHJvdjJfZl9jb250cmF0dGlfcHJvdjJfRl9Db250cmF0dGkucGhwIjtzOjk6InRvX21vZHVsZSI7czoxNzoicHJvdjJfRl9Db250cmF0dGkiO31pOjE7YToyOntzOjQ6ImZyb20iO3M6OTk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL3JlbGF0aW9uc2hpcHMvdmFyZGVmcy9wcm92Ml9mX2NsaWVudGVfcHJvdjJfZl9jb250cmF0dGlfcHJvdjJfRl9DbGllbnRlLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTU6InByb3YyX0ZfQ2xpZW50ZSI7fWk6MjthOjI6e3M6NDoiZnJvbSI7czo5NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvcmVsYXRpb25zaGlwcy92YXJkZWZzL3Byb3YyX2ZfdXBsb2FkX3Byb3YyX2ZfY2xpZW50ZV9wcm92Ml9GX0NsaWVudGUucGhwIjtzOjk6InRvX21vZHVsZSI7czoxNToicHJvdjJfRl9DbGllbnRlIjt9aTozO2E6Mjp7czo0OiJmcm9tIjtzOjk1OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9yZWxhdGlvbnNoaXBzL3ZhcmRlZnMvcHJvdjJfZl91cGxvYWRfcHJvdjJfZl9jbGllbnRlX3Byb3YyX0ZfVXBsb2FkLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTQ6InByb3YyX0ZfVXBsb2FkIjt9fXM6MTI6ImxheW91dGZpZWxkcyI7YToyOntpOjA7YToxOntzOjE3OiJhZGRpdGlvbmFsX2ZpZWxkcyI7YTowOnt9fWk6MTthOjE6e3M6MTc6ImFkZGl0aW9uYWxfZmllbGRzIjthOjA6e319fX1zOjE2OiJ1cGdyYWRlX21hbmlmZXN0IjtzOjA6IiI7fQ==', '2014-06-05 06:51:26', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` char(36) NOT NULL,
  `user_name` varchar(60) DEFAULT NULL,
  `user_hash` varchar(255) DEFAULT NULL,
  `system_generated_password` tinyint(1) DEFAULT NULL,
  `pwd_last_changed` datetime DEFAULT NULL,
  `authenticate_id` varchar(100) DEFAULT NULL,
  `sugar_login` tinyint(1) DEFAULT '1',
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `external_auth_only` tinyint(1) DEFAULT '0',
  `receive_notifications` tinyint(1) DEFAULT '1',
  `description` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `phone_home` varchar(50) DEFAULT NULL,
  `phone_mobile` varchar(50) DEFAULT NULL,
  `phone_work` varchar(50) DEFAULT NULL,
  `phone_other` varchar(50) DEFAULT NULL,
  `phone_fax` varchar(50) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `address_street` varchar(150) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `portal_only` tinyint(1) DEFAULT '0',
  `show_on_employees` tinyint(1) DEFAULT '1',
  `employee_status` varchar(100) DEFAULT NULL,
  `messenger_id` varchar(100) DEFAULT NULL,
  `messenger_type` varchar(100) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `is_group` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_name` (`user_name`,`is_group`,`status`,`last_name`,`first_name`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `user_name`, `user_hash`, `system_generated_password`, `pwd_last_changed`, `authenticate_id`, `sugar_login`, `first_name`, `last_name`, `is_admin`, `external_auth_only`, `receive_notifications`, `description`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `title`, `department`, `phone_home`, `phone_mobile`, `phone_work`, `phone_other`, `phone_fax`, `status`, `address_street`, `address_city`, `address_state`, `address_country`, `address_postalcode`, `deleted`, `portal_only`, `show_on_employees`, `employee_status`, `messenger_id`, `messenger_type`, `reports_to_id`, `is_group`) VALUES
('1', 'admin', '$1$Uv3.NK3.$gVcszihtTGI93dsVt6OrC1', 0, NULL, NULL, 1, 'Jean Paul', 'Sanchez', 1, 0, 1, NULL, '2014-05-30 20:01:02', '2014-05-30 20:02:44', '1', '', 'Administrator', NULL, NULL, NULL, NULL, NULL, NULL, 'Active', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 'Active', NULL, NULL, '', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `users_feeds`
--

CREATE TABLE IF NOT EXISTS `users_feeds` (
  `user_id` varchar(36) DEFAULT NULL,
  `feed_id` varchar(36) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_ud_user_id` (`user_id`,`feed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `users_last_import`
--

CREATE TABLE IF NOT EXISTS `users_last_import` (
  `id` char(36) NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `import_module` varchar(36) DEFAULT NULL,
  `bean_type` varchar(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `users_password_link`
--

CREATE TABLE IF NOT EXISTS `users_password_link` (
  `id` char(36) NOT NULL,
  `username` varchar(36) DEFAULT NULL,
  `date_generated` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `users_signatures`
--

CREATE TABLE IF NOT EXISTS `users_signatures` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `signature` text,
  `signature_html` text,
  PRIMARY KEY (`id`),
  KEY `idx_usersig_uid` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `user_preferences`
--

CREATE TABLE IF NOT EXISTS `user_preferences` (
  `id` char(36) NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` longtext,
  PRIMARY KEY (`id`),
  KEY `idx_userprefnamecat` (`assigned_user_id`,`category`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `user_preferences`
--

INSERT INTO `user_preferences` (`id`, `category`, `deleted`, `date_entered`, `date_modified`, `assigned_user_id`, `contents`) VALUES
('ed3ed153-e2c9-43a3-f658-5388e34592cc', 'global', 0, '2014-05-30 20:01:02', '2014-05-31 09:03:34', '1', 'YToyNzp7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MzY6ImVkMDA1M2Y1LTYyYjUtMjVjNi0yNzljLTUzODhlMzYyMzhkZSI7czoxMDoidXNlcl90aGVtZSI7czo2OiJTdWl0ZTciO3M6MTM6InJlbWluZGVyX3RpbWUiO2k6MTgwMDtzOjEyOiJtYWlsbWVyZ2Vfb24iO3M6Mjoib24iO3M6ODoidGltZXpvbmUiO3M6MTI6IkV1cm9wZS9QYXJpcyI7czoxNjoic3dhcF9sYXN0X3ZpZXdlZCI7czowOiIiO3M6MTQ6InN3YXBfc2hvcnRjdXRzIjtzOjA6IiI7czoxOToibmF2aWdhdGlvbl9wYXJhZGlnbSI7czoyOiJnbSI7czoxMzoic3VicGFuZWxfdGFicyI7czowOiIiO3M6MTQ6Im1vZHVsZV9mYXZpY29uIjtzOjA6IiI7czo5OiJoaWRlX3RhYnMiO2E6MDp7fXM6MTE6InJlbW92ZV90YWJzIjthOjA6e31zOjc6Im5vX29wcHMiO3M6Mzoib2ZmIjtzOjE5OiJlbWFpbF9yZW1pbmRlcl90aW1lIjtpOi0xO3M6MjoidXQiO3M6MToiMSI7czo4OiJjdXJyZW5jeSI7czozOiItOTkiO3M6MzU6ImRlZmF1bHRfY3VycmVuY3lfc2lnbmlmaWNhbnRfZGlnaXRzIjtzOjE6IjIiO3M6MTE6Im51bV9ncnBfc2VwIjtzOjE6IiwiO3M6NzoiZGVjX3NlcCI7czoxOiIuIjtzOjU6ImRhdGVmIjtzOjU6Im0vZC9ZIjtzOjU6InRpbWVmIjtzOjQ6Img6aWEiO3M6MjY6ImRlZmF1bHRfbG9jYWxlX25hbWVfZm9ybWF0IjtzOjU6InMgZiBsIjtzOjE0OiJ1c2VfcmVhbF9uYW1lcyI7czoyOiJvbiI7czoxNzoibWFpbF9zbXRwYXV0aF9yZXEiO3M6MDoiIjtzOjEyOiJtYWlsX3NtdHBzc2wiO2k6MDtzOjE3OiJlbWFpbF9zaG93X2NvdW50cyI7aTowO3M6MTA6IkVtcGxveWVlc1EiO2E6Mzp7czo2OiJtb2R1bGUiO3M6OToiRW1wbG95ZWVzIjtzOjY6ImFjdGlvbiI7czo1OiJpbmRleCI7czo1OiJxdWVyeSI7czo0OiJ0cnVlIjt9fQ=='),
('831db33a-b8a4-f5bf-6945-5388e3135520', 'ETag', 0, '2014-05-30 20:02:44', '2014-06-06 10:30:00', '1', 'YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6NDM7fQ=='),
('4deb1823-f247-53dc-b8d2-5388e30bde0c', 'Home', 0, '2014-05-30 20:02:44', '2014-06-06 07:37:31', '1', 'YToyOntzOjg6ImRhc2hsZXRzIjthOjE1OntzOjM2OiI5MGYxODU2ZS01NTdkLTBlN2ItNmQxNy01Mzg4ZTY4Y2FiZTMiO2E6NTp7czo5OiJjbGFzc05hbWUiO3M6MTM6ImlGcmFtZURhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjQ6IkhvbWUiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjUzOiJtb2R1bGVzL0hvbWUvRGFzaGxldHMvaUZyYW1lRGFzaGxldC9pRnJhbWVEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjM6e3M6MTA6InRpdGxlTGFiZWwiO3M6MTc6IkRpc2NvdmVyIFN1aXRlQ1JNIjtzOjM6InVybCI7czoyMzoiaHR0cDovL3d3dy5zdWl0ZWNybS5jb20iO3M6NjoiaGVpZ2h0IjtpOjMxNTt9fXM6MzY6IjkxMzAwNjI1LTUyNjYtNjc1NC04NzVjLTUzODhlNjRlYWU2ZCI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjkxZWI4M2JjLThmZWYtOGUyNS0wOTZjLTUzODhlNmFiOWQ4YiI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjkyZTU4MDAzLTJiMWYtYmNhYy1mZGMxLTUzODhlNmY5Mjc3YSI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjkzYTExNDhkLTc5MTItN2Q4Yi0yMDU3LTUzODhlNmUxMzVhYyI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiOTQ5YjFiMGUtMDg4ZC0yNDBhLTc1MWQtNTM4OGU2ZjhhNDQ3IjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiOTU5NTEzYmMtNmQyNS1mMDI5LTJkMjYtNTM4OGU2MTBlODBmIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMzRiOWFkMmMtMzcyYS03YWE1LTdiZWUtNTM4YWU2YzliMTJkIjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjIwOiJwcm92Ml9BemllbmRlRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6InByb3YyX0F6aWVuZGUiO3M6Nzoib3B0aW9ucyI7YTowOnt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjc2OiJtb2R1bGVzL3Byb3YyX0F6aWVuZGUvRGFzaGxldHMvcHJvdjJfQXppZW5kZURhc2hsZXQvcHJvdjJfQXppZW5kZURhc2hsZXQucGhwIjt9czozNjoiOTRmYjJkM2EtYTRlNy03OGQxLWM1YjctNTM4YWU5NzkyOTQ4IjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjE2OiJwcm92Ml9hejFEYXNobGV0IjtzOjY6Im1vZHVsZSI7czo5OiJwcm92Ml9hejEiO3M6Nzoib3B0aW9ucyI7YTowOnt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY0OiJtb2R1bGVzL3Byb3YyX2F6MS9EYXNobGV0cy9wcm92Ml9hejFEYXNobGV0L3Byb3YyX2F6MURhc2hsZXQucGhwIjt9czozNjoiNjNlZjFjZDYtYjZkNC02MmJiLWQ3MzQtNTM4YWU5ZjAzOTU0IjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJwcm92Ml9kaXAxRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTA6InByb3YyX2RpcDEiO3M6Nzoib3B0aW9ucyI7YTowOnt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY3OiJtb2R1bGVzL3Byb3YyX2RpcDEvRGFzaGxldHMvcHJvdjJfZGlwMURhc2hsZXQvcHJvdjJfZGlwMURhc2hsZXQucGhwIjt9czozNjoiZjNlNWIyODMtMzM4My03NTFjLTY4MmEtNTM4ZmExMTAzYzU5IjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjIyOiJwcm92MV9DX0F6aWVuZGFEYXNobGV0IjtzOjY6Im1vZHVsZSI7czoxNToicHJvdjFfQ19BemllbmRhIjtzOjc6Im9wdGlvbnMiO2E6Njp7czo3OiJmaWx0ZXJzIjthOjA6e31zOjU6InRpdGxlIjtzOjEzOiJNaW8gQ19BemllbmRhIjtzOjExOiJteUl0ZW1zT25seSI7YjowO3M6MTE6ImRpc3BsYXlSb3dzIjtzOjE6IjUiO3M6MTQ6ImRpc3BsYXlDb2x1bW5zIjthOjQ6e2k6MDtzOjQ6Im5hbWUiO2k6MTtzOjU6ImNpdHRhIjtpOjI7czo5OiJwcm92aW5jaWEiO2k6MztzOjU6InN0YXRvIjt9czoxMToiYXV0b1JlZnJlc2giO3M6MjoiLTEiO31zOjEyOiJmaWxlTG9jYXRpb24iO3M6ODI6Im1vZHVsZXMvcHJvdjFfQ19BemllbmRhL0Rhc2hsZXRzL3Byb3YxX0NfQXppZW5kYURhc2hsZXQvcHJvdjFfQ19BemllbmRhRGFzaGxldC5waHAiO31zOjM2OiJkMTdmZGI0My0zYWU5LTIyYjUtYzJlMC01MzhmYTEwZWM4OTciO2E6NTp7czo5OiJjbGFzc05hbWUiO3M6MjU6InByb3YxX0NfRGlwZW5kZW50aURhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjE4OiJwcm92MV9DX0RpcGVuZGVudGkiO3M6Nzoib3B0aW9ucyI7YTo2OntzOjc6ImZpbHRlcnMiO2E6MDp7fXM6NToidGl0bGUiO3M6MTY6Ik1pbyBDX0RpcGVuZGVudGkiO3M6MTE6Im15SXRlbXNPbmx5IjtiOjA7czoxMToiZGlzcGxheVJvd3MiO3M6MToiNSI7czoxNDoiZGlzcGxheUNvbHVtbnMiO2E6Mzp7aTowO3M6NDoibmFtZSI7aToxO3M6NzoiY29nbm9tZSI7aToyO3M6Mzk6InByb3YxX2NfYXppZW5kYV9wcm92MV9jX2RpcGVuZGVudGlfbmFtZSI7fXM6MTE6ImF1dG9SZWZyZXNoIjtzOjI6Ii0xIjt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjkxOiJtb2R1bGVzL3Byb3YxX0NfRGlwZW5kZW50aS9EYXNobGV0cy9wcm92MV9DX0RpcGVuZGVudGlEYXNobGV0L3Byb3YxX0NfRGlwZW5kZW50aURhc2hsZXQucGhwIjtzOjEyOiJzb3J0X29wdGlvbnMiO2E6Mjp7czo5OiJzb3J0T3JkZXIiO3M6NDoiZGVzYyI7czo3OiJvcmRlckJ5IjtzOjM5OiJwcm92MV9jX2F6aWVuZGFfcHJvdjFfY19kaXBlbmRlbnRpX25hbWUiO319czozNjoiM2Q2NzQyY2QtZTkzNS0xMWYzLTU5M2MtNTM5MDE2NDI2MzIwIjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjIyOiJwcm92Ml9GX0NsaWVudGVEYXNobGV0IjtzOjY6Im1vZHVsZSI7czoxNToicHJvdjJfRl9DbGllbnRlIjtzOjc6Im9wdGlvbnMiO2E6Njp7czo3OiJmaWx0ZXJzIjthOjI6e3M6MTI6ImRhdGVfZW50ZXJlZCI7YTowOnt9czoxMzoiZGF0ZV9tb2RpZmllZCI7YTowOnt9fXM6NToidGl0bGUiO3M6MTM6Ik1pbyBGX0NsaWVudGUiO3M6MTE6Im15SXRlbXNPbmx5IjtiOjA7czoxMToiZGlzcGxheVJvd3MiO3M6MToiNSI7czoxNDoiZGlzcGxheUNvbHVtbnMiO2E6NTp7aTowO3M6NDoibmFtZSI7aToxO3M6NzoiY29nbm9tZSI7aToyO3M6Mzg6InByb3YyX2ZfY2xpZW50ZV9wcm92Ml9mX2NvbnRyYXR0aV9uYW1lIjtpOjM7czoxNzoic3RpcHVsYV9jb250cmF0dG8iO2k6NDtzOjE0OiJmaW5lX2NvbnRyYXR0byI7fXM6MTE6ImF1dG9SZWZyZXNoIjtzOjI6Ii0xIjt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgyOiJtb2R1bGVzL3Byb3YyX0ZfQ2xpZW50ZS9EYXNobGV0cy9wcm92Ml9GX0NsaWVudGVEYXNobGV0L3Byb3YyX0ZfQ2xpZW50ZURhc2hsZXQucGhwIjt9czozNjoiNGNhNDE1YjAtMzUzYS04ZDkyLWIyNzMtNTM5MDE2MmQ2YjExIjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjI0OiJwcm92Ml9GX0NvbnRyYXR0aURhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjE3OiJwcm92Ml9GX0NvbnRyYXR0aSI7czo3OiJvcHRpb25zIjthOjY6e3M6NzoiZmlsdGVycyI7YToyOntzOjEyOiJkYXRlX2VudGVyZWQiO2E6MDp7fXM6MTM6ImRhdGVfbW9kaWZpZWQiO2E6MDp7fX1zOjU6InRpdGxlIjtzOjE1OiJNaW8gRl9Db250cmF0dGkiO3M6MTE6Im15SXRlbXNPbmx5IjtiOjA7czoxMToiZGlzcGxheVJvd3MiO3M6MToiNSI7czoxNDoiZGlzcGxheUNvbHVtbnMiO2E6NDp7aTowO3M6NDoibmFtZSI7aToxO3M6MTk6InNlbGV6aW9uYV9jb250cmF0dG8iO2k6MjtzOjEyOiJwcmV6em9fZmlzc28iO2k6MztzOjExOiJkZXNjcml6aW9uZSI7fXM6MTE6ImF1dG9SZWZyZXNoIjtzOjI6Ii0xIjt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjg4OiJtb2R1bGVzL3Byb3YyX0ZfQ29udHJhdHRpL0Rhc2hsZXRzL3Byb3YyX0ZfQ29udHJhdHRpRGFzaGxldC9wcm92Ml9GX0NvbnRyYXR0aURhc2hsZXQucGhwIjt9czozNjoiY2E0ZWExMDItMzViNC1hMWE1LTU4OTUtNTM5MDE3ZGRlNTkyIjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjMzOiJwcm92Ml9GX1BvdGVuemlhbGVfQ2xpZW50ZURhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjI2OiJwcm92Ml9GX1BvdGVuemlhbGVfQ2xpZW50ZSI7czo3OiJvcHRpb25zIjthOjY6e3M6NzoiZmlsdGVycyI7YToyOntzOjEyOiJkYXRlX2VudGVyZWQiO2E6MDp7fXM6MTM6ImRhdGVfbW9kaWZpZWQiO2E6MDp7fX1zOjU6InRpdGxlIjtzOjI0OiJNaW8gRl9Qb3RlbnppYWxlIENsaWVudGUiO3M6MTE6Im15SXRlbXNPbmx5IjtiOjA7czoxMToiZGlzcGxheVJvd3MiO3M6MToiNSI7czoxNDoiZGlzcGxheUNvbHVtbnMiO2E6NDp7aTowO3M6NDoibmFtZSI7aToxO3M6NzoiY29nbm9tZSI7aToyO3M6MTQ6Im51bWVyb19wYXRlbnRlIjtpOjM7czo4OiJ0ZWxlZm9ubyI7fXM6MTE6ImF1dG9SZWZyZXNoIjtzOjI6Ii0xIjt9czoxMjoiZmlsZUxvY2F0aW9uIjtzOjExNToibW9kdWxlcy9wcm92Ml9GX1BvdGVuemlhbGVfQ2xpZW50ZS9EYXNobGV0cy9wcm92Ml9GX1BvdGVuemlhbGVfQ2xpZW50ZURhc2hsZXQvcHJvdjJfRl9Qb3RlbnppYWxlX0NsaWVudGVEYXNobGV0LnBocCI7fX1zOjU6InBhZ2VzIjthOjE6e2k6MDthOjM6e3M6NzoiY29sdW1ucyI7YToyOntpOjA7YToyOntzOjU6IndpZHRoIjtzOjM6IjYwJSI7czo4OiJkYXNobGV0cyI7YTo1OntpOjE7czozNjoiY2E0ZWExMDItMzViNC1hMWE1LTU4OTUtNTM5MDE3ZGRlNTkyIjtpOjI7czozNjoiM2Q2NzQyY2QtZTkzNS0xMWYzLTU5M2MtNTM5MDE2NDI2MzIwIjtpOjM7czozNjoiNGNhNDE1YjAtMzUzYS04ZDkyLWIyNzMtNTM5MDE2MmQ2YjExIjtpOjQ7czozNjoiZDE3ZmRiNDMtM2FlOS0yMmI1LWMyZTAtNTM4ZmExMGVjODk3IjtpOjU7czozNjoiZjNlNWIyODMtMzM4My03NTFjLTY4MmEtNTM4ZmExMTAzYzU5Ijt9fWk6MTthOjI6e3M6NToid2lkdGgiO3M6MzoiNDAlIjtzOjg6ImRhc2hsZXRzIjthOjE6e2k6MDtzOjA6IiI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIyIjtzOjE0OiJwYWdlVGl0bGVMYWJlbCI7czoyMDoiTEJMX0hPTUVfUEFHRV8xX05BTUUiO319fQ=='),
('3611f9e1-22a5-f459-f2c9-5388ec2c47b5', 'Home2_PROV1_AZIENDE', 0, '2014-05-30 20:41:02', '2014-05-30 20:41:02', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('50d920f8-3489-316b-67d5-5388e3ab86ae', 'Home2_CALL', 0, '2014-05-30 20:02:44', '2014-05-30 20:02:44', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('534a213e-591e-4a48-d404-5388e351ef61', 'Home2_MEETING', 0, '2014-05-30 20:02:44', '2014-05-30 20:02:44', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('55bb3d8a-3ae5-4ffd-cbcc-5388e35f5845', 'Home2_OPPORTUNITY', 0, '2014-05-30 20:02:44', '2014-05-30 20:02:44', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('57edb7bf-0d30-db80-9393-5388e349ca8f', 'Home2_ACCOUNT', 0, '2014-05-30 20:02:44', '2014-05-30 20:02:44', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('5a5ecb4d-1d34-ed80-8746-5388e3fef91e', 'Home2_LEAD', 0, '2014-05-30 20:02:44', '2014-05-30 20:02:44', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('5d0e58d5-18e6-c532-f821-5388e383040b', 'Home2_SUGARFEED', 0, '2014-05-30 20:02:44', '2014-05-30 20:02:44', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('9d77b0bf-abd9-9a57-6bf8-538ae62f6c69', 'Home2_PROV2_AZIENDE', 0, '2014-06-01 08:38:37', '2014-06-01 08:38:37', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('425744b9-27f7-7b8e-32eb-5388ec43f882', 'prov1_Aziende2_PROV1_AZIENDE', 0, '2014-05-30 20:41:23', '2014-05-30 20:41:23', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('bfa304b2-3b3e-8f06-8264-538aea460ed7', 'prov2_dip12_PROV2_DIP1', 0, '2014-06-01 08:53:47', '2014-06-01 08:53:47', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('a9c3184f-2e53-67dc-15cd-538ae99f9615', 'Home2_PROV2_DIP1', 0, '2014-06-01 08:51:15', '2014-06-01 08:51:15', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('529dfa6b-5d1d-ebd5-ed03-5388ed3ac8f7', 'Home2_PROV1_DIPENDENTI', 0, '2014-05-30 20:43:03', '2014-05-30 20:43:03', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('9da20197-09d6-11c9-a4f2-5388ed06a820', 'prov1_Dipendenti2_PROV1_DIPENDENTI', 0, '2014-05-30 20:43:19', '2014-05-30 20:43:19', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('43b35dab-9abb-287a-c651-538aeaf0e75f', 'prov2_az12_PROV2_AZ1', 0, '2014-06-01 08:53:51', '2014-06-01 08:53:51', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('c36df758-f4d9-a31a-b3e7-538f91b36787', 'Home2_PROV1_C_AZIENDA', 0, '2014-06-04 21:36:51', '2014-06-04 22:27:43', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czowOiIiO3M6OToic29ydE9yZGVyIjtzOjA6IiI7fX0='),
('8b031b43-2f3b-031f-891d-5389958822f5', 'Home2_PROV2_AZIENDE_A', 0, '2014-05-31 08:42:12', '2014-05-31 08:42:12', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('454fd43e-ef70-672e-79b9-538996dde7fc', 'Campaigns2_CAMPAIGN', 0, '2014-05-31 08:45:15', '2014-05-31 08:45:15', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('691a4bbc-2047-e28b-7037-53899591a271', 'Home2_PROV2_DIPENDENTI_A', 0, '2014-05-31 08:42:14', '2014-05-31 08:42:14', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('57c2c8ce-9f9c-b4ee-9f74-53899536d86e', 'prov2_Aziende_A2_PROV2_AZIENDE_A', 0, '2014-05-31 08:42:30', '2014-05-31 08:42:30', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('c998680a-c057-81e8-6b98-53899a788776', 'Employees2_EMPLOYEE', 0, '2014-05-31 09:03:34', '2014-05-31 09:03:34', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('73f0c766-f78d-aa1b-ca68-538ae68a9d82', 'prov2_Aziende2_PROV2_AZIENDE', 0, '2014-06-01 08:38:52', '2014-06-01 08:38:52', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('db4c20ea-794b-a9bd-04e0-538ae9347c4d', 'Home2_PROV2_AZ1', 0, '2014-06-01 08:51:13', '2014-06-01 08:51:13', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('2c536fe3-1fce-fb5c-4fdb-538f91d8b580', 'Home2_PROV1_C_DIPENDENTI', 0, '2014-06-04 21:36:53', '2014-06-04 22:57:22', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czozOToicHJvdjFfY19hemllbmRhX3Byb3YxX2NfZGlwZW5kZW50aV9uYW1lIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('2736e56f-3152-e4dc-f952-538f92bd0b1e', 'SecurityGroups2_SECURITYGROUP', 0, '2014-06-04 21:39:13', '2014-06-04 21:39:13', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('5b372df3-c8ef-2658-5236-538ffe2eab4e', 'Leads2_LEAD', 0, '2014-06-05 05:22:49', '2014-06-05 05:22:49', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('dcf05f41-5aed-3247-2947-539013c87cd6', 'Home2_PROV2_F_CONTRATTI', 0, '2014-06-05 06:51:48', '2014-06-05 06:51:48', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('9cfd0d82-183c-00ea-216b-538f9285134d', 'Home2_AOS_CONTRACTS', 0, '2014-06-04 21:39:58', '2014-06-04 21:39:58', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('b6531fa5-2e25-7b36-706b-538f926917da', 'prov1_C_Azienda2_PROV1_C_AZIENDA', 0, '2014-06-04 21:40:09', '2014-06-04 21:40:09', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('7883142d-168d-812e-8542-538f9266e50b', 'prov1_C_Dipendenti2_PROV1_C_DIPENDENTI', 0, '2014-06-04 21:40:41', '2014-06-04 21:40:41', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('53d25892-9540-7c04-497a-539013f35aa8', 'prov2_F_Contratti2_PROV2_F_CONTRATTI', 0, '2014-06-05 06:51:55', '2014-06-05 06:51:55', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('5bb2971b-c877-3883-b530-5390135d491f', 'prov2_F_Cliente2_PROV2_F_CLIENTE', 0, '2014-06-05 06:52:17', '2014-06-05 06:52:17', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('954bbc33-3d3d-da23-fe61-539015c1378a', 'Home2_PROV2_F_CLIENTE', 0, '2014-06-05 06:59:15', '2014-06-05 06:59:15', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('833c006c-d079-de2a-e01b-539015e0d6c7', 'Home2_PROV2_F_POTENZIALE_CLIENTE', 0, '2014-06-05 06:59:16', '2014-06-05 06:59:16', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('d79578bc-59c7-348b-c586-539018e8930f', 'prov2_F_Potenziale_Cliente2_PROV2_F_POTENZIALE_CLI', 0, '2014-06-05 07:12:48', '2014-06-05 07:12:48', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('3a439587-7f84-6731-09d9-539041f5b0a4', 'Schedulers2_SCHEDULER', 0, '2014-06-05 10:08:41', '2014-06-05 10:08:41', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('7d8fc475-cb79-b64b-24d9-5390502b5c46', 'Accounts2_ACCOUNT', 0, '2014-06-05 11:12:41', '2014-06-05 11:12:41', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('5f7a21cb-575a-b3b9-14f3-539059e17385', 'Emails', 0, '2014-06-05 11:51:06', '2014-06-05 11:51:06', '1', 'YToxOntzOjExOiJmb2N1c0ZvbGRlciI7czo2MjoiYToyOntzOjQ6ImllSWQiO3M6OToidW5kZWZpbmVkIjtzOjY6ImZvbGRlciI7czo5OiJ1bmRlZmluZWQiO30iO30='),
('50220f16-3ccd-19bc-d178-5390ef48e46e', 'prov2_F_Upload2_PROV2_F_UPLOAD', 0, '2014-06-05 22:28:47', '2014-06-05 22:28:47', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('af2d5d43-764b-d4dc-bc5a-5390603a0f72', 'Home2_EMAIL', 0, '2014-06-05 12:18:37', '2014-06-05 12:18:37', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),
('ae290a47-7dce-fcfc-c35e-539060a55737', 'Home2_AOS_PRODUCTS', 0, '2014-06-05 12:20:54', '2014-06-05 12:20:54', '1', 'YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ==');

-- --------------------------------------------------------

--
-- Struttura della tabella `vcals`
--

CREATE TABLE IF NOT EXISTS `vcals` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `idx_vcal` (`type`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `versions`
--

CREATE TABLE IF NOT EXISTS `versions` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_version` varchar(255) DEFAULT NULL,
  `db_version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_version` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `versions`
--

INSERT INTO `versions` (`id`, `deleted`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `name`, `file_version`, `db_version`) VALUES
('a709056f-4aa9-4060-1ce3-5388e3230fa8', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'Chart Data Cache', '3.5.1', '3.5.1'),
('b2c10b81-f964-04cb-3a14-5388e324c9a1', 0, '2014-05-30 20:01:02', '2014-05-30 20:01:02', '1', '1', 'htaccess', '3.5.1', '3.5.1'),
('8e8a98fa-3b53-1023-9f57-539198f392cf', 0, '2014-06-06 10:30:05', '2014-06-06 10:30:05', '1', '1', 'Rebuild Relationships', '4.0.0', '4.0.0'),
('e3415474-0067-4712-f37b-539195bfcb8c', 0, '2014-06-06 10:20:10', '2014-06-06 10:20:10', '1', '1', 'Rebuild Extensions', '4.0.0', '4.0.0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
