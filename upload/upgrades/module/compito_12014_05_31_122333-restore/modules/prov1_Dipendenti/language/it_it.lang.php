<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Cancellato',
  'LBL_NAME' => 'Name',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_ADDRESS_INFORMATION' => 'Indirizzo',
  'LBL_ALT_ADDRESS' => 'Indirizzo Alternativo',
  'LBL_ALT_ADDRESS_CITY' => 'Comune Alternativo',
  'LBL_ALT_ADDRESS_COUNTRY' => 'Nazione Alternativa',
  'LBL_ALT_ADDRESS_POSTALCODE' => 'C.A.P. Alternativo',
  'LBL_ALT_ADDRESS_STATE' => 'Provincia Alternativa',
  'LBL_ALT_ADDRESS_STREET' => 'Indirizzo Alternativo',
  'LBL_ALT_ADDRESS_STREET_2' => 'Indirizzo Alternativo 2:',
  'LBL_ALT_ADDRESS_STREET_3' => 'Indirizzo Alternativo 3:',
  'LBL_ALT_STREET' => 'Altro Indirizzo',
  'LBL_ANY_EMAIL' => 'Qualsiasi Email',
  'LBL_ASSISTANT' => 'Assistente',
  'LBL_ASSISTANT_PHONE' => 'Telefono Assistente',
  'LBL_CITY' => 'Comune',
  'LBL_CONTACT_INFORMATION' => 'Informazione Contatto',
  'LBL_COUNTRY' => 'Nazione',
  'LBL_DEPARTMENT' => 'Dipartimento',
  'LBL_DO_NOT_CALL' => 'Non chiamare',
  'LBL_EMAIL_ADDRESS' => 'Indirizzo email',
  'LBL_FAX_PHONE' => 'Fax',
  'LBL_FIRST_NAME' => 'Nome',
  'LBL_HOME_PHONE' => 'Telefono',
  'LBL_LAST_NAME' => 'Last Name',
  'LBL_MOBILE_PHONE' => 'Cellulare',
  'LBL_OFFICE_PHONE' => 'Telefono Ufficio',
  'LBL_OTHER_EMAIL_ADDRESS' => 'Altra Email:',
  'LBL_OTHER_PHONE' => 'Altro Telefono',
  'LBL_PICTURE_FILE' => 'Immagine',
  'LBL_POSTALCODE' => 'C.A.P.',
  'LBL_POSTAL_CODE' => 'C.A.P.',
  'LBL_PRIMARY_ADDRESS' => 'Indirizzo Primario',
  'LBL_PRIMARY_ADDRESS_CITY' => 'Comune Primario',
  'LBL_PRIMARY_ADDRESS_COUNTRY' => 'Nazione Primaria:',
  'LBL_PRIMARY_ADDRESS_POSTALCODE' => 'C.A.P. Primario',
  'LBL_PRIMARY_ADDRESS_STATE' => 'Provincia Primaria',
  'LBL_PRIMARY_ADDRESS_STREET' => 'Indirizzo Primario',
  'LBL_PRIMARY_ADDRESS_STREET_2' => 'Indirizzo Primario 2:',
  'LBL_PRIMARY_ADDRESS_STREET_3' => 'Indirizzo Primario 3:',
  'LBL_PRIMARY_STREET' => 'Indirizzo',
  'LBL_SALUTATION' => 'Titolo',
  'LBL_STATE' => 'Provincia',
  'LBL_STREET' => 'Altro Indirizzo',
  'LBL_TITLE' => 'Titolo',
  'LBL_WORK_PHONE' => 'Telefono Ufficio',
  'LNK_IMPORT_VCARD' => 'Crea da vCard',
  'LBL_LIST_FORM_TITLE' => 'Dipendenti Lista',
  'LBL_MODULE_NAME' => 'Dipendenti',
  'LBL_MODULE_TITLE' => 'Dipendenti',
  'LBL_HOMEPAGE_TITLE' => 'Mio Dipendenti',
  'LNK_NEW_RECORD' => 'Crea Dipendenti',
  'LNK_LIST' => 'Visualizza Dipendenti',
  'LNK_IMPORT_PROV1_DIPENDENTI' => 'Importa Dipendenti',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca Dipendenti',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_PROV1_DIPENDENTI_SUBPANEL_TITLE' => 'Dipendenti',
  'LBL_NEW_FORM_TITLE' => 'Nuovo Dipendenti',
  'LBL_COGNOME' => 'Cognome',
  'LBL_VIA' => 'Via',
  'LBL_CITTA' => 'Citta',
  'LBL_CAP' => 'CAP',
  'LBL_PROVINCIA' => 'Provincia',
  'LBL_CODICE_FISCALE' => 'Codice Fiscale',
  'LBL_EDITVIEW_PANEL1' => 'Assegna',
);