<?php
$popupMeta = array (
    'moduleMain' => 'prov1_C_Azienda',
    'varName' => 'prov1_C_Azienda',
    'orderBy' => 'prov1_c_azienda.name',
    'whereClauses' => array (
  'name' => 'prov1_c_azienda.name',
  'citta' => 'prov1_c_azienda.citta',
  'provincia' => 'prov1_c_azienda.provincia',
  'partita_iva' => 'prov1_c_azienda.partita_iva',
),
    'searchInputs' => array (
  5 => 'name',
  6 => 'citta',
  7 => 'provincia',
  8 => 'partita_iva',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'name' => 'name',
  ),
  'citta' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CITTA',
    'width' => '10%',
    'name' => 'citta',
  ),
  'provincia' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PROVINCIA',
    'width' => '10%',
    'name' => 'provincia',
  ),
  'partita_iva' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PARTITA_IVA',
    'width' => '10%',
    'name' => 'partita_iva',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
    'name' => 'name',
  ),
  'CITTA' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_CITTA',
    'width' => '10%',
  ),
  'PROVINCIA' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PROVINCIA',
    'width' => '10%',
  ),
),
);
