<?php
$dashletData['prov1_C_DipendentiDashlet']['searchFields'] = array (
  'nome' => 
  array (
    'default' => '',
  ),
  'cognome' => 
  array (
    'default' => '',
  ),
);
$dashletData['prov1_C_DipendentiDashlet']['columns'] = array (
  'nome' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_NOME',
    'width' => '10%',
  ),
  'cognome' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COGNOME',
    'width' => '10%',
  ),
);
