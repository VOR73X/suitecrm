<?php
$module_name='prov1_C_Dipendenti';
$subpanel_layout = array (
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'popup_module' => 'prov1_C_Dipendenti',
    ),
  ),
  'where' => '',
  'list_fields' => 
  array (
    'nome' => 
    array (
      'type' => 'varchar',
      'default' => true,
      'vname' => 'LBL_NOME',
      'width' => '10%',
    ),
    'cognome' => 
    array (
      'type' => 'varchar',
      'default' => true,
      'vname' => 'LBL_COGNOME',
      'width' => '10%',
    ),
  ),
);