<?php
$module_name = 'prov1_C_Dipendenti';
$listViewDefs [$module_name] = 
array (
  'NOME' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_NOME',
    'width' => '10%',
  ),
  'COGNOME' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COGNOME',
    'width' => '10%',
  ),
);
?>
