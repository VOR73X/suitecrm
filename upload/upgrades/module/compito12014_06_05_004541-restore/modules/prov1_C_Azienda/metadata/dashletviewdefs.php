<?php
$dashletData['prov1_C_AziendaDashlet']['searchFields'] = array (
  'name' => 
  array (
    'default' => '',
  ),
);
$dashletData['prov1_C_AziendaDashlet']['columns'] = array (
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
);
