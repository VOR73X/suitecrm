<?php
$popupMeta = array (
    'moduleMain' => 'prov1_C_Azienda',
    'varName' => 'prov1_C_Azienda',
    'orderBy' => 'prov1_c_azienda.name',
    'whereClauses' => array (
  'name' => 'prov1_c_azienda.name',
),
    'searchInputs' => array (
  5 => 'name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'name' => 'name',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
),
);
