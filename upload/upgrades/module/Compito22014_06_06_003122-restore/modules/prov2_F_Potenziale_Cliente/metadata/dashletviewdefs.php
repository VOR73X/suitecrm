<?php
$dashletData['prov2_F_Potenziale_ClienteDashlet']['searchFields'] = array (
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'default' => 'Jean Paul Sanchez',
  ),
);
$dashletData['prov2_F_Potenziale_ClienteDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'cognome' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COGNOME',
    'width' => '10%',
  ),
  'numero_patente' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_NUMERO_PATENTE',
    'width' => '10%',
  ),
  'telefono' => 
  array (
    'type' => 'phone',
    'default' => true,
    'label' => 'LBL_TELEFONO',
    'width' => '10%',
  ),
);
