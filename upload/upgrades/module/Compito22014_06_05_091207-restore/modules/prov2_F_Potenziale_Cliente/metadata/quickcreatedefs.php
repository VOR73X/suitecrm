<?php
$module_name = 'prov2_F_Potenziale_Cliente';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'cognome',
            'label' => 'LBL_COGNOME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'numero_patente',
            'label' => 'LBL_NUMERO_PATENTE',
          ),
        ),
      ),
    ),
  ),
);
?>
