<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Assegnato a ID:',
  'LBL_ASSIGNED_TO_NAME' => 'Assegnato a:',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data Inserimento',
  'LBL_DATE_MODIFIED' => 'Data Modifica',
  'LBL_MODIFIED' => 'Modificato da',
  'LBL_MODIFIED_ID' => 'Modificato da Id',
  'LBL_MODIFIED_NAME' => 'Modificato da Nome',
  'LBL_CREATED' => 'Creato da',
  'LBL_CREATED_ID' => 'Creato da Id',
  'LBL_DESCRIPTION' => 'Descrizione',
  'LBL_DELETED' => 'Cancellato',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Creato da utente',
  'LBL_MODIFIED_USER' => 'Modificato da utente',
  'LBL_LIST_FORM_TITLE' => 'F_Cliente Lista',
  'LBL_MODULE_NAME' => 'F_Cliente',
  'LBL_MODULE_TITLE' => 'F_Cliente',
  'LBL_HOMEPAGE_TITLE' => 'Mio F_Cliente',
  'LNK_NEW_RECORD' => 'Crea F_Cliente',
  'LNK_LIST' => 'Visualizza F_Cliente',
  'LNK_IMPORT_PROV2_F_CLIENTE' => 'Importa F_Cliente',
  'LBL_SEARCH_FORM_TITLE' => 'Ricerca F_Cliente',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Cronologia',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Attività',
  'LBL_PROV2_F_CLIENTE_SUBPANEL_TITLE' => 'F_Cliente',
  'LBL_NEW_FORM_TITLE' => 'Nuovo F_Cliente',
  'LBL_COGNOME' => 'Cognome',
  'LBL_CODICE_FISCALE' => 'Codice Fiscale',
  'LBL_VIA' => 'Via',
  'LBL_CITTA' => 'Citta',
  'LBL_PROVINCIA' => 'Provincia',
  'LBL_STATO' => 'Stato',
  'LBL_TELEFONO' => 'Telefono',
  'LBL_EDITVIEW_PANEL1' => 'Nuovo Pannello 1',
  'LBL_DATA_NASCITA' => 'Data di Nascita',
  'LBL_STIPULA_CONTRATTO' => 'Stipula Contratto',
  'LBL_FINE_CONTRATTO' => 'Fine Contratto',
  'LBL_PREZZO_AFFITTO' => 'Prezzo Affitto',
  'LBL_CAUZIONE' => 'Cauzione',
  'LBL_PREZZO_TOTALE' => 'Prezzo Totale',
  'LBL_IVA' => 'IVA',
);