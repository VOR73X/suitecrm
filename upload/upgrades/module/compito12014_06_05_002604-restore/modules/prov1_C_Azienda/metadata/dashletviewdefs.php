<?php
$dashletData['prov1_C_AziendaDashlet']['searchFields'] = array (
  'ragione_sociale' => 
  array (
    'default' => '',
  ),
);
$dashletData['prov1_C_AziendaDashlet']['columns'] = array (
  'ragione_sociale' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_RAGIONE_SOCIALE',
    'width' => '10%',
    'default' => true,
  ),
);
