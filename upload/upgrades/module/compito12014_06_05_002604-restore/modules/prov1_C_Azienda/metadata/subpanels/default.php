<?php
$module_name='prov1_C_Azienda';
$subpanel_layout = array (
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopCreateButton',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'popup_module' => 'prov1_C_Azienda',
    ),
  ),
  'where' => '',
  'list_fields' => 
  array (
    'ragione_sociale' => 
    array (
      'type' => 'varchar',
      'vname' => 'LBL_RAGIONE_SOCIALE',
      'width' => '10%',
      'default' => true,
    ),
  ),
);