<?php
$dashletData['prov1_C_DipendentiDashlet']['searchFields'] = array (
  'nome' => 
  array (
    'default' => '',
  ),
  'cognome' => 
  array (
    'default' => '',
  ),
);
$dashletData['prov1_C_DipendentiDashlet']['columns'] = array (
  'nome' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NOME',
    'width' => '10%',
    'default' => true,
  ),
  'cognome' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COGNOME',
    'width' => '10%',
    'name' => 'cognome',
  ),
  'prov1_c_azienda_prov1_c_dipendenti_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PROV1_C_AZIENDA_PROV1_C_DIPENDENTI_FROM_PROV1_C_AZIENDA_TITLE',
    'id' => 'PROV1_C_AZIENDA_PROV1_C_DIPENDENTIPROV1_C_AZIENDA_IDA',
    'width' => '10%',
    'default' => true,
  ),
);
