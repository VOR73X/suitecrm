<?php
$module_name = 'prov2_F_Contratti';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'seleziona_contratto',
            'studio' => 'visible',
            'label' => 'LBL_SELEZIONA_CONTRATTO',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'descrizione',
            'label' => 'LBL_DESCRIZIONE',
          ),
          1 => 
          array (
            'name' => 'prezzo_fisso',
            'label' => 'LBL_PREZZO_FISSO',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'proprietario_veicolo',
            'label' => 'LBL_PROPRIETARIO_VEICOLO',
          ),
          1 => 
          array (
            'name' => 'descrizione_veicolo',
            'label' => 'LBL_DESCRIZIONE_VEICOLO',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'potenza_veicolo',
            'label' => 'LBL_POTENZA_VEICOLO',
          ),
          1 => 
          array (
            'name' => 'velocita_massima',
            'label' => 'LBL_VELOCITA_MASSIMA',
          ),
        ),
      ),
    ),
  ),
);
?>
