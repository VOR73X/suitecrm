<?php
$module_name = 'prov1_C_Azienda';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'via',
            'label' => 'LBL_VIA',
          ),
          1 => 
          array (
            'name' => 'citta',
            'label' => 'LBL_CITTA',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'provincia',
            'label' => 'LBL_PROVINCIA',
          ),
          1 => 
          array (
            'name' => 'cap',
            'label' => 'LBL_CAP',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'stato',
            'label' => 'LBL_STATO',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'partita_iva',
            'label' => 'LBL_PARTITA_IVA',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'telefono',
            'label' => 'LBL_TELEFONO',
          ),
          1 => 
          array (
            'name' => 'fax',
            'label' => 'LBL_FAX',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'cellulare',
            'label' => 'LBL_CELLULARE',
          ),
        ),
      ),
    ),
  ),
);
?>
