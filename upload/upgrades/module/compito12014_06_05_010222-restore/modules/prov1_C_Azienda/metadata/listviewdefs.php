<?php
$module_name = 'prov1_C_Azienda';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
  'PARTITA_IVA' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PARTITA_IVA',
    'width' => '10%',
  ),
  'TELEFONO' => 
  array (
    'type' => 'phone',
    'default' => true,
    'label' => 'LBL_TELEFONO',
    'width' => '10%',
  ),
);
?>
