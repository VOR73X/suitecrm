<?php
$module_name = 'prov1_C_Azienda';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'via',
            'label' => 'LBL_VIA',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'citta',
            'label' => 'LBL_CITTA',
          ),
          1 => 
          array (
            'name' => 'provincia',
            'label' => 'LBL_PROVINCIA',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'cap',
            'label' => 'LBL_CAP',
          ),
          1 => 
          array (
            'name' => 'stato',
            'label' => 'LBL_STATO',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'partita_iva',
            'label' => 'LBL_PARTITA_IVA',
          ),
          1 => 
          array (
            'name' => 'telefono',
            'label' => 'LBL_TELEFONO',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'fax',
            'label' => 'LBL_FAX',
          ),
          1 => 
          array (
            'name' => 'cellulare',
            'label' => 'LBL_CELLULARE',
          ),
        ),
      ),
    ),
  ),
);
?>
