<?php
$module_name = 'prov1_C_Azienda';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'type' => 'name',
        'link' => true,
        'label' => 'LBL_NAME',
        'width' => '10%',
        'default' => true,
        'name' => 'name',
      ),
      'citta' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CITTA',
        'width' => '10%',
        'name' => 'citta',
      ),
      'provincia' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PROVINCIA',
        'width' => '10%',
        'name' => 'provincia',
      ),
      'partita_iva' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PARTITA_IVA',
        'width' => '10%',
        'name' => 'partita_iva',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'type' => 'name',
        'link' => true,
        'label' => 'LBL_NAME',
        'width' => '10%',
        'default' => true,
        'name' => 'name',
      ),
      'citta' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CITTA',
        'width' => '10%',
        'name' => 'citta',
      ),
      'provincia' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PROVINCIA',
        'width' => '10%',
        'name' => 'provincia',
      ),
      'partita_iva' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PARTITA_IVA',
        'width' => '10%',
        'name' => 'partita_iva',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
