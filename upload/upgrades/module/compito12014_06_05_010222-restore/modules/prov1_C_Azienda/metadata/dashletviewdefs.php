<?php
$dashletData['prov1_C_AziendaDashlet']['searchFields'] = array (
  'name' => 
  array (
    'default' => '',
  ),
  'citta' => 
  array (
    'default' => '',
  ),
  'provincia' => 
  array (
    'default' => '',
  ),
  'partita_iva' => 
  array (
    'default' => '',
  ),
);
$dashletData['prov1_C_AziendaDashlet']['columns'] = array (
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
    'name' => 'name',
  ),
  'citta' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_CITTA',
    'width' => '10%',
  ),
  'provincia' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PROVINCIA',
    'width' => '10%',
  ),
  'stato' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_STATO',
    'width' => '10%',
  ),
);
