<?php
$dashletData['prov1_C_DipendentiDashlet']['searchFields'] = array (
  'nome' => 
  array (
    'default' => '',
  ),
  'cognome' => 
  array (
    'default' => '',
  ),
);
$dashletData['prov1_C_DipendentiDashlet']['columns'] = array (
  'cognome' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COGNOME',
    'width' => '10%',
    'name' => 'cognome',
  ),
);
