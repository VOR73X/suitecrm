<?php
$dashletData['prov2_F_ContrattiDashlet']['searchFields'] = array (
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'default' => 'Jean Paul Sanchez',
  ),
);
$dashletData['prov2_F_ContrattiDashlet']['columns'] = array (
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
    'name' => 'date_entered',
  ),
  'prezzo_fisso' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_PREZZO_FISSO',
    'width' => '10%',
  ),
  'descrizione' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_DESCRIZIONE',
    'width' => '10%',
  ),
);
