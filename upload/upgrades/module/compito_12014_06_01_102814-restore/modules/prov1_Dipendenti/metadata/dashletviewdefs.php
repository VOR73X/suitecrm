<?php
$dashletData['prov1_DipendentiDashlet']['searchFields'] = array (
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'default' => 'Jean Paul Sanchez',
  ),
);
$dashletData['prov1_DipendentiDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'prov1_aziende_prov1_dipendenti_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PROV1_AZIENDE_PROV1_DIPENDENTI_FROM_PROV1_AZIENDE_TITLE',
    'id' => 'PROV1_AZIENDE_PROV1_DIPENDENTIPROV1_AZIENDE_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'prov1_aziende_prov1_dipendenti_name',
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => false,
  ),
);
