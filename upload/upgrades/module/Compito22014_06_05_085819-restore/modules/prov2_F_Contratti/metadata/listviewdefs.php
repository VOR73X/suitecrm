<?php
$module_name = 'prov2_F_Contratti';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'PROPRIETARIO_VEICOLO' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PROPRIETARIO_VEICOLO',
    'width' => '10%',
  ),
  'PREZZO_FISSO' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_PREZZO_FISSO',
    'width' => '10%',
  ),
  'POTENZA_VEICOLO' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_POTENZA_VEICOLO',
    'width' => '10%',
  ),
  'VELOCITA_MASSIMA' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_VELOCITA_MASSIMA',
    'width' => '10%',
  ),
);
?>
