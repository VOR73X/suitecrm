<?php
$module_name = 'prov2_F_Potenziale_Cliente';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'COGNOME' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COGNOME',
    'width' => '10%',
  ),
  'CITTA' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_CITTA',
    'width' => '10%',
  ),
  'PROVINCIA' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PROVINCIA',
    'width' => '10%',
  ),
  'DATA_NASCITA' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DATA_NASCITA',
    'width' => '10%',
    'default' => true,
  ),
);
?>
