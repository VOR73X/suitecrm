<?php
$module_name = 'prov1_Aziende';
$_object_name = 'prov1_aziende';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Accounts/Account.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_ACCOUNT_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_account_information' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'via',
            'label' => 'LBL_VIA',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'citta',
            'label' => 'LBL_CITTA',
          ),
          1 => 
          array (
            'name' => 'provincia',
            'label' => 'LBL_PROVINCIA',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'cap',
            'label' => 'LBL_CAP',
          ),
          1 => 
          array (
            'name' => 'stato',
            'label' => 'LBL_STATO',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'partita_iva',
            'label' => 'LBL_PARTITA_IVA',
          ),
          1 => 
          array (
            'name' => 'telefono',
            'label' => 'LBL_TELEFONO',
          ),
        ),
        4 => 
        array (
          1 => 
          array (
            'name' => 'cellulare',
            'label' => 'LBL_CELLULARE',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
?>
