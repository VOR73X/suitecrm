<?php
$module_name = 'prov1_Dipendenti';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_CONTACT_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_contact_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 => 
          array (
            'name' => 'cognome',
            'label' => 'LBL_COGNOME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'via',
            'label' => 'LBL_VIA',
          ),
          1 => 
          array (
            'name' => 'citta',
            'label' => 'LBL_CITTA',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'provincia',
            'label' => 'LBL_PROVINCIA',
          ),
          1 => 
          array (
            'name' => 'cap',
            'label' => 'LBL_CAP',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'codice_fiscale',
            'label' => 'LBL_CODICE_FISCALE',
          ),
          1 => 
          array (
            'name' => 'prov1_aziende_prov1_dipendenti_name',
            'label' => 'LBL_PROV1_AZIENDE_PROV1_DIPENDENTI_FROM_PROV1_AZIENDE_TITLE',
          ),
        ),
      ),
    ),
  ),
);
?>
