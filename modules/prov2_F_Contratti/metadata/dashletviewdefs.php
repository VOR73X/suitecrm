<?php
$dashletData['prov2_F_ContrattiDashlet']['searchFields'] = array (
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'default' => 'Jean Paul Sanchez',
  ),
);
$dashletData['prov2_F_ContrattiDashlet']['columns'] = array (
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
  'seleziona_contratto' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_SELEZIONA_CONTRATTO',
    'width' => '10%',
  ),
  'prezzo_fisso' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_PREZZO_FISSO',
    'width' => '10%',
    'name' => 'prezzo_fisso',
  ),
  'descrizione' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_DESCRIZIONE',
    'width' => '10%',
    'name' => 'descrizione',
  ),
);
