<?php
$dashletData['prov2_F_ClienteDashlet']['searchFields'] = array (
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'default' => 'Jean Paul Sanchez',
  ),
);
$dashletData['prov2_F_ClienteDashlet']['columns'] = array (
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'cognome' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COGNOME',
    'width' => '10%',
    'name' => 'cognome',
  ),
  'prov2_f_cliente_prov2_f_contratti_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PROV2_F_CLIENTE_PROV2_F_CONTRATTI_FROM_PROV2_F_CONTRATTI_TITLE',
    'id' => 'PROV2_F_CLIENTE_PROV2_F_CONTRATTIPROV2_F_CONTRATTI_IDB',
    'width' => '10%',
    'default' => true,
    'name' => 'prov2_f_cliente_prov2_f_contratti_name',
  ),
  'stipula_contratto' => 
  array (
    'type' => 'date',
    'label' => 'LBL_STIPULA_CONTRATTO',
    'width' => '10%',
    'default' => true,
    'name' => 'stipula_contratto',
  ),
  'fine_contratto' => 
  array (
    'type' => 'date',
    'label' => 'LBL_FINE_CONTRATTO',
    'width' => '10%',
    'default' => true,
    'name' => 'fine_contratto',
  ),
  'prezzo_totale' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PREZZO_TOTALE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
);
