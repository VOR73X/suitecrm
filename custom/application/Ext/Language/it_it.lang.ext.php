<?php 
 //WARNING: The contents of this file are auto-generated

 
/**
 * Products, Quotations & Invoices modules.
 * Extensions to SugarCRM
 * @package Advanced OpenSales for SugarCRM
 * @subpackage Products
 * @copyright SalesAgility Ltd http://www.salesagility.com
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 * along with this program; if not, see http://www.gnu.org/licenses
 * or write to the Free Software Foundation,Inc., 51 Franklin Street,
 * Fifth Floor, Boston, MA 02110-1301  USA
 *
 * @author Salesagility Ltd <support@salesagility.com>
 */

$app_list_strings['moduleList']['AOS_Contracts'] = 'Contratti';
$app_list_strings['moduleList']['AOS_Invoices'] = 'Fatture';
$app_list_strings['moduleList']['AOS_PDF_Templates'] = 'Modelli PDF';
$app_list_strings['moduleList']['AOS_Product_Categories'] = 'Product Categories';
$app_list_strings['moduleList']['AOS_Products'] = 'Prodotti';
$app_list_strings['moduleList']['AOS_Products_Quotes'] = 'Listini';
$app_list_strings['moduleList']['AOS_Line_Item_Groups'] = 'Line Item Groups';
$app_list_strings['moduleList']['AOS_Quotes'] = 'Preventivi';
$app_list_strings['aos_quotes_type_dom'][''] = '';
$app_list_strings['aos_quotes_type_dom']['Analyst'] = 'Analista';
$app_list_strings['aos_quotes_type_dom']['Competitor'] = 'Concorrente';
$app_list_strings['aos_quotes_type_dom']['Customer'] = 'Cliente';
$app_list_strings['aos_quotes_type_dom']['Integrator'] = 'Integratore';
$app_list_strings['aos_quotes_type_dom']['Investor'] = 'Investitore';
$app_list_strings['aos_quotes_type_dom']['Partner'] = 'Partner';
$app_list_strings['aos_quotes_type_dom']['Press'] = 'Stampa';
$app_list_strings['aos_quotes_type_dom']['Prospect'] = 'Prospetto';
$app_list_strings['aos_quotes_type_dom']['Reseller'] = 'Rivenditore';
$app_list_strings['aos_quotes_type_dom']['Other'] = 'Altro';
$app_list_strings['template_ddown_c_list'][''] = '';
$app_list_strings['quote_stage_dom']['Draft'] = 'Bozza';
$app_list_strings['quote_stage_dom']['Negotiation'] = 'In trattitiva';
$app_list_strings['quote_stage_dom']['Delivered'] = 'Consegnato';
$app_list_strings['quote_stage_dom']['On Hold'] = 'In Attesa';
$app_list_strings['quote_stage_dom']['Confirmed'] = 'Confermato';
$app_list_strings['quote_stage_dom']['Closed Accepted'] = 'Chiuso - Accettato';
$app_list_strings['quote_stage_dom']['Closed Lost'] = 'Chiuso - Perso';
$app_list_strings['quote_stage_dom']['Closed Dead'] = 'Chiuso - Estinto';
$app_list_strings['quote_term_dom']['Net 15'] = 'Net 15';
$app_list_strings['quote_term_dom']['Net 30'] = 'Net 30';
$app_list_strings['quote_term_dom'][''] = '';
$app_list_strings['approval_status_dom']['Approved'] = 'Approvato';
$app_list_strings['approval_status_dom']['Not Approved'] = 'Non Approvato';
$app_list_strings['approval_status_dom'][''] = '';
$app_list_strings['vat_list']['20.0'] = '20.0';
$app_list_strings['vat_list']['10.0'] = '10.0';
$app_list_strings['discount_list']['Percentage'] = 'Percentuale';
$app_list_strings['discount_list']['Amount'] = 'Importo';
$app_list_strings['aos_invoices_type_dom'][''] = '';
$app_list_strings['aos_invoices_type_dom']['Analyst'] = 'Analista';
$app_list_strings['aos_invoices_type_dom']['Competitor'] = 'Concorrente';
$app_list_strings['aos_invoices_type_dom']['Customer'] = 'Cliente';
$app_list_strings['aos_invoices_type_dom']['Integrator'] = 'Integratore';
$app_list_strings['aos_invoices_type_dom']['Investor'] = 'Investitore';
$app_list_strings['aos_invoices_type_dom']['Partner'] = 'Partner';
$app_list_strings['aos_invoices_type_dom']['Press'] = 'Stampa';
$app_list_strings['aos_invoices_type_dom']['Prospect'] = 'Prospetto';
$app_list_strings['aos_invoices_type_dom']['Reseller'] = 'Rivneditore';
$app_list_strings['aos_invoices_type_dom']['Other'] = 'Altro';
$app_list_strings['invoice_status_dom']['Paid'] = 'Pagato';
$app_list_strings['invoice_status_dom']['Unpaid'] = 'Non pagato';
$app_list_strings['invoice_status_dom']['Cancelled'] = 'Cancellato';
$app_list_strings['invoice_status_dom'][''] = '';
$app_list_strings['quote_invoice_status_dom']['Not Invoiced'] = 'Non Fatturato';
$app_list_strings['quote_invoice_status_dom']['Invoiced'] = 'Fatturato';
$app_list_strings['product_code_dom']['XXXX'] = 'XXXX';
$app_list_strings['product_code_dom']['YYYY'] = 'YYYY';
$app_list_strings['product_category_dom']['Laptops'] = 'Laptops';
$app_list_strings['product_category_dom']['Desktops'] = 'Desktops';
$app_list_strings['product_category_dom'][''] = '';
$app_list_strings['product_type_dom']['Good'] = 'Prodotto';
$app_list_strings['product_type_dom']['Service'] = 'Servizio';
$app_list_strings['product_quote_parent_type_dom']['AOS_Quotes'] = 'Preventivi';
$app_list_strings['product_quote_parent_type_dom']['AOS_Invoices'] = 'Fatture';
$app_list_strings['pdf_template_type_dom']['AOS_Quotes'] = 'Preventivi';
$app_list_strings['pdf_template_type_dom']['AOS_Invoices'] = 'Fatture';
$app_list_strings['pdf_template_type_dom']['Accounts'] = 'Aziende';
$app_list_strings['pdf_template_type_dom']['Contacts'] = 'Contatti';
$app_list_strings['pdf_template_type_dom']['Leads'] = 'Leads';
$app_list_strings['contract_status_list']['Not Started'] = 'Non Iniziato';
$app_list_strings['contract_status_list']['In Progress'] = 'In Corso';
$app_list_strings['contract_status_list']['Signed'] = 'Approvato';
$app_list_strings['contract_type_list']['Type'] = 'Tipologia';
$app_strings['LBL_GENERATE_LETTER'] = 'Genera Lettera';
$app_strings['LBL_SELECT_TEMPLATE'] = 'Seleziona un modello';
$app_strings['LBL_NO_TEMPLATE'] = 'ERRORE\nNessun modello trovato.\nCrea un nuovo modello attraverso il modulo Modelli PDF';



/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['prov1_C_Azienda'] = 'C_Azienda';
$app_list_strings['moduleList']['prov1_C_Dipendenti'] = 'C_Dipendenti';


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['prov2_F_Contratti'] = 'F_Contratti';
$app_list_strings['moduleList']['prov2_F_Upload'] = 'F_Upload';
$app_list_strings['moduleList']['prov2_Potenziale_Cliente'] = 'F_Potenziale Cliente';
$app_list_strings['moduleList']['prov2_F_Potenziale_Cliente'] = 'F_Potenziale Cliente';
$app_list_strings['moduleList']['prov2_F_Cliente'] = 'F_Cliente';
$app_list_strings['contratti_list']['Giornaliero'] = 'Giornaliero';
$app_list_strings['contratti_list']['Mensile'] = 'Mensile';
$app_list_strings['contratti_list']['Annuale'] = 'Annuale';
$app_list_strings['prov2_f_upload_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['prov2_f_upload_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['prov2_f_upload_category_dom'][''] = ' ';
$app_list_strings['prov2_f_upload_category_dom']['Sales'] = 'Vendite';
$app_list_strings['prov2_f_upload_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['prov2_f_upload_subcategory_dom'][''] = ' ';
$app_list_strings['prov2_f_upload_subcategory_dom']['Marketing Collateral'] = 'Materiale di Marketing';
$app_list_strings['prov2_f_upload_subcategory_dom']['Product Brochures'] = 'Brochure Prodotto';
$app_list_strings['prov2_f_upload_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['prov2_f_upload_status_dom']['Active'] = 'Attivo';
$app_list_strings['prov2_f_upload_status_dom']['Draft'] = 'Bozza';
$app_list_strings['prov2_f_upload_status_dom']['Expired'] = 'Scaduto';
$app_list_strings['prov2_f_upload_status_dom']['Under Review'] = 'In Revisione';
$app_list_strings['prov2_f_upload_status_dom']['Pending'] = 'In attesa';
$app_list_strings['_category_dom'][''] = '';
$app_list_strings['_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['_category_dom']['Knowledege Base'] = 'Base de Conocimiento';
$app_list_strings['_category_dom']['Sales'] = 'Ventas';
$app_list_strings['_subcategory_dom'][''] = '';
$app_list_strings['_subcategory_dom']['Marketing Collateral'] = 'Impresos de Marketing';
$app_list_strings['_subcategory_dom']['Product Brochures'] = 'Folletos de Producto';
$app_list_strings['_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['_status_dom']['Active'] = 'Activo';
$app_list_strings['_status_dom']['Draft'] = 'Borrador';
$app_list_strings['_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['_status_dom']['Expired'] = 'Caducado';
$app_list_strings['_status_dom']['Under Review'] = 'En Revisión';
$app_list_strings['_status_dom']['Pending'] = 'Pendiente';


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['prov1_Aziende'] = 'Aziende';
$app_list_strings['moduleList']['prov1_Dipendenti'] = 'Dipendenti';
$app_list_strings['prov1_aziende_type_dom'][''] = '';
$app_list_strings['prov1_aziende_type_dom']['Analyst'] = 'Analyst';
$app_list_strings['prov1_aziende_type_dom']['Competitor'] = 'Competitor';
$app_list_strings['prov1_aziende_type_dom']['Customer'] = 'Customer';
$app_list_strings['prov1_aziende_type_dom']['Integrator'] = 'Integrator';
$app_list_strings['prov1_aziende_type_dom']['Investor'] = 'Investor';
$app_list_strings['prov1_aziende_type_dom']['Partner'] = 'Partner';
$app_list_strings['prov1_aziende_type_dom']['Press'] = 'Press';
$app_list_strings['prov1_aziende_type_dom']['Prospect'] = 'Prospect';
$app_list_strings['prov1_aziende_type_dom']['Reseller'] = 'Reseller';
$app_list_strings['prov1_aziende_type_dom']['Other'] = 'Other';
$app_list_strings['_type_dom'][''] = '';
$app_list_strings['_type_dom']['Analyst'] = 'Analista';
$app_list_strings['_type_dom']['Competitor'] = 'Competidor';
$app_list_strings['_type_dom']['Customer'] = 'Cliente';
$app_list_strings['_type_dom']['Integrator'] = 'Integrador';
$app_list_strings['_type_dom']['Investor'] = 'Inversor';
$app_list_strings['_type_dom']['Partner'] = 'Partner';
$app_list_strings['_type_dom']['Press'] = 'Prensa';
$app_list_strings['_type_dom']['Prospect'] = 'Prospecto';
$app_list_strings['_type_dom']['Reseller'] = 'Revendedor';
$app_list_strings['_type_dom']['Other'] = 'Otro';



$app_list_strings["moduleList"]["SecurityGroups"] = 'Gestione Gruppi di Sicurezza';
$app_strings['LBL_LOGIN_AS'] = "Inizio attivit� come ";
$app_strings['LBL_LOGOUT_AS'] = "Termine attivit� come ";


?>