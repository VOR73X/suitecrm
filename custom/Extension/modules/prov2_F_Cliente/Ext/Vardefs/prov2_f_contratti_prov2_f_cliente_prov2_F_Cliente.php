<?php
// created: 2014-06-05 08:51:11
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_contratti_prov2_f_cliente"] = array (
  'name' => 'prov2_f_contratti_prov2_f_cliente',
  'type' => 'link',
  'relationship' => 'prov2_f_contratti_prov2_f_cliente',
  'source' => 'non-db',
  'module' => 'prov2_F_Contratti',
  'bean_name' => false,
  'vname' => 'LBL_PROV2_F_CONTRATTI_PROV2_F_CLIENTE_FROM_PROV2_F_CONTRATTI_TITLE',
  'id_name' => 'prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_contratti_prov2_f_cliente_name"] = array (
  'name' => 'prov2_f_contratti_prov2_f_cliente_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV2_F_CONTRATTI_PROV2_F_CLIENTE_FROM_PROV2_F_CONTRATTI_TITLE',
  'save' => true,
  'id_name' => 'prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida',
  'link' => 'prov2_f_contratti_prov2_f_cliente',
  'table' => 'prov2_f_contratti',
  'module' => 'prov2_F_Contratti',
  'rname' => 'name',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida"] = array (
  'name' => 'prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida',
  'type' => 'link',
  'relationship' => 'prov2_f_contratti_prov2_f_cliente',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_PROV2_F_CONTRATTI_PROV2_F_CLIENTE_FROM_PROV2_F_CONTRATTI_TITLE',
);
