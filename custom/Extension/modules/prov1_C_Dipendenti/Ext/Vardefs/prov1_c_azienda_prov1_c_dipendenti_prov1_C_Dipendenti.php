<?php
// created: 2014-06-05 01:02:22
$dictionary["prov1_C_Dipendenti"]["fields"]["prov1_c_azienda_prov1_c_dipendenti"] = array (
  'name' => 'prov1_c_azienda_prov1_c_dipendenti',
  'type' => 'link',
  'relationship' => 'prov1_c_azienda_prov1_c_dipendenti',
  'source' => 'non-db',
  'module' => 'prov1_C_Azienda',
  'bean_name' => 'prov1_C_Azienda',
  'vname' => 'LBL_PROV1_C_AZIENDA_PROV1_C_DIPENDENTI_FROM_PROV1_C_AZIENDA_TITLE',
  'id_name' => 'prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida',
);
$dictionary["prov1_C_Dipendenti"]["fields"]["prov1_c_azienda_prov1_c_dipendenti_name"] = array (
  'name' => 'prov1_c_azienda_prov1_c_dipendenti_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV1_C_AZIENDA_PROV1_C_DIPENDENTI_FROM_PROV1_C_AZIENDA_TITLE',
  'save' => true,
  'id_name' => 'prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida',
  'link' => 'prov1_c_azienda_prov1_c_dipendenti',
  'table' => 'prov1_c_azienda',
  'module' => 'prov1_C_Azienda',
  'rname' => 'name',
);
$dictionary["prov1_C_Dipendenti"]["fields"]["prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida"] = array (
  'name' => 'prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida',
  'type' => 'link',
  'relationship' => 'prov1_c_azienda_prov1_c_dipendenti',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_PROV1_C_AZIENDA_PROV1_C_DIPENDENTI_FROM_PROV1_C_DIPENDENTI_TITLE',
);
