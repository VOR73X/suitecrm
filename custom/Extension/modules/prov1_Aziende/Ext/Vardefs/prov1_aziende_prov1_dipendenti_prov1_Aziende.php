<?php
// created: 2014-06-01 10:28:14
$dictionary["prov1_Aziende"]["fields"]["prov1_aziende_prov1_dipendenti"] = array (
  'name' => 'prov1_aziende_prov1_dipendenti',
  'type' => 'link',
  'relationship' => 'prov1_aziende_prov1_dipendenti',
  'source' => 'non-db',
  'module' => 'prov1_Dipendenti',
  'bean_name' => 'prov1_Dipendenti',
  'side' => 'right',
  'vname' => 'LBL_PROV1_AZIENDE_PROV1_DIPENDENTI_FROM_PROV1_DIPENDENTI_TITLE',
);
