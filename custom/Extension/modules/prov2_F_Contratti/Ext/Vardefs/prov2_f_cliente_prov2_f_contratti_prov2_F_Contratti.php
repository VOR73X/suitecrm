<?php
// created: 2014-06-06 12:29:34
$dictionary["prov2_F_Contratti"]["fields"]["prov2_f_cliente_prov2_f_contratti"] = array (
  'name' => 'prov2_f_cliente_prov2_f_contratti',
  'type' => 'link',
  'relationship' => 'prov2_f_cliente_prov2_f_contratti',
  'source' => 'non-db',
  'module' => 'prov2_F_Cliente',
  'bean_name' => 'prov2_F_Cliente',
  'vname' => 'LBL_PROV2_F_CLIENTE_PROV2_F_CONTRATTI_FROM_PROV2_F_CLIENTE_TITLE',
  'id_name' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida',
);
$dictionary["prov2_F_Contratti"]["fields"]["prov2_f_cliente_prov2_f_contratti_name"] = array (
  'name' => 'prov2_f_cliente_prov2_f_contratti_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV2_F_CLIENTE_PROV2_F_CONTRATTI_FROM_PROV2_F_CLIENTE_TITLE',
  'save' => true,
  'id_name' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida',
  'link' => 'prov2_f_cliente_prov2_f_contratti',
  'table' => 'prov2_f_cliente',
  'module' => 'prov2_F_Cliente',
  'rname' => 'name',
);
$dictionary["prov2_F_Contratti"]["fields"]["prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida"] = array (
  'name' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida',
  'type' => 'link',
  'relationship' => 'prov2_f_cliente_prov2_f_contratti',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_PROV2_F_CLIENTE_PROV2_F_CONTRATTI_FROM_PROV2_F_CLIENTE_TITLE',
);
