<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-06-06 12:29:34
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_cliente_prov2_f_contratti"] = array (
  'name' => 'prov2_f_cliente_prov2_f_contratti',
  'type' => 'link',
  'relationship' => 'prov2_f_cliente_prov2_f_contratti',
  'source' => 'non-db',
  'module' => 'prov2_F_Contratti',
  'bean_name' => 'prov2_F_Contratti',
  'vname' => 'LBL_PROV2_F_CLIENTE_PROV2_F_CONTRATTI_FROM_PROV2_F_CONTRATTI_TITLE',
  'id_name' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_cliente_prov2_f_contratti_name"] = array (
  'name' => 'prov2_f_cliente_prov2_f_contratti_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV2_F_CLIENTE_PROV2_F_CONTRATTI_FROM_PROV2_F_CONTRATTI_TITLE',
  'save' => true,
  'id_name' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb',
  'link' => 'prov2_f_cliente_prov2_f_contratti',
  'table' => 'prov2_f_contratti',
  'module' => 'prov2_F_Contratti',
  'rname' => 'name',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb"] = array (
  'name' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb',
  'type' => 'link',
  'relationship' => 'prov2_f_cliente_prov2_f_contratti',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_PROV2_F_CLIENTE_PROV2_F_CONTRATTI_FROM_PROV2_F_CONTRATTI_TITLE',
);


// created: 2014-06-05 08:51:11
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_contratti_prov2_f_cliente"] = array (
  'name' => 'prov2_f_contratti_prov2_f_cliente',
  'type' => 'link',
  'relationship' => 'prov2_f_contratti_prov2_f_cliente',
  'source' => 'non-db',
  'module' => 'prov2_F_Contratti',
  'bean_name' => false,
  'vname' => 'LBL_PROV2_F_CONTRATTI_PROV2_F_CLIENTE_FROM_PROV2_F_CONTRATTI_TITLE',
  'id_name' => 'prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_contratti_prov2_f_cliente_name"] = array (
  'name' => 'prov2_f_contratti_prov2_f_cliente_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV2_F_CONTRATTI_PROV2_F_CLIENTE_FROM_PROV2_F_CONTRATTI_TITLE',
  'save' => true,
  'id_name' => 'prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida',
  'link' => 'prov2_f_contratti_prov2_f_cliente',
  'table' => 'prov2_f_contratti',
  'module' => 'prov2_F_Contratti',
  'rname' => 'name',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida"] = array (
  'name' => 'prov2_f_contratti_prov2_f_clienteprov2_f_contratti_ida',
  'type' => 'link',
  'relationship' => 'prov2_f_contratti_prov2_f_cliente',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_PROV2_F_CONTRATTI_PROV2_F_CLIENTE_FROM_PROV2_F_CONTRATTI_TITLE',
);


// created: 2014-06-06 12:29:35
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_upload_prov2_f_cliente"] = array (
  'name' => 'prov2_f_upload_prov2_f_cliente',
  'type' => 'link',
  'relationship' => 'prov2_f_upload_prov2_f_cliente',
  'source' => 'non-db',
  'module' => 'prov2_F_Upload',
  'bean_name' => 'prov2_F_Upload',
  'vname' => 'LBL_PROV2_F_UPLOAD_PROV2_F_CLIENTE_FROM_PROV2_F_UPLOAD_TITLE',
  'id_name' => 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_upload_prov2_f_cliente_name"] = array (
  'name' => 'prov2_f_upload_prov2_f_cliente_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV2_F_UPLOAD_PROV2_F_CLIENTE_FROM_PROV2_F_UPLOAD_TITLE',
  'save' => true,
  'id_name' => 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida',
  'link' => 'prov2_f_upload_prov2_f_cliente',
  'table' => 'prov2_f_upload',
  'module' => 'prov2_F_Upload',
  'rname' => 'document_name',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_upload_prov2_f_clienteprov2_f_upload_ida"] = array (
  'name' => 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida',
  'type' => 'link',
  'relationship' => 'prov2_f_upload_prov2_f_cliente',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_PROV2_F_UPLOAD_PROV2_F_CLIENTE_FROM_PROV2_F_UPLOAD_TITLE',
);

?>