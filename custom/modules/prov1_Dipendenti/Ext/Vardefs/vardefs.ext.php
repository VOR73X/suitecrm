<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-06-01 10:28:14
$dictionary["prov1_Dipendenti"]["fields"]["prov1_aziende_prov1_dipendenti"] = array (
  'name' => 'prov1_aziende_prov1_dipendenti',
  'type' => 'link',
  'relationship' => 'prov1_aziende_prov1_dipendenti',
  'source' => 'non-db',
  'module' => 'prov1_Aziende',
  'bean_name' => 'prov1_Aziende',
  'vname' => 'LBL_PROV1_AZIENDE_PROV1_DIPENDENTI_FROM_PROV1_AZIENDE_TITLE',
  'id_name' => 'prov1_aziende_prov1_dipendentiprov1_aziende_ida',
);
$dictionary["prov1_Dipendenti"]["fields"]["prov1_aziende_prov1_dipendenti_name"] = array (
  'name' => 'prov1_aziende_prov1_dipendenti_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV1_AZIENDE_PROV1_DIPENDENTI_FROM_PROV1_AZIENDE_TITLE',
  'save' => true,
  'id_name' => 'prov1_aziende_prov1_dipendentiprov1_aziende_ida',
  'link' => 'prov1_aziende_prov1_dipendenti',
  'table' => 'prov1_aziende',
  'module' => 'prov1_Aziende',
  'rname' => 'name',
);
$dictionary["prov1_Dipendenti"]["fields"]["prov1_aziende_prov1_dipendentiprov1_aziende_ida"] = array (
  'name' => 'prov1_aziende_prov1_dipendentiprov1_aziende_ida',
  'type' => 'link',
  'relationship' => 'prov1_aziende_prov1_dipendenti',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_PROV1_AZIENDE_PROV1_DIPENDENTI_FROM_PROV1_DIPENDENTI_TITLE',
);

?>