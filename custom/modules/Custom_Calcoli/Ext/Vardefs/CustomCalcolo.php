<?php

$dictionary['ab_calc']['fields']['summa'] = array (
    'name' => 'summa',
    'vname' => 'LBL_SUMMA',
    'type' => 'currency',
    'function' => array('name'=>'getSumma', 'returns'=>'html', 'include'=>'custom/CustomCode.php'),
    'len' => '255',
    'comment' => 'calc sum for two currency fields'
);