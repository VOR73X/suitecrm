<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

$mod_strings = array (
  'LBL_ASSIGNED_TO_ID' => 'Ответственный(ая)',
  'LBL_ASSIGNED_TO_NAME' => 'Ответственный(ая)',
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Дата создания',
  'LBL_DATE_MODIFIED' => 'Дата изменения',
  'LBL_MODIFIED' => 'Изменено',
  'LBL_MODIFIED_ID' => 'Изменено(ID)',
  'LBL_MODIFIED_NAME' => 'Изменено',
  'LBL_CREATED' => 'Создано',
  'LBL_CREATED_ID' => 'Создано(ID)',
  'LBL_DESCRIPTION' => 'Описание',
  'LBL_DELETED' => 'Удалено',
  'LBL_NAME' => 'Название',
  'LBL_CREATED_USER' => 'Создано',
  'LBL_MODIFIED_USER' => 'Изменено',
  'LBL_LIST_NAME' => 'Название',
  'LBL_EDIT_BUTTON' => 'Править',
  'LBL_REMOVE' => 'Удалить',
  'LBL_LIST_FORM_TITLE' => 'C_Azienda Список',
  'LBL_MODULE_NAME' => 'C_Azienda',
  'LBL_MODULE_TITLE' => 'C_Azienda',
  'LBL_HOMEPAGE_TITLE' => 'Мой C_Azienda',
  'LNK_NEW_RECORD' => 'Создать C_Azienda',
  'LNK_LIST' => 'View C_Azienda',
  'LNK_IMPORT_PROV1_C_AZIENDA' => 'Importa C_Azienda',
  'LBL_SEARCH_FORM_TITLE' => 'Поиск C_Azienda',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Просмотр истории',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Мероприятия',
  'LBL_PROV1_C_AZIENDA_SUBPANEL_TITLE' => 'C_Azienda',
  'LBL_NEW_FORM_TITLE' => 'Новый C_Azienda',
  'LBL_RAGIONE_SOCIALE' => 'Ragione Sociale',
  'LBL_VIA' => 'Via',
  'LBL_CITTA' => 'Citta',
  'LBL_PROVINCIA' => 'Provincia',
  'LBL_CAP' => 'Cap',
  'LBL_STATO' => 'Stato',
  'LBL_PARTITA_IVA' => 'Partita Iva',
  'LBL_TELEFONO' => 'Telefono',
  'LBL_FAX' => 'Fax',
  'LBL_CELLULARE' => 'Cellulare',
);