<?php
 // created: 2014-06-05 01:02:22
$layout_defs["prov1_C_Azienda"]["subpanel_setup"]['prov1_c_azienda_prov1_c_dipendenti'] = array (
  'order' => 100,
  'module' => 'prov1_C_Dipendenti',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROV1_C_AZIENDA_PROV1_C_DIPENDENTI_FROM_PROV1_C_DIPENDENTI_TITLE',
  'get_subpanel_data' => 'prov1_c_azienda_prov1_c_dipendenti',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
