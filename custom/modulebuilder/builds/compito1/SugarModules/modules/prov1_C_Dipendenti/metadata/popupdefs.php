<?php
$popupMeta = array (
    'moduleMain' => 'prov1_C_Dipendenti',
    'varName' => 'prov1_C_Dipendenti',
    'orderBy' => 'prov1_c_dipendenti.name',
    'whereClauses' => array (
  'name' => 'prov1_c_dipendenti.name',
),
    'searchInputs' => array (
  1 => 'name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'name' => 'name',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
),
);
