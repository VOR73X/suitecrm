<?php
$module_name = 'prov1_C_Dipendenti';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'type' => 'name',
    'link' => true,
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
  ),
  'COGNOME' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_COGNOME',
    'width' => '10%',
  ),
  'PROV1_C_AZIENDA_PROV1_C_DIPENDENTI_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PROV1_C_AZIENDA_PROV1_C_DIPENDENTI_FROM_PROV1_C_AZIENDA_TITLE',
    'id' => 'PROV1_C_AZIENDA_PROV1_C_DIPENDENTIPROV1_C_AZIENDA_IDA',
    'width' => '10%',
    'default' => true,
  ),
);
?>
