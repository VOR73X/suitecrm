<?php
// created: 2014-05-31 10:53:04
$dictionary["prov2_aziende_a_prov2_dipendenti_a"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'prov2_aziende_a_prov2_dipendenti_a' => 
    array (
      'lhs_module' => 'prov2_Aziende_A',
      'lhs_table' => 'prov2_aziende_a',
      'lhs_key' => 'id',
      'rhs_module' => 'prov2_Dipendenti_A',
      'rhs_table' => 'prov2_dipendenti_a',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'prov2_aziende_a_prov2_dipendenti_a_c',
      'join_key_lhs' => 'prov2_aziende_a_prov2_dipendenti_aprov2_aziende_a_ida',
      'join_key_rhs' => 'prov2_aziende_a_prov2_dipendenti_aprov2_dipendenti_a_idb',
    ),
  ),
  'table' => 'prov2_aziende_a_prov2_dipendenti_a_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'prov2_aziende_a_prov2_dipendenti_aprov2_aziende_a_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'prov2_aziende_a_prov2_dipendenti_aprov2_dipendenti_a_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'prov2_aziende_a_prov2_dipendenti_aspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'prov2_aziende_a_prov2_dipendenti_a_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prov2_aziende_a_prov2_dipendenti_aprov2_aziende_a_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'prov2_aziende_a_prov2_dipendenti_a_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'prov2_aziende_a_prov2_dipendenti_aprov2_dipendenti_a_idb',
      ),
    ),
  ),
);