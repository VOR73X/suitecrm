<?php
 // created: 2014-05-31 10:53:04
$layout_defs["prov2_Aziende_A"]["subpanel_setup"]['prov2_aziende_a_prov2_dipendenti_a'] = array (
  'order' => 100,
  'module' => 'prov2_Dipendenti_A',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROV2_AZIENDE_A_PROV2_DIPENDENTI_A_FROM_PROV2_DIPENDENTI_A_TITLE',
  'get_subpanel_data' => 'prov2_aziende_a_prov2_dipendenti_a',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
