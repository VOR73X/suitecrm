<?php
// created: 2014-05-31 10:53:04
$dictionary["prov2_Aziende_A"]["fields"]["prov2_aziende_a_prov2_dipendenti_a"] = array (
  'name' => 'prov2_aziende_a_prov2_dipendenti_a',
  'type' => 'link',
  'relationship' => 'prov2_aziende_a_prov2_dipendenti_a',
  'source' => 'non-db',
  'module' => 'prov2_Dipendenti_A',
  'bean_name' => 'prov2_Dipendenti_A',
  'side' => 'right',
  'vname' => 'LBL_PROV2_AZIENDE_A_PROV2_DIPENDENTI_A_FROM_PROV2_DIPENDENTI_A_TITLE',
);
