<?php
// created: 2014-05-31 10:53:04
$dictionary["prov2_Dipendenti_A"]["fields"]["prov2_aziende_a_prov2_dipendenti_a"] = array (
  'name' => 'prov2_aziende_a_prov2_dipendenti_a',
  'type' => 'link',
  'relationship' => 'prov2_aziende_a_prov2_dipendenti_a',
  'source' => 'non-db',
  'module' => 'prov2_Aziende_A',
  'bean_name' => 'prov2_Aziende_A',
  'vname' => 'LBL_PROV2_AZIENDE_A_PROV2_DIPENDENTI_A_FROM_PROV2_AZIENDE_A_TITLE',
  'id_name' => 'prov2_aziende_a_prov2_dipendenti_aprov2_aziende_a_ida',
);
$dictionary["prov2_Dipendenti_A"]["fields"]["prov2_aziende_a_prov2_dipendenti_a_name"] = array (
  'name' => 'prov2_aziende_a_prov2_dipendenti_a_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV2_AZIENDE_A_PROV2_DIPENDENTI_A_FROM_PROV2_AZIENDE_A_TITLE',
  'save' => true,
  'id_name' => 'prov2_aziende_a_prov2_dipendenti_aprov2_aziende_a_ida',
  'link' => 'prov2_aziende_a_prov2_dipendenti_a',
  'table' => 'prov2_aziende_a',
  'module' => 'prov2_Aziende_A',
  'rname' => 'name',
);
$dictionary["prov2_Dipendenti_A"]["fields"]["prov2_aziende_a_prov2_dipendenti_aprov2_aziende_a_ida"] = array (
  'name' => 'prov2_aziende_a_prov2_dipendenti_aprov2_aziende_a_ida',
  'type' => 'link',
  'relationship' => 'prov2_aziende_a_prov2_dipendenti_a',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_PROV2_AZIENDE_A_PROV2_DIPENDENTI_A_FROM_PROV2_DIPENDENTI_A_TITLE',
);
