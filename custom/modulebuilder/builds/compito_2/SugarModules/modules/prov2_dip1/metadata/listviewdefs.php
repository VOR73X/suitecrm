<?php
$module_name = 'prov2_dip1';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'PROV2_AZ1_PROV2_DIP1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_PROV2_AZ1_PROV2_DIP1_FROM_PROV2_AZ1_TITLE',
    'id' => 'PROV2_AZ1_PROV2_DIP1PROV2_AZ1_IDA',
    'width' => '10%',
    'default' => true,
  ),
);
?>
