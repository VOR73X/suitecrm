<?php
// created: 2014-06-01 10:52:24
$dictionary["prov2_az1_prov2_dip1"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'prov2_az1_prov2_dip1' => 
    array (
      'lhs_module' => 'prov2_az1',
      'lhs_table' => 'prov2_az1',
      'lhs_key' => 'id',
      'rhs_module' => 'prov2_dip1',
      'rhs_table' => 'prov2_dip1',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'prov2_az1_prov2_dip1_c',
      'join_key_lhs' => 'prov2_az1_prov2_dip1prov2_az1_ida',
      'join_key_rhs' => 'prov2_az1_prov2_dip1prov2_dip1_idb',
    ),
  ),
  'table' => 'prov2_az1_prov2_dip1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'prov2_az1_prov2_dip1prov2_az1_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'prov2_az1_prov2_dip1prov2_dip1_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'prov2_az1_prov2_dip1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'prov2_az1_prov2_dip1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prov2_az1_prov2_dip1prov2_az1_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'prov2_az1_prov2_dip1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'prov2_az1_prov2_dip1prov2_dip1_idb',
      ),
    ),
  ),
);