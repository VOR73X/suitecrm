<?php
 // created: 2014-06-01 10:52:24
$layout_defs["prov2_az1"]["subpanel_setup"]['prov2_az1_prov2_dip1'] = array (
  'order' => 100,
  'module' => 'prov2_dip1',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PROV2_AZ1_PROV2_DIP1_FROM_PROV2_DIP1_TITLE',
  'get_subpanel_data' => 'prov2_az1_prov2_dip1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
