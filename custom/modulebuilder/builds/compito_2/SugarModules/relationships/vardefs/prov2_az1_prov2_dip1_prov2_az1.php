<?php
// created: 2014-06-01 10:52:24
$dictionary["prov2_az1"]["fields"]["prov2_az1_prov2_dip1"] = array (
  'name' => 'prov2_az1_prov2_dip1',
  'type' => 'link',
  'relationship' => 'prov2_az1_prov2_dip1',
  'source' => 'non-db',
  'module' => 'prov2_dip1',
  'bean_name' => 'prov2_dip1',
  'side' => 'right',
  'vname' => 'LBL_PROV2_AZ1_PROV2_DIP1_FROM_PROV2_DIP1_TITLE',
);
