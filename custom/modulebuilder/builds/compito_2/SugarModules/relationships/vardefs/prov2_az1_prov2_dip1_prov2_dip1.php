<?php
// created: 2014-06-01 10:52:24
$dictionary["prov2_dip1"]["fields"]["prov2_az1_prov2_dip1"] = array (
  'name' => 'prov2_az1_prov2_dip1',
  'type' => 'link',
  'relationship' => 'prov2_az1_prov2_dip1',
  'source' => 'non-db',
  'module' => 'prov2_az1',
  'bean_name' => 'prov2_az1',
  'vname' => 'LBL_PROV2_AZ1_PROV2_DIP1_FROM_PROV2_AZ1_TITLE',
  'id_name' => 'prov2_az1_prov2_dip1prov2_az1_ida',
);
$dictionary["prov2_dip1"]["fields"]["prov2_az1_prov2_dip1_name"] = array (
  'name' => 'prov2_az1_prov2_dip1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV2_AZ1_PROV2_DIP1_FROM_PROV2_AZ1_TITLE',
  'save' => true,
  'id_name' => 'prov2_az1_prov2_dip1prov2_az1_ida',
  'link' => 'prov2_az1_prov2_dip1',
  'table' => 'prov2_az1',
  'module' => 'prov2_az1',
  'rname' => 'name',
);
$dictionary["prov2_dip1"]["fields"]["prov2_az1_prov2_dip1prov2_az1_ida"] = array (
  'name' => 'prov2_az1_prov2_dip1prov2_az1_ida',
  'type' => 'link',
  'relationship' => 'prov2_az1_prov2_dip1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_PROV2_AZ1_PROV2_DIP1_FROM_PROV2_DIP1_TITLE',
);
