<?php
// created: 2014-06-06 12:29:35
$dictionary["prov2_f_upload_prov2_f_cliente"] = array (
  'true_relationship_type' => 'one-to-one',
  'relationships' => 
  array (
    'prov2_f_upload_prov2_f_cliente' => 
    array (
      'lhs_module' => 'prov2_F_Upload',
      'lhs_table' => 'prov2_f_upload',
      'lhs_key' => 'id',
      'rhs_module' => 'prov2_F_Cliente',
      'rhs_table' => 'prov2_f_cliente',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'prov2_f_upload_prov2_f_cliente_c',
      'join_key_lhs' => 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida',
      'join_key_rhs' => 'prov2_f_upload_prov2_f_clienteprov2_f_cliente_idb',
    ),
  ),
  'table' => 'prov2_f_upload_prov2_f_cliente_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'prov2_f_upload_prov2_f_clienteprov2_f_cliente_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'prov2_f_upload_prov2_f_clientespk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'prov2_f_upload_prov2_f_cliente_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'prov2_f_upload_prov2_f_cliente_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prov2_f_upload_prov2_f_clienteprov2_f_cliente_idb',
      ),
    ),
  ),
);