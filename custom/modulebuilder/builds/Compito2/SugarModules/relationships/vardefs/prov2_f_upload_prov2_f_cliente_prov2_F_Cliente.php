<?php
// created: 2014-06-06 12:29:35
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_upload_prov2_f_cliente"] = array (
  'name' => 'prov2_f_upload_prov2_f_cliente',
  'type' => 'link',
  'relationship' => 'prov2_f_upload_prov2_f_cliente',
  'source' => 'non-db',
  'module' => 'prov2_F_Upload',
  'bean_name' => 'prov2_F_Upload',
  'vname' => 'LBL_PROV2_F_UPLOAD_PROV2_F_CLIENTE_FROM_PROV2_F_UPLOAD_TITLE',
  'id_name' => 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_upload_prov2_f_cliente_name"] = array (
  'name' => 'prov2_f_upload_prov2_f_cliente_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV2_F_UPLOAD_PROV2_F_CLIENTE_FROM_PROV2_F_UPLOAD_TITLE',
  'save' => true,
  'id_name' => 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida',
  'link' => 'prov2_f_upload_prov2_f_cliente',
  'table' => 'prov2_f_upload',
  'module' => 'prov2_F_Upload',
  'rname' => 'document_name',
);
$dictionary["prov2_F_Cliente"]["fields"]["prov2_f_upload_prov2_f_clienteprov2_f_upload_ida"] = array (
  'name' => 'prov2_f_upload_prov2_f_clienteprov2_f_upload_ida',
  'type' => 'link',
  'relationship' => 'prov2_f_upload_prov2_f_cliente',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_PROV2_F_UPLOAD_PROV2_F_CLIENTE_FROM_PROV2_F_UPLOAD_TITLE',
);
