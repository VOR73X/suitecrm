<?php
// created: 2014-06-05 08:51:11
$dictionary["prov2_F_Contratti"]["fields"]["prov2_f_contratti_prov2_f_cliente"] = array (
  'name' => 'prov2_f_contratti_prov2_f_cliente',
  'type' => 'link',
  'relationship' => 'prov2_f_contratti_prov2_f_cliente',
  'source' => 'non-db',
  'module' => 'prov2_F_Cliente',
  'bean_name' => false,
  'vname' => 'LBL_PROV2_F_CONTRATTI_PROV2_F_CLIENTE_FROM_PROV2_F_CLIENTE_TITLE',
  'id_name' => 'prov2_f_contratti_prov2_f_clienteprov2_f_cliente_idb',
);
$dictionary["prov2_F_Contratti"]["fields"]["prov2_f_contratti_prov2_f_cliente_name"] = array (
  'name' => 'prov2_f_contratti_prov2_f_cliente_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_PROV2_F_CONTRATTI_PROV2_F_CLIENTE_FROM_PROV2_F_CLIENTE_TITLE',
  'save' => true,
  'id_name' => 'prov2_f_contratti_prov2_f_clienteprov2_f_cliente_idb',
  'link' => 'prov2_f_contratti_prov2_f_cliente',
  'table' => 'prov2_f_cliente',
  'module' => 'prov2_F_Cliente',
  'rname' => 'name',
);
$dictionary["prov2_F_Contratti"]["fields"]["prov2_f_contratti_prov2_f_clienteprov2_f_cliente_idb"] = array (
  'name' => 'prov2_f_contratti_prov2_f_clienteprov2_f_cliente_idb',
  'type' => 'link',
  'relationship' => 'prov2_f_contratti_prov2_f_cliente',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_PROV2_F_CONTRATTI_PROV2_F_CLIENTE_FROM_PROV2_F_CLIENTE_TITLE',
);
