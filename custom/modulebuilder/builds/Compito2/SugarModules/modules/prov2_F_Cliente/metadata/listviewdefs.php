<?php
$module_name = 'prov2_F_Cliente';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'PREZZO_TOTALE' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PREZZO_TOTALE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
);
?>
