<?php
// created: 2014-06-06 12:29:34
$dictionary["prov2_f_cliente_prov2_f_contratti"] = array (
  'true_relationship_type' => 'one-to-one',
  'relationships' => 
  array (
    'prov2_f_cliente_prov2_f_contratti' => 
    array (
      'lhs_module' => 'prov2_F_Cliente',
      'lhs_table' => 'prov2_f_cliente',
      'lhs_key' => 'id',
      'rhs_module' => 'prov2_F_Contratti',
      'rhs_table' => 'prov2_f_contratti',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'prov2_f_cliente_prov2_f_contratti_c',
      'join_key_lhs' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida',
      'join_key_rhs' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb',
    ),
  ),
  'table' => 'prov2_f_cliente_prov2_f_contratti_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'prov2_f_cliente_prov2_f_contrattispk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'prov2_f_cliente_prov2_f_contratti_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prov2_f_cliente_prov2_f_contrattiprov2_f_cliente_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'prov2_f_cliente_prov2_f_contratti_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prov2_f_cliente_prov2_f_contrattiprov2_f_contratti_idb',
      ),
    ),
  ),
);