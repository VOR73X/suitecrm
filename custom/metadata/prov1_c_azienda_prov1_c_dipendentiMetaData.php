<?php
// created: 2014-06-05 01:02:22
$dictionary["prov1_c_azienda_prov1_c_dipendenti"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'prov1_c_azienda_prov1_c_dipendenti' => 
    array (
      'lhs_module' => 'prov1_C_Azienda',
      'lhs_table' => 'prov1_c_azienda',
      'lhs_key' => 'id',
      'rhs_module' => 'prov1_C_Dipendenti',
      'rhs_table' => 'prov1_c_dipendenti',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'prov1_c_azienda_prov1_c_dipendenti_c',
      'join_key_lhs' => 'prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida',
      'join_key_rhs' => 'prov1_c_azienda_prov1_c_dipendentiprov1_c_dipendenti_idb',
    ),
  ),
  'table' => 'prov1_c_azienda_prov1_c_dipendenti_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'prov1_c_azienda_prov1_c_dipendentiprov1_c_dipendenti_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'prov1_c_azienda_prov1_c_dipendentispk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'prov1_c_azienda_prov1_c_dipendenti_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'prov1_c_azienda_prov1_c_dipendentiprov1_c_azienda_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'prov1_c_azienda_prov1_c_dipendenti_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'prov1_c_azienda_prov1_c_dipendentiprov1_c_dipendenti_idb',
      ),
    ),
  ),
);